<?php

class LoginController extends Controller
{
    public $model;

    /**
     * summary
     */
    public function __construct()
    {
        $this->model = new Login();
    }

    public function login($taiKhoan, $matKhau)
    {
        $data = $this->model->check($taiKhoan, $matKhau);

        if (count($data) >= 1) {
            $_SESSION['user'] = $data[0]['KH_TaiKhoan'];
            header('Location: index.php');

            exit;
        } else {
            $_SESSION['message'] = 'User name or password is not true !';
            header("Refresh:0");

            exit;
        }
    }
}
