<?php

class NewsController extends Controller
{
    public $model;

    public function __construct()
    {
        $this->model = new News();
    }

    public function index($perPage, $current)
    {
        return $this->model->index($perPage, $current);
    }

    public function show($id)
    {
        return $this->model->show($id);
    }

    public function getTotalPage()
    {
        return $this->model->totalPage();
    }
}
