<?php

class CartController extends Controller
{
    public function index()
    {
        $data = isset($_SESSION['cart']) ? unserialize($_SESSION['cart']) : null;

        return $data;
    }


    public function add($id)
    {
        try {
            $oldCart = isset($_SESSION['cart']) ? unserialize($_SESSION['cart']) : null;
            $cart = new Cart($oldCart);
            $cart->add($id);

            unset($cart->connect);

            $_SESSION['cart'] = serialize((array)$cart);

            return $_SESSION['cart'];
        } catch (Exception $e) {
            echo '<pre>';
            var_dump($e);
            echo '<pre>';
        }
    }

    public function minus($id)
    {
        try {
            $oldCart = isset($_SESSION['cart']) ? unserialize($_SESSION['cart']) : null;
            $cart = new Cart($oldCart);
            $cart->minus($id);

            unset($cart->connect);

            if ($cart->totalQuantity < 1) {
                unset($_SESSION['cart']);
            } else {
                $_SESSION['cart'] = serialize((array)$cart);

                return $_SESSION['cart'];
            }
        } catch (Exception $e) {
            echo '<pre>';
            var_dump($e);
            echo '<pre>';
        }
    }
}
