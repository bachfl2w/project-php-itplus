<?php
    include 'Controller/ProductController.php';

    $controller = new ProductController();

    $data = [];

    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        $data = $controller->show($id);
?>

<div class="row" style="padding-bottom: 150px">
    <?php foreach ($data as $value) { ?>
        <div class="col-sm-3">
            <img class="img-fluid" src="Asset/image/<?= $value['Anh_Ten'] ?>">
            <div class="border" style="padding: 10px">
                <p>some text</p>
            </div>
        </div>
        <div class="col-sm-6 border">
            <h5>
                <?= $value['SP_Ten'] ?>
                <?php if ($value['SP_TinhTrang'] == 1): ?>
                    <span class="text-success"> - Active</span>
                <?php else: ?>
                    <span class="text-danger"> - Unactive</span>
                <?php endif ?>
            </h5>
            <p>
                <b>
                <?php
                    $price = $value['SP_GiaNiemYet'];

                    if ($value['SP_KhuyenMai'] != 0) {
                        echo '<s>' . number_format($price, 0, '', '.') . ' ₫</s><br>';
                        $price = $value['SP_GiaNiemYet'] - $value['SP_GiaNiemYet'] * $value['SP_KhuyenMai'] / 100;
                    }

                    echo number_format($price, 0, '', '.') . ' ₫';
                ?>
                </b>
            </p>
            <hr>
            <ul style="margin-left: -20px">
                <li><b>manufacturer:</b> <?= $value['NSX_Ten'] ?></li>
                <li><b>operating system:</b> <?= $value['Lap_HDH'] ?></li>
                <li><b>CPU:</b> <?= $value['Lap_CPU'] ?></li>
                <li><b>Screen:</b> <?= $value['Lap_ManHinh'] ?></li>
                <li><b>Hardware:</b> <?= $value['Lap_OCung'] ?></li>
                <li><b>CD Driver:</b> <?= $value['Lap_ODia'] ?></li>
                <li><b>RAM:</b> <?= $value['Lap_RAM'] ?></li>
                <li><b>Pin:</b> <?= $value['Lap_Pin'] ?></li>
            </ul>
            <a href="#" class="btn btn-danger" style="color: white">Buy</a>
            <a href="<?= $indexLink ?>?type=client&page=show_cart&action=add&id=<?= $value['SP_Ma'] ?>" class="btn btn-warning">Add to Cart</a>
            <hr>
            <p><?= $value['Lap_MoTa'] ?></p>
        </div>
        <div class="col-sm-3">
            <div class="border" style="padding: 10px">
                <p>some text</p>
            </div>
        </div>
    <?php } ?>

    <div class="container" style="padding-top: 150px; font-size: 13px">
        <div class="media border p-3">
            <img src="https://api.adorable.io/avatars/1" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:60px;">
            <div class="media-body">
                <h4>John Doe <small><i>Posted on February 19, 2016</i></small></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <div class="media p-3">
                    <img src="https://api.adorable.io/avatars/2" alt="Jane Doe" class="mr-3 mt-3 rounded-circle" style="width:45px;">
                    <div class="media-body">
                        <h4>Jane Doe <small><i>Posted on February 20 2016</i></small></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
        </div>
        <form class="form-inline" action="/action_page.php">
            <input type="email" class="form-control" id="email" style="width: 80%; border-radius: 0; border-top: 0" placeholder="Comment ...">
            <button type="submit" class="btn btn-dark" style="width: 20%; border-radius: 0">Comment</button>
        </form>
    </div>
</div>

<?php } else { ?>
    <div class="row" style="padding-bottom: 100px">
        <div class="col-sm-12 text-center">
            <h1>404</h1>
        </div>
    </div>
<?php } ?>
