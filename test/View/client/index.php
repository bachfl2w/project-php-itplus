<?php
    include 'Controller/ProductController.php';

    $controller = new ProductController();
    $total = $controller->getTotalPage();
    $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $per = 9;
    $cur = 1;

    $totalInPage = $total['total'] / $per;

    $page = $totalInPage + 1;

    if (isset($_GET['cur'])) {
        $cur = $_GET['cur'];
        if ($cur < 1) {
            $cur = 1;
        } elseif ($cur > $totalInPage) {
            $cur = $totalInPage;
        }
    }

    $data = $controller->index($per, $cur);
?>
<div class="row" style="padding-top: 70px">
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">Filter</div>
            <div class="card-body">
                <span>Price:</span>
                <input type="range" class="custom-range">
            </div>
            <div class="card-footer">
                <button class="btn btn-info" type="button">Apply</button>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card">
            <div class="card-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" title="Gallery view" href="#list1"><i class="fas fa-th-large"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" title="List view" href="#list2"><i class="fas fa-th-list"></i></a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane container active" id="list1">
                        <div class="row">
                            <?php foreach ($data as $value): ?>
                                <div class="col-sm-4" style="padding: 25px 10px">
                                    <div class="card">
                                        <img src="Asset/image/<?= $value['Anh_Ten'] ?>" style="height: 250px">
                                        <div class="card-body">
                                            <?= $value['SP_Ten'] ?>
                                        </div>
                                        <div class="card-footer">
                                            <?php
                                                $price = $value['SP_GiaNiemYet'];

                                                if ($value['SP_KhuyenMai'] != 0) {
                                                    echo '<s>' . number_format($price, 0, '', '.') . '₫</s><br>';
                                                    $price = $value['SP_GiaNiemYet'] - $value['SP_GiaNiemYet'] * $value['SP_KhuyenMai'] / 100;
                                                }

                                                echo number_format($price, 0, '', '.') . '₫';
                                            ?>
                                            <a href="<?= $indexLink ?>?type=client&page=show_laptop&id=<?= $value['SP_Ma'] ?>" class="btn btn-secondary float-right btn-sm">Show</a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                            <div class="col-md-12">
                                <ul class="pagination">
                                    <?php
                                        if ($cur == 1)
                                            $disabled = 'disabled';
                                        else
                                            $disabled = '';
                                        ?>
                                            <li class="page-item <?= $disabled ?>"><a class="page-link" href="<?= $indexLink ?>?cur=<?= ($cur - 1) ?>">Previous</a></li>
                                    <?php
                                        for ($i = 1; $i < $page; $i++) {
                                            if ($cur == $i)
                                                $active = 'active';
                                            else
                                                $active = '';
                                        ?>
                                            <li class="page-item <?= $active ?>"><a class="page-link" href="<?= $indexLink ?>?cur=<?= $i ?>"><?= $i ?></a></li>
                                        <?php
                                        }
                                    ?>
                                    <?php
                                        if ($cur == $page)
                                            $disabled = 'disabled';
                                        else
                                            $disabled = '';
                                        ?>
                                            <li class="page-item <?= $disabled ?>"><a class="page-link" href="<?= $indexLink ?>?cur=<?= ($cur + 1) ?>">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane container fade" id="list2">
                        <?php foreach ($data as $value): ?>
                            <div class="row" style="padding: 15px 0px">
                                <div class="col-sm-4">
                                    <a href="#"><img src="Asset/image/<?= $value['Anh_Ten'] ?>" style="height: 250px"></a>
                                </div>
                                <div class="col-sm-8">
                                    <h4><?= $value['SP_Ten'] ?></h4>
                                    <?php
                                        $price = $value['SP_GiaNiemYet'];

                                        if ($value['SP_KhuyenMai'] != 0) {
                                            echo '<s>' . number_format($price, 0, '', '.') . '₫</s><br>';
                                            $price = $value['SP_GiaNiemYet'] - $value['SP_GiaNiemYet'] * $value['SP_KhuyenMai'] / 100;
                                        }

                                        echo number_format($price, 0, '', '.') . '₫';
                                    ?>
                                    <br>
                                </div>
                            </div>
                            <hr>
                        <?php endforeach ?>
                        <div class="col-md-12">
                            <ul class="pagination">
                                <?php
                                    if ($cur == 1)
                                        $disabled = 'disabled';
                                    else
                                        $disabled = '';
                                    ?>
                                        <li class="page-item <?= $disabled ?>"><a class="page-link" href="<?= $indexLink ?>?cur=<?= ($cur - 1) ?>">Previous</a></li>
                                <?php
                                    for ($i = 1; $i < $page; $i++) {
                                        if ($cur == $i)
                                            $active = 'active';
                                        else
                                            $active = '';
                                    ?>
                                        <li class="page-item <?= $active ?>"><a class="page-link" href="<?= $indexLink ?>?cur=<?= $i ?>"><?= $i ?></a></li>
                                    <?php
                                    }
                                ?>
                                <?php
                                    if ($cur == $page)
                                        $disabled = 'disabled';
                                    else
                                        $disabled = '';
                                    ?>
                                        <li class="page-item <?= $disabled ?>"><a class="page-link" href="<?= $indexLink ?>?cur=<?= ($cur + 1) ?>">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
