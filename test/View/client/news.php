<?php
    include 'Controller/NewsController.php';

    $controller = new NewsController();
    $total = $controller->getTotalPage();
    $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $per = 4;
    $cur = 1;

    $totalInPage = $total['total'] / $per;

    $page = $totalInPage + 1;

    if (isset($_GET['cur'])) {

        $cur = $_GET['cur'];

        if ($cur < 1) {
            $cur = 1;
        } elseif ($cur > $totalInPage) {
            $cur = $totalInPage;
        }
    }

    $data = $controller->index($per, $cur);
?>

<div class="row" style="padding-bottom: 100px">
    <div class="col-sm-4">
        <h2>About Me</h2>
        <h5>Photo of me:</h5>
        <div class="fakeimg">Fake Image</div>
        <p>Some text about me in culpa qui officia deserunt mollit anim..</p>
        <h3>Some Links</h3>
        <p>Lorem ipsum dolor sit ame.</p>
    </div>
    <div class="col-sm-8 background-white">
        <?php foreach ($data as $value) { ?>
            <div class="col-sm-12">
                <h2><?= $value['TT_TieuDe'] ?></h2>
                <h5><?= $value['TT_NgayViet'] ?></h5>
                <div class="fakeimg">
                    <img src="Asset/image/<?= $value['TT_HinhAnh'] ?>">
                </div>
                <p>
                    <?= substr($value['TT_NoiDung'], 0, 500) . ' ...' ?>
                    <a href="<?= $indexLink ?>?type=client&page=show_news&id=<?= $value['TT_Ma'] ?>" class="btn-link">Show more</a>
                </p>
            </div>
            <hr>
        <?php } ?>
        <div class="col-md-12">
            <ul class="pagination">
                <?php
                    if ($cur == 1)
                        $disabled = 'disabled';
                    else
                        $disabled = '';
                    ?>
                        <li class="page-item <?= $disabled ?>"><a class="page-link" href="<?= $indexLink ?>?type=client&page=news&cur=<?= ($cur - 1) ?>">Previous</a></li>
                <?php
                    for ($i = 1; $i < $page; $i++) {
                        if ($cur == $i)
                            $active = 'active';
                        else
                            $active = '';
                    ?>
                        <li class="page-item <?= $active ?>"><a class="page-link" href="<?= $indexLink ?>?type=client&page=news&cur=<?= $i ?>"><?= $i ?></a></li>
                    <?php
                    }
                ?>
                <?php
                    if ($cur == $page) {
                        $disabled = 'disabled';
                    }
                    else {
                        $disabled = '';
                    }
                    ?>
                        <li class="page-item <?= $disabled ?>"><a class="page-link" href="<?= $indexLink ?>?type=client&page=news&cur=<?= ($cur + 1) ?>">Next</a></li>
            </ul>
        </div>
    </div>
</div>
