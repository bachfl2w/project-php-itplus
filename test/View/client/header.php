<!-- nav bar -->
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <div class="container">
        <!-- Brand/logo -->
        <a class="navbar-brand" href="index.php">Logo</a>

        <!-- Links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php?type=client&page=news">News</a>
            </li>
            <?php if (isset($_GET['logout']) && $_GET['logout'] == 'true'): ?>
                <?php unset($_SESSION['user']); ?>
            <?php endif ?>
            <li class="nav-item">
            <?php
                $total = 'empty';
                $link = $indexLink . '?type=client&page=show_cart';
                if (isset($_SESSION['cart'])) {
                    $total = unserialize($_SESSION['cart'])['totalQuantity'];
                }
            ?>
            <a class="nav-link" href="<?= $link ?>">Cart (<?= $total ?>)</a>
            </li>
            <?php if (isset($_SESSION['user'])): ?>
                <li class="nav-item">
                    <a class="nav-link" class="dropdown-toggle" data-toggle="dropdown" href="index.php"><?= $_SESSION['user'] ?></a>
                    <div class="dropdown-menu" style="margin-left: 1000px">
                        <a class="dropdown-item" href="#">My Profile</a>
                        <a class="dropdown-item" href="index.php?type=client&page=login&logout=true">Logout</a>
                    </div>
                </li>
                <?php else: ?>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?type=client&page=login">Login</a>
                </li>
            <?php endif ?>
            <li class="nav-item">
                <form class="form-inline" action="/action_page.php">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search">
                    <button class="btn btn-success" type="submit">Search</button>
                </form>
            </li>
        </ul>
    </div>
</nav>
<!-- end nav bar -->
