<?php
    if (isset($_POST['order'])) {
        include 'Controller/';
        // $controller
    }
?>

<div class="row" style="padding-bottom: 150px">
    <?php if ($data): ?>
    <div class="col-sm-8">
        <div class="card">
            <div class="card-header">Cart</div>
            <?php foreach ($data['item'] as $value) { ?>
                <div class="card-body row">
                    <div class="col-sm-3">
                        <img src="Asset/image/<?= $value['product']['Anh_Ten'] ?>" alt="John Doe" class="mr-3 mt-3" style="width: 100%">
                    </div>
                    <div class="col-sm-9 container">
                        <b><?= $value['product']['SP_Ten'] ?></b>
                        <br>
                        <?= number_format($value['price'], 0, '', '.') . '₫' ?>
                        <div class="form-inline">
                            <a href="<?= $indexLink ?>?type=client&page=show_cart&action=minus&id=<?= $value['product']['SP_Ma'] ?>" class="minus btn btn-warning" style="border-radius: 0">-</a>
                            <input readonly type="number" class="form-control" style="width: 20%; border-radius: 0" value="<?= $value['qty'] ?>">
                            <a href="<?= $indexLink ?>?type=client&page=show_cart&action=add&id=<?= $value['product']['SP_Ma'] ?>" class="add btn btn-warning" style="border-radius: 0">+</a>
                        </div>
                    </div>
                </div>
                <?php if (!end($value)): ?>
                    <hr>
                <?php endif ?>
            <?php } ?>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-header" style="padding: 10px">Infomation</div>
            <div class="card-body">
                <p><b>Total Price:</b> <?= number_format($data['totalPrice'], 0, '', '.') . '₫' ?></p>
                <p><b>Total Quantity:</b> <?= $data['totalQuantity'] ?></p>
            </div>
        </div>
    </div>
    <?php endif ?>

    <div class="container background-white" style="margin-top: 150px; padding: 40px">
        <form method="POST" class="row" action="">
            <input type="hidden" name="id" value="">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="receiver">Receiver:</label>
                    <input type="text" name="receiver" class="form-control" id="receiver" required="">
                </div>
                <div class="form-group">
                    <label for="address">Receiving address:</label>
                    <input type="text" class="form-control" name="address" id="address" required="">
                </div>
                <div class="form-group">
                    <label for="phone">Phone number:</label>
                    <input type="number" class="form-control" id="phone" required="">
                </div>
                <div class="form-group">
                    <label for="pwd">Note:</label>
                    <textarea class="form-control" name="note"></textarea>
                </div>
                <button type="submit" name="order" class="btn btn-warning">Order</button>
                <button type="reset" class="btn btn-dark">Reset</button>
            </div>
            <div class="col-sm-6 border" style="background-color: gray"></div>
        </form>
    </div>

    <div class="container background-white" style="margin-top: 150px; font-size: 13px; padding: 0px">
        <div class="media p-3">
            <img src="https://api.adorable.io/avatars/1" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:60px;">
            <div class="media-body">
                <h4>John Doe <small><i>Posted on February 19, 2016</i></small></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <div class="media p-3">
                    <img src="https://api.adorable.io/avatars/2" alt="Jane Doe" class="mr-3 mt-3 rounded-circle" style="width:45px;">
                    <div class="media-body">
                        <h4>Jane Doe <small><i>Posted on February 20 2016</i></small></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
        </div>
        <form class="form-inline" action="/action_page.php">
            <input type="email" class="form-control" id="email" style="width: 80%; border-radius: 0;" placeholder="Comment ...">
            <button type="submit" class="btn btn-dark" style="width: 20%; border-radius: 0">Comment</button>
        </form>
    </div>
</div>
