<?php
    include 'Controller/NewsController.php';

    $controller = new NewsController();

    $data = [];

    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        $data = $controller->show($id);
?>

<div class="row background-white" style="padding-bottom: 100px">
    <div class="col-sm-12">
        <?php foreach ($data as $value) { ?>
            <div class="col-sm-12">
                <h2><?= $value['TT_TieuDe'] ?></h2>
                <h5><?= $value['TT_NgayViet'] ?></h5>
                <div class="fakeimg" style="padding: 20px 0px 30px">
                    <img src="Asset/image/<?= $value['TT_HinhAnh'] ?>">
                </div>
                <p><?= $value['TT_NoiDung'] ?></p>
            </div>
        <?php } ?>
    </div>
</div>

<?php } else { ?>
    <div class="row" style="padding-bottom: 100px">
        <div class="col-sm-12 text-center">
            <h1>404</h1>
        </div>
    </div>
<?php } ?>
