<?php
    require_once('Controller/LoginController.php');

    $controller = new LoginController();

    if (isset($_POST['submit'])) {
        $controller->login(
            $_POST['username'],
            $_POST['pwd']
        );
    }
?>

<div class="container jumbotron col-sm-6 background-white">
    <form action="" method="post">
        <div class="form-group">
            <label for="username">Username:</label>
            <input type="text" class="form-control" id="username" name="username">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" name="pwd">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <?php if (isset($_SESSION['message'])): ?>
            <p><?php echo $_SESSION['message']; ?></p>
            <?php unset($_SESSION['message']) ?>
        <?php endif ?>
        <button type="submit" name="submit" class="btn btn-success">Submit</button>
    </form>
</div>
