<?php
/**
 * summary
 */
class News extends Model
{
    public function index($perPage, $current)
    {
        $sql = '
            SELECT *
            FROM tin_tuc
            LIMIT :batDau, :ketThuc
        ';

        $start = $perPage * ($current - 1);
        $end = $start + $perPage;

        $sta = $this->connect->prepare($sql);
        $sta->bindParam(':batDau', $start, PDO::PARAM_INT);
        $sta->bindParam(':ketThuc', $end, PDO::PARAM_INT);
        $sta->execute();

        return $sta->fetchAll();
    }

    public function show($id)
    {
        $sql = '
            SELECT *
            FROM tin_tuc
            WHERE TT_Ma = :id
        ';

        $sta = $this->connect->prepare($sql);
        $sta->bindParam(':id', $id, PDO::PARAM_INT);
        $sta->execute();

        return $sta->fetchAll();
    }

    public function totalPage()
    {
        $sql = '
            SELECT count(TT_Ma) as total
            FROM tin_tuc
        ';

        $sta = $this->connect->prepare($sql);
        $sta->execute();

        return $sta->fetch();
    }
}
