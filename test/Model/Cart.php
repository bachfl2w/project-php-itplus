<?php
/**
 * summary
 */
class Cart extends Model
{
    public $item = null;
    public $totalPrice = 0;
    public $totalQuantity = 0;

    public function __construct($oldCart)
    {
        parent::__construct();

        if ($oldCart) {
            $this->item = $oldCart['item'];
            $this->totalPrice = $oldCart['totalPrice'];
            $this->totalQuantity = $oldCart['totalQuantity'];
        }
    }

    public function add($id)
    {
        $sql = '
            SELECT *
            FROM san_pham a
            INNER JOIN anh b
            ON a.SP_Ma = b.SP_Ma
            WHERE a.SP_Ma = :id
        ';

        $sta = $this->connect->prepare($sql);
        $sta->bindParam(':id', $id, PDO::PARAM_INT);
        $sta->execute();

        $product = $sta->fetch(PDO::FETCH_ASSOC);

        $cart = [
            'qty' => 0,
            'price' => 0,
            'product' => $product,
        ];

        if ($this->item) {
            if (array_key_exists($product['SP_Ma'], $this->item)) {
                $cart = $this->item[$id];
            }
        }

        $cart['qty']++;
        $this->totalQuantity++;

        $cart['price'] = $product['SP_GiaNiemYet'] - ($product['SP_GiaNiemYet'] * $product['SP_KhuyenMai'] / 100);
        $this->totalPrice += $cart['price'];

        $this->item[$product['SP_Ma']] = $cart;
    }

    public function minus($id)
    {
        if (isset($this->item[$id])) {
            $this->item[$id]['qty']--;
            $this->item[$id]['price'] -= $this->item[$id]['product']['price'];
            $this->totalQuantity--;
            $this->totalPrice -= $this->item[$id]['price'];

            if($this->item[$id]['qty'] <= 0){
                unset($this->item[$id]);
            }
        }
    }
}
