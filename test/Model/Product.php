<?php
/**
 * summary
 */
class Product extends Model
{
    public function index($perPage, $current)
    {
        $sql = '
            SELECT *
            FROM san_pham a
            INNER JOIN sp_laptop b
            on a.SP_MA = b.SP_MA
            INNER JOIN anh c
            ON a.SP_MA = c.SP_MA
            INNER JOIN nha_san_xuat d
            ON d.NSX_Ma = a.NSX_Ma
            where a.SP_TinhTrang = 1
            LIMIT :batDau, :ketThuc
        ';

        $start = $perPage * ($current - 1);
        $end = $start + $perPage;

        $sta = $this->connect->prepare($sql);
        $sta->bindParam(':batDau', $start, PDO::PARAM_INT);
        $sta->bindParam(':ketThuc', $end, PDO::PARAM_INT);
        $sta->execute();

        return $sta->fetchAll();
    }

    public function show($id)
    {
        $sql = '
            SELECT *
            FROM san_pham a
            INNER JOIN sp_laptop b
            on a.SP_MA = b.SP_MA
            INNER JOIN anh c
            ON a.SP_MA = c.SP_MA
            INNER JOIN nha_san_xuat d
            ON d.NSX_Ma = a.NSX_Ma
            where a.SP_MA = :id
        ';

        $sta = $this->connect->prepare($sql);
        $sta->bindParam(':id', $id, PDO::PARAM_INT);
        $sta->execute();

        return $sta->fetchAll();
    }

    public function totalPage()
    {
        $sql = '
            SELECT count(a.SP_MA) as total
            FROM san_pham a
            INNER JOIN sp_laptop b
            on a.SP_MA = b.SP_MA
        ';

        $sta = $this->connect->prepare($sql);
        $sta->execute();

        return $sta->fetch();
    }
}
