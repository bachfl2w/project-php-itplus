<?php
/**
 * summary
 */
class Login extends Model
{
    public function check($taiKhoan, $matKhau)
    {
        $sql = '
            SELECT KH_TaiKhoan, KH_MatKhau
            FROM khach_hang a
            where KH_TaiKhoan = :tai_khoan AND KH_MatKhau = :mat_khau
        ';

        $sta = $this->connect->prepare($sql);
        $sta->bindParam(':tai_khoan', $taiKhoan, PDO::PARAM_STR);
        $sta->bindParam(':mat_khau', $matKhau, PDO::PARAM_STR);
        $sta->execute();

        return $sta->fetchAll();
    }
}
