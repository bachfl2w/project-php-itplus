<?php

abstract class Model
{
    public $connect;

    public function __construct()
    {
        try {
            $this->connect = new PDO(
                'mysql:host=localhost;dbname=shoplaptop',
                'root', // user
                '',     // pass
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
            );
        } catch (Exception $e) {
            print_r($e);
        }
    }
}
