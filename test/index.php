<?php
    session_start();

    require_once 'Controller/Controller.php';
    require_once 'Controller/CartController.php';

    require_once 'Model/Model.php';
    require_once 'Model/Login.php';
    require_once 'Model/Cart.php';
    require_once 'Model/News.php';
    require_once 'Model/Product.php';

    $cartController = new CartController();
    $data = [];

    if (isset($_GET['action'])) {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];

            if ($_GET['action'] == 'add') {
                try {
                    $data = unserialize($cartController->add($id));
                } catch (Exception $e) {
                    echo '<pre>';
                    var_dump($e);
                    echo '<pre>';
                }
            } elseif ($_GET['action'] == 'minus') {
                try {
                    $data = unserialize($cartController->minus($id));
                } catch (Exception $e) {
                    echo '<pre>';
                    var_dump($e);
                    echo '<pre>';
                }
            }
        }
    } else {
        $data = $cartController->index();
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>index</title>

    <!-- bootstrap -->
    <?php include 'Asset/asset.php'; ?>
    <!-- bootstrap -->

    <style type="text/css">
        body {
            background-color: #EBEBEBFF
        }

        .background-white {
            background-color: white;
        }
    </style>
</head>
<body>

<?php
    $indexLink = 'http://localhost/project-php-itplus/test/index.php';

    if (!isset($_SESSION['user'])){
        $actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $loginLink = 'http://localhost/project-php-itplus/test/index.php?type=client&page=login';
        if ($actualLink != $loginLink) {
            header('Location: ' . $loginLink);

            exit;
        }
    }

    include 'View/client/header.php';

    $type = 'client';

    if (isset($_GET['type'])) {
        $type = $_GET['type'];

        if (isset($_GET['type'])) {
            $page = $_GET['page'];
            ?>
                <div class="container" style="padding-top: 170px">
                    <?php include 'View/' . $type . '/' . $page . '.php'; ?>
                </div>
            <?php
        } else {
        ?>
            <div class="container">
                <div class="well">
                    <p>404 - Cannot find this page !</p>
                </div>
            </div>
        <?php
        }
    } else {
    ?>
        <div class="container" style="padding: 70px 0px">
            <?php include 'View/client/index.php'; ?>
        </div>
    <?php
    }

    include 'View/client/footer.php';

    include 'Asset/asset_js.php'
?>

</body>
</html>
