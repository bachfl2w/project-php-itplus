<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<html>
<head>

  <meta charset="UTF-8">
  <title>Trang Chủ Của Quản Trị Viên</title>

  <link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>

  <link rel="stylesheet" type="text/css" href="css/style3.css?v=1">
  
  <link href='http://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 

</head>
<body>

<?php
if(!isset($_SESSION['admin']))
{
	header('Location: FormLogin.php');
	}

	
?>

<?php
     
    include('Views/LeftSideBar.php');
    include('Views/Top-Nav.php'); 	   	   
	   
	   $action = '';
	   $function='';
	   if(isset($_GET['function']))
			$function = $_GET['function'];
		
		//cau truc switch/case ben ngoai dung de lua chon function	
		switch($function) {
			
			case 'trangtinchung':
			{
				include('Views/Rightside-content.php');
				break;
				}//hết Case Trang Tin Chung
			
			
			case 'nhasanxuat': {
				include('Controller/NSXController.php');
	            $NSXController = new NSXController();
				include('Views/SwitchCase/NhaSanXuat.php');					
				break;
			}//Kết Thúc Case  Nhà Sản Xuất
			
			case 'loaisanpham': {
				include('Controller/LSPController.php');
	            $LSPController = new LSPController();
				include('Views/SwitchCase/LoaiSanPham.php');					
				break;
			}
			case 'sanphamchung':{  	
			    include('Controller/SPCController.php');
	            $SPCController = new SPCController();						
				include('Views/SwitchCase/SanPhamChung.php');	
				break;
			}//Kết Thúc Case Sản Phẩm Chung
			
			case 'laptop':{
				include('Controller/LaptopController.php');
	            $LaptopController = new LaptopController(); 
				include('Views/SwitchCase/Laptop.php');					
				break;
			}//Kết Thúc Case Laptop
			
	        case 'phukien':{
				include('Controller/PhuKienController.php');
	            $PKController = new PhuKienController();
				include('Views/SwitchCase/PhuKien.php');						
				break;
			}	//Kết thúc Case Phụ Kiện
			
			case 'baohanh':{	
			     include('Controller/BaoHanhController.php');
	             $BaoHanhController = new BaoHanhController();			    					
			     include('Views/SwitchCase/BaoHanh.php');			
			     break;
			}	//Kết thúc Bảo Hành
			
			case 'khachhang': {
				 include('Controller/KHController.php');
	             $KHController = new KHController();
				 include('Views/SwitchCase/KhachHang.php');						
				 break;
			}//Kết Thúc Case Khách hàng
			
			case 'admin': {
				  include('Controller/ADController.php');
	              $ADController = new ADController();
				  include('Views/SwitchCase/Admin.php');						
				  break;
			}//Kết Thúc Case Admin 
			
			case 'image': {		
			      include('Controller/ImageController.php');
	              $ImageController = new ImageController();		    
			      include('Views/SwitchCase/Image.php');			
				  break;
			}//Kết thúc case image
			
			case 'tintuc': {
				  include('Controller/TinTucController.php');
	              $TinTucController = new TinTucController();
				  include('Views/SwitchCase/TinTuc.php');						
				  break;
			}//Kết thúc case Tin Tuc
				
			case 'hoidap':{
				  include('Controller/HoiDapController.php');
               	  $HoiDapController = new HoiDapController();
				  include('Views/SwitchCase/HoiDap.php');						
			      break;
			}//Kết thúc case Hoi Dap
			case 'quangcao':{
				  include('Controller/QCController.php');
               	  $QCController = new QCController();
				  include('Views/SwitchCase/QuangCao.php');						
			      break;
			}//Kết thúc case Quảng Cáo
			case 'dondathang':{
				  include('Controller/DDHController.php');
	              $DDHController = new DDHController();
				  include('Views/SwitchCase/DonDatHang.php');						
			      break;
			}//Kết thúc case Đơn Hàng
			case 'ctdondathang':{
				  include('Controller/CTDDHController.php');
	              $CTDDHController = new CTDDHController();
				  include('Views/SwitchCase/CTDonDatHang.php');						
			      break;
			}//Kết thúc case Đơn Hàng
			case 'doanhthu':{
				  include('Controller/DTController.php');
	              $DTController = new DTController();
				  include('Views/SwitchCase/DoanhThu.php');						
			      break;
			}//Kết thúc case Đơn Hàng								
	 		default:
				include('Views/Rightside-content.php');
				break;
		}
		
		exit();		 
	   
	?>






</body>
</html>