<!-- them class well va panel de canh giua va lam dep -->
<div class="rightside well">
  <div class="panel panel-primary">
          <div class="panel-heading">Đơn Đặt Hàng</div>
          <table border="1" class="table table-success table-striped">
            <thread>
            <tr>
                <th>Mã Đơn Hàng	</th>
                <th>Khách Hàng</th>
                <th>Người Nhận</th>
                <th>Địa Chỉ</th>
                <th>Số Điện Thoại</th>
                <th>Ngày Đặt Hàng</th>
                <th>Ngày Nhận Hàng</th>
                <th>Trạng Thái</th>
                <th>Xóa</th>
                <th>Sửa</th>
                <th>Chi Tiết</th>
                
            </tr>        
            </thread>
                <tbody>
                <?php 
				   foreach($recordset as $r)
				   {
				?>
                   <tr>
                       <td><?php echo $r->DDH_Ma ; ?></td>
                       <td><?php echo $r->KH_HoTen ; ?></td>
                       <td><?php echo $r->DDH_NguoiNhan ; ?></td>
                       <td><?php echo $r->DDH_DiaChi ; ?></td>
                       <td><?php echo $r->DDH_SDT ; ?></td>
                       <td><?php echo $r->DDH_NgayDat ; ?></td>
                       <td><?php echo $r->DDH_NgayNhan ; ?></td>
                       <td><?php if($r->DDH_TinhTrang == 0 ) {echo 'Đang Xử Lý';} else if($r->DDH_TinhTrang == 1) {echo 'Đang Gửi Hàng';}else if($r->DDH_TinhTrang == 2) {echo 'Đã Nhận Hàng';}  ;?></td>
                       
                       <td><a href="index.php?function=dondathang&action=delete&id=<?php echo $r->DDH_Ma ; ?>" onClick="return confirm('Bạn Có Muốn Xóa Dòng Này ?');">Xóa</a></td>
                       <td><a href="index.php?function=dondathang&action=viewedit&id=<?php echo $r->DDH_Ma ; ?>">Sửa</a></td>
                       <td><a href="index.php?function=dondathang&action=viewdetail&id=<?php echo $r->DDH_Ma ; ?>">Chi Tiết</a></td>
                   </tr>
              
                <?php
				   }
				?>
                
                <tr>
                    <td colspan="11" align="center">
                    <a href="index.php?function=dondathang&action=viewadd"><b>Thêm Mới </b></a>
                    </td>
                </tr>
                </tbody>
          </table>   

  </div>
         <ul class="pagination">
              <?php
			    include('Pagination.php');
				    
				if($curent_page >1 && $total_page >1)
		        {
			    echo '<li><a href="index.php?function=dondathang&action=getlist&page='.($curent_page - 1).'">Prev</a></li>';
			    }	
			
                //Hiển Thị SỐ Trang
			    for($i=1 ;$i<=$total_page ;$i++)
				{
					if($curent_page == $i)					
						echo '<li class="active"><a href="index.php?function=dondathang&action=getlist&page='.$i.'">'.$i.'</a></li>';					
					else
					    echo '<li><a href="index.php?function=dondathang&action=getlist&page='.$i.'">'.$i.'</a></li>';
					
					}
						
				if($curent_page <$total_page && $total_page >1)
		         {
				 echo '<li><a href="index.php?function=dondathang&action=getlist&page='.($curent_page +1).'">Next</a></li>';
				 }								  
			 ?>
          
          </ul>   
</div>