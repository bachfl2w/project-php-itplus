<?php
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=mb_substr($x,0,$length) . '...';
    echo $y;
  }
}
?>
<div class="rightside well">
  <div class="panel panel-primary">
          <div class="panel-heading">Hỏi - Đáp</div>
          <table border="1" align="center" class="table table-striped">
            <thread>
            <tr>
                <th>Mã</th>
                <th>Tiêu Đề</th>      
                <th>Email Người Gửi</th>  
                <th>Nội Dung Câu Hỏi</th> 
                <th>Câu Trả Lời</th> 
                <th>Ngày Gửi</th> 
                <th>Ngày Phản Hồi</th>
                <th>Trạng Thái</th>        
                <th>Xóa</th>
                <th>Sửa</th>
                
            </tr>        
            </thread>
                <tbody>
                <?php 
				   foreach($recordset as $r)
				   {
				?>
                   <tr>
                       <td><?php echo $r->HD_Ma ; ?></td>
                       <td><?php echo $r->HD_TieuDe ; ?></td>
                       <td><?php echo $r->Email ; ?></td>
                       <td><?php custom_echo($r->HD_NoiDung,200); ?></td>
                       <td><?php custom_echo($r->HD_CauTraLoi,200); ?></td>
                       <td><?php echo $r->HD_NgayGui ; ?></td>
                       <td><?php echo $r->HD_NgayPhanHoi ;?></td>
                       <td><?php if($r->HD_TrangThai == 1)
					                {echo 'Đã Phản Hồi';}
								 else
								    echo 'Chưa Phản Hồi'; ?></td>
                       
                       <td><a href="index.php?function=hoidap&action=delete&id=<?php echo $r->HD_Ma ; ?>" onClick="return confirm('Bạn Có Muốn Xóa Dòng Này ?');">Xóa</a></td>
                       <td><a href="index.php?function=hoidap&action=viewedit&id=<?php echo $r->HD_Ma ; ?>">Sửa</a></td>
                   </tr>
              
                <?php
				   }
				?>
                
                <tr>
                    <td colspan="11" align="center">
                    <a href="index.php?function=hoidap&action=viewadd"><b>Thêm Mới Hỏi-Đáp</b></a>
                    </td>
                </tr>
                </tbody>
          </table>   

</div>
          <ul class="pagination">
              <?php
			    include('Pagination.php');
				    
				if($curent_page >1 && $total_page >1)
		        {
			    echo '<li><a href="index.php?function=hoidap&action=getlist&page='.($curent_page - 1).'">Prev</a></li>';
			    }	
			
                //Hiển Thị SỐ Trang
			    for($i=1 ;$i<=$total_page ;$i++)
				{
					if($curent_page == $i)					
						echo '<li class="active"><a href="index.php?function=hoidap&action=getlist&page='.$i.'">'.$i.'</a></li>';					
					else
					    echo '<li><a href="index.php?function=hoidap&action=getlist&page='.$i.'">'.$i.'</a></li>';
					
					}
						
				if($curent_page <$total_page && $total_page >1)
		         {
				 echo '<li><a href="index.php?function=hoidap&action=getlist&page='.($curent_page +1).'">Next</a></li>';
				 }								  
			 ?>
          
          </ul>
</div>