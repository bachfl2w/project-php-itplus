<?php
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=mb_substr($x,0,$length,'utf-8') . '...';
    echo $y;
  }
}
?>
<div class="rightside well">
  <div class="panel panel-primary">
          <div class="panel-heading"><h4>Sản Phẩm Phụ Kiện</h4></div>
          <table border="2" align="center" class="table table-striped">
            <thread>
            <tr>
                <th>Mã SP</th>
                <th>Tên</th>
                <th>Nhà Sản Xuất</th>
                <th>Màu Sắc</th>    
                <th>DPI</th>  
                <th>Độ Bền(Số Click)</th>  
                <th>Loại Chuột</th>  
                <th>Chi Tiết</th>                             
                <th>Xóa</th>
                <th>Sửa</th>
                
            </tr>        
            </thread>
                <tbody>
                <?php 
				   foreach($recordset as $r)
				   {
				?>
                   <tr>
                       <td><?php echo $r->SP_Ma ; ?></td>
                       <td><?php echo $r->SP_Ten ; ?></td>
                       <td><?php echo $r->NSX_Ten ; ?></td>
                       <td><?php echo $r->PK_MauSac ; ?></td>
                       <td><?php echo $r->PK_DPI ; ?></td>
                       <td><?php echo $r->PK_DoBen ; ?></td>
                       <td><?php echo $r->PK_LoaiChuot ; ?></td>
                       <td><?php custom_echo($r->PK_MoTaChiTiet,200) ; ?></td>                       
                       <td><a href="index.php?function=phukien&action=delete&id=<?php echo $r->SP_Ma ; ?>" onClick="return confirm('Bạn Có Muốn Xóa Dòng Này ?');">Xóa</a></td>
                       <td><a href="index.php?function=phukien&action=viewedit&id=<?php echo $r->SP_Ma ; ?>">Sửa</a></td>
                   </tr>
              
                <?php
				   }
				?>
                
                <tr>
                    <td colspan="11" align="center">
                    <a href="index.php?function=phukien&action=viewadd"><b>Thêm Mới Phụ Kiện</b></a>
                    </td>
                </tr>
                </tbody>
          </table>   


</div>

      <ul class="pagination">
              <?php
			    include('Pagination.php');
				    
				if($curent_page >1 && $total_page >1)
		        {
			    echo '<li><a href="index.php?function=phukien&action=getlist&page='.($curent_page - 1).'">Prev</a></li>';
			    }	
			
                //Hiển Thị SỐ Trang
			    for($i=1 ;$i<=$total_page ;$i++)
				{
					if($curent_page == $i)					
						echo '<li class="active"><a href="index.php?function=phukien&action=getlist&page='.$i.'">'.$i.'</a></li>';					
					else
					    echo '<li><a href="index.php?function=phukien&action=getlist&page='.$i.'">'.$i.'</a></li>';
					
					}
						
				if($curent_page <$total_page && $total_page >1)
		         {
				 echo '<li><a href="index.php?function=phukien&action=getlist&page='.($curent_page +1).'">Next</a></li>';
				 }								  
			 ?>
          
          </ul>
</div>