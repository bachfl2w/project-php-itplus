<?php
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y= mb_substr($x,0,$length,'utf-8') . '...';
    echo $y;
  }
}
?>
<div class="rightside well">
  <div class="panel panel-primary">
          <div class="panel-heading">Laptop</div>
         
          <table border="1" align="center" class="table table-striped">
            <thread>
            <tr>
                <th>Mã Sản Phẩm</th>
                <th>CPU</th>
                <th>RAM</th>
                <th>Màn Hình</th>
                <th>Card MH</th>
                <th>Hệ Điều Hành</th>
                <th>Bộ Nhớ Đệm</th>
                <th>Ổ Cứng</th>
                <th>Ổ Đĩa</th>
                <th>Cổng Kết Nối</th>
                <th>Âm Thanh</th>
                <th>Pin	</th>
                <th>Chức Năng Khác</th>
                <th>Mô Tả</th>
                <th>Xóa</th>
                <th>Sửa</th>
                
            </tr>        
            </thread>
                <tbody>
                <?php 
				   foreach($recordset as $r)
				   {
				?>
                   <tr>
                       <td><?php echo $r->SP_Ma ; ?></td>
                       <td><?php echo $r->Lap_CPU ; ?></td>
                       <td><?php echo $r->Lap_RAM ; ?></td>
                       <td><?php echo $r->Lap_ManHinh ; ?></td>
                       <td><?php echo $r->Lap_CardMH ; ?></td>
                       <td><?php echo $r->Lap_HDH ; ?></td>
                       <td><?php echo $r->Lap_BoNhoDem ; ?></td>
                       <td><?php echo $r->Lap_OCung ; ?></td>
                       <td><?php echo $r->Lap_ODia ; ?></td>
                       <td><?php echo $r->Lap_CongKetNoi ; ?></td>
                       <td><?php echo $r->Lap_AmThanh ; ?></td>
                       <td><?php echo $r->Lap_Pin; ?></td>
                       <td><?php echo $r->Lap_ChucNangKhac ; ?></td>
                       <td><?php custom_echo($r->Lap_MoTa,83 ); ?></td>
                       
                       <td><a href="index.php?function=laptop&action=delete&id=<?php echo $r->SP_Ma ; ?>" onClick="return confirm('Bạn Có Muốn Xóa Dòng Này ?');">Xóa</a></td>
                       <td><a href="index.php?function=laptop&action=viewedit&id=<?php echo $r->SP_Ma ; ?>">Sửa</a></td>
                   </tr>
              
                <?php
				   }
				?>
                
                <tr>
                    <td colspan="18" align="center">
                    <a href="index.php?function=laptop&action=viewadd"><b>Thêm Mới Laptop</b></a>
                    </td>
                </tr>
                </tbody>
          </table>   


</div>
            <ul class="pagination">
              <?php
			    include('Pagination.php');
				    
				if($curent_page >1 && $total_page >1)
		        {
			    echo '<li><a href="index.php?function=laptop&action=getlist&page='.($curent_page - 1).'">Prev</a></li>';
			    }	
			
                //Hiển Thị SỐ Trang
			    for($i=1 ;$i<=$total_page ;$i++)
				{
					if($curent_page == $i)					
						echo '<li class="active"><a href="index.php?function=laptop&action=getlist&page='.$i.'">'.$i.'</a></li>';					
					else
					    echo '<li><a href="index.php?function=laptop&action=getlist&page='.$i.'">'.$i.'</a></li>';
					
					}
						
				if($curent_page <$total_page && $total_page >1)
		         {
				 echo '<li><a href="index.php?function=laptop&action=getlist&page='.($curent_page +1).'">Next</a></li>';
				 }								  
			 ?>
          
          </ul>
</div>