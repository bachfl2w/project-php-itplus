<div class="rightside well">
  <div class="panel panel-primary">
          <div class="panel-heading"><h4>Bảo Hành</h4></div>
          <table border="2" align="center" class="table table-striped">
            <thread>
            <tr>
                <th>Mã BH</th>
                <th>Mã Đơn Đặt Hàng</th>
                <th>Thời Hạn BH</th>
                <th>Ngày Bắt Đầu</th>
                <th>Ngày Xong Dự Kiến</th>
                <th>Phí BH</th>
                <th>Hình Thức</th>
                <th>Mô Tả + Lưu Ý</th>
                <th>Xóa</th>
                <th>Sửa</th>
                
            </tr>        
            </thread>
                <tbody>
                <?php 
				   foreach($recordset as $r)
				   {
				?>
                   <tr>
                       <td><?php echo $r->BH_Ma ; ?></td>
                       <td><?php echo $r->DDH_Ma ; ?></td>
                       <td><?php echo $r->BH_ThoiHanBH ; ?></td>
                       <td><?php echo $r->BH_NgayBD ; ?></td>
                       <td><?php echo $r->BH_NgayHT ; ?></td>
                       <td><?php echo $r->BH_Phi ; ?></td>
                       <td><?php echo $r->BH_HinhThuc ; ?></td>
                       <td><?php echo $r->BH_Mota ; ?></td>
                       <td><a href="index.php?function=baohanh&action=delete&id=<?php echo $r->BH_Ma ; ?>" onClick="return confirm('Bạn Có Muốn Xóa Dòng Này ?');">Xóa</a></td>
                       <td><a href="index.php?function=baohanh&action=viewedit&id=<?php echo $r->BH_Ma ; ?>">Sửa</a></td>
                   </tr>
              
                <?php
				   }
				?>
                
                <tr>
                    <td colspan="10" align="center">
                    <a href="index.php?function=baohanh&action=viewadd"><b>Thêm Mới Bảo Hành</b></a>
                    </td>
                </tr>
                </tbody>
          </table>   

</div>

          <ul class="pagination">
              <?php
			    include('Pagination.php');
				    
				if($curent_page >1 && $total_page >1)
		        {
			    echo '<li><a href="index.php?function=baohanh&action=getlist&page='.($curent_page - 1).'">Prev</a></li>';
			    }	
			
                //Hiển Thị SỐ Trang
			    for($i=1 ;$i<=$total_page ;$i++)
				{
					if($curent_page == $i)					
						echo '<li class="active"><a href="index.php?function=baohanh&action=getlist&page='.$i.'">'.$i.'</a></li>';					
					else
					    echo '<li><a href="index.php?function=baohanh&action=getlist&page='.$i.'">'.$i.'</a></li>';
					
					}
						
				if($curent_page <$total_page && $total_page >1)
		         {
				 echo '<li><a href="index.php?function=baohanh&action=getlist&page='.($curent_page +1).'">Next</a></li>';
				 }								  
			 ?>
          
          </ul>
</div>