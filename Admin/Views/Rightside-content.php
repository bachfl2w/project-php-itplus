<?php
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=mb_substr($x,0,$length,'utf8') . '...';
    echo $y;
  }
}
?>

<div class="rightside">
  
  <div class="container-fluid row" style="padding-top: 2%">
    
    <div class="col-sm-6">
      <a href="index.php?function=hoidap">
      <div class="well well-sm">
        <ul>
          <li><i class="fa fa-envelope-o" aria-hidden="true"></i>
              <b>Tất Cả Tin Nhắn</b>
          </li>
        </ul>
      </div>
      </a>
    </div>
    <div class="col-sm-6">
    <a href="index.php?function=doanhthu">
      <div class="well well-sm">
        <ul>
          <li><i class="fa fa-usd" aria-hidden="true"></i>
              <b>Doanh Thu</b>
          </li>
        </ul>
      </div>
    </div> 
           </a>  
  </div><!--End Div container-fluid row-->

          <div class="container-fluid row">
            <div class="col-sm-12" >
              <div class="well well-sm" style="height: 40px; background-color: white">
                  <b>Tin Nhắn Của Khách Hàng :</b>
              </div>
            </div>
   
   <div class="container-fluid row">
      <div class="col-sm-12" >
          <div class="well well-sm" style="height: 545px; background-color: white">
           
           <table class="table table-striped" >
                <thead>
                  <tr>
                    <th>Mã</th>
                    <th>Email Người Gửi</th>
                    <th>Ngày Gửi</th> 
                    <th>Nội Dung Câu Hỏi</th>
                    <th>Trạng Thái</th>
                    <th> Chi Tiết</th>
                  </tr>
                </thead>
            <?php
			  include('Views/Connect/Connect.php');
			  $sql = 'SELECT *,DATE_FORMAT(HD_NgayGui,"%d-%m-%Y") AS HD_NgayGui FROM `hoi_dap` ORDER by HD_NgayGui DESC Limit 0,8 ';
			  $records = mysql_query($sql);
			  while($row = mysql_fetch_array($records)	){	
			?>   
             
                <tbody>
                    <tr>
                       <td><?php echo $row['HD_Ma']; ?></td>
                       <td><?php echo $row['Email']; ?></td>
                       <td><?php echo $row['HD_NgayGui'	]; ?></td>
                       <td><?php custom_echo($row['HD_NoiDung'], 70); ?></td>
                       <td 
					   <?php if($row['HD_TrangThai'] == 1) {echo 'class="text-success"';} else {echo 'class="text-danger"';}?>>
                       <b>
					   <?php if($row['HD_TrangThai'] == 1) {echo 'Đã Phản Hồi';} else {echo 'Chưa Phản Hồi';} ?> 
                       </b>
                       </td>
                       <td><a href="index.php?function=hoidap&action=viewdetail&id=<?php echo $row['HD_Ma']; ?>">Chi Tiết</a></td>                       
                    </tr>
                </tbody>
           <?php
			  }
		  ?> 
          </table>
           
            
          </div>
      </div>   
   </div>         
            
  
  </div><!--End Div rightside-content-->