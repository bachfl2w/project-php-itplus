
<aside class="sidebar">
  <div id="leftside-navigation" class="nano">
    <ul class="nano-content">
      <li class="sub-menu active">
        <a href="index.php?function=trangtinchung"><i class="fa fa-dashboard"></i><span>Trang Tin Chung</span></a>
      </li>
      
    <li class="sub-menu">
        <a href="index.php?function=nhasanxuat"><i class="fa fa-user"></i><span>Nhà Sản Xuất</span><i class="arrow fa fa-angle-right pull-right"></i></a>
      </li>
   
    <li class="sub-menu">
        <a href="#"><i class="fa fa-tasks"></i><span>Quản Lý Sản Phẩm</span><i class="arrow fa fa-angle-right pull-right"></i></a>
        <ul>
              <li class="active">
                 <a href="index.php?function=sanphamchung">Sản Phẩm Chung</a>
              </li>   
              <li>
                 <a href="index.php?function=loaisanpham">Loại Sản Phẩm</a>
              </li>
              <li>
                 <a href="index.php?function=laptop">Laptop</a>
              </li>
              <li>
                 <a href="index.php?function=phukien">Phụ Kiện</a>
              </li>
              <li>
                 <a href="index.php?function=baohanh">Bảo Hành</a>
              </li>
              <li>
                 <a href="index.php?function=image">Ảnh</a>
              </li>
        </ul>
      </li>
      
      <li class="sub-menu">
        <a href="#"><i class="fa fa-user"></i><span>Quản Lý Tài Khoản</span><i class="arrow fa fa-angle-right pull-right"></i></a>
             <ul>
                  <li>
                     <a href="index.php?function=admin">Admin</a>
                  </li>   
                  <li>
                     <a href="index.php?function=khachhang">Khách Hàng</a>
                  </li>              
             </ul> 
      </li>
      
      <li class="sub-menu">
        <a href="#"><i class="fa fa-map-marker"></i><span>Quản Lý Đơn Hàng</span><i class="arrow fa fa-angle-right pull-right"></i></a>
            <ul>
                  <li>
                     <a href="index.php?function=dondathang">Đơn Đặt hàng</a>
                  </li>   
                  <li>
                     <a href="index.php?function=ctdondathang">Chi Tiết Đơn Đặt Hang</a>
                  </li>              
             </ul> 
      </li>
      
      <li class="sub-menu">
        <a href="index.php?function=tintuc"><i class="fa fa-bar-chart-o"></i><span>Tin Tức</span><i class="arrow fa fa-angle-right pull-right"></i></a>
        
      </li>
      <li class="sub-menu">
        <a href="index.php?function=quangcao"><i class="fa fa-bar-chart-o"></i><span>Quảng Cáo</span><i class="arrow fa fa-angle-right pull-right"></i></a>
         
      </li>
      <li class="sub-menu">
        <a href="index.php?function=hoidap"><i class="fa fa-envelope-o"></i><span>Hỏi Đáp</span><i class="arrow fa fa-angle-right pull-right"></i></a>
         
      </li>
      
      <li class="sub-menu">
        <a href="index.php?function=doanhthu"><i class="fa fa-money"></i><span>Doanh Thu</span><i class="arrow fa fa-angle-right pull-right"></i></a>
        
      </li>
      
    </ul>
  </div>
</aside>