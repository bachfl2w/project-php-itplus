<?php
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=mb_substr($x,0,$length,'utf-8') . '...';
    echo $y;
  }
}
?>
<div class="rightside well">
  <div class="panel panel-primary">
          <div class="panel-heading">Tin Tức Chung</div>
          <table border="1" align="center" class="table table-striped">
            <thread>
            <tr>
                <th>Mã</th>
                <th>Tiêu Đề </th>      
                <th>Ngày Viết</th> 
                <th>Nội Dung</th>
                <th>Hình Ảnh</th>        
                <th>Xóa</th>
                <th>Sửa</th>
                
            </tr>        
            </thread>
                <tbody>
                <?php 
				   foreach($recordset as $r)
				   {
				?>
                   <tr>
                       <td><?php echo $r->TT_Ma ; ?></td>
                       <td><?php echo $r->TT_TieuDe ; ?></td>
                       <td><?php echo $r->TT_NgayViet ; ?></td>
                       <td><?php custom_echo($r->TT_NoiDung ,400); ?></td>      
                       <td><img width="200" height="100" src="images/<?php echo $r->TT_HinhAnh ; ?>"</td>
                       
                       <td><a href="index.php?function=tintuc&action=delete&id=<?php echo $r->TT_Ma ; ?>" onClick="return confirm('Bạn Có Muốn Xóa Dòng Này ?');">Xóa</a></td>
                       <td><a href="index.php?function=tintuc&action=viewedit&id=<?php echo $r->TT_Ma ; ?>">Sửa</a></td>
                   </tr>
              
                <?php
				   }
				?>
                
                <tr>
                    <td colspan="9" align="center">
                    <a href="index.php?function=tintuc&action=viewadd"><b>Thêm Mới</b></a>
                    </td>
                </tr>
                </tbody>
          </table>   

</div>
         <ul class="pagination">
              <?php
			    include('Pagination.php');
				    
				if($curent_page >1 && $total_page >1)
		        {
			    echo '<li><a href="index.php?function=tintuc&action=getlist&page='.($curent_page - 1).'">Prev</a></li>';
			    }	
			
                //Hiển Thị SỐ Trang
			    for($i=1 ;$i<=$total_page ;$i++)
				{
					if($curent_page == $i)					
						echo '<li class="active"><a href="index.php?function=tintuc&action=getlist&page='.$i.'">'.$i.'</a></li>';					
					else
					    echo '<li><a href="index.php?function=tintuc&action=getlist&page='.$i.'">'.$i.'</a></li>';
					
					}
						
				if($curent_page <$total_page && $total_page >1)
		         {
				 echo '<li><a href="index.php?function=tintuc&action=getlist&page='.($curent_page +1).'">Next</a></li>';
				 }								  
			 ?>
          
          </ul>
</div>