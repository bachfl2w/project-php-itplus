<?php
include('Views/SanPham/Laptop/Pagination.php');
				$action = '';
		
				if(isset($_GET['action']))
					$action = $_GET['action'];
				
				//cau truc switch/case ben ngoai dung de lua chon action	
				switch($action)
				 {
					case 'getlist': {
						$LaptopController->getlist($start);
						break;		
					}
					case 'viewadd': {
						$LaptopController->viewadd();
						break;		
					}
					case 'viewedit': {
						$id = $_GET['id'];
						$LaptopController->viewedit($id);
						break;		
					}
					case 'add':{
						 $id = $_POST['txtid'];
						 $CPU = $_POST['txtcpu'];
						 $RAM = $_POST['txtram'];					
						 $ManHinh = $_POST['txtmh'];
						 $CardMH = $_POST['txtcardmh'];
						 $HDH = $_POST['txthdh'];
						 $BoNhoDem = $_POST['txtbonhodem'];
						 $OCung = $_POST['txtocung'];
						 $ODia = $_POST['txtodia'];
						 $CongKetNoi = $_POST['txtcongketnoi'];
						 $AmThanh = $_POST['txtamthanh'];
						 $Pin = $_POST['txtpin'];
						 $ChucNangKhac = $_POST['txtchucnangkhac'];
						 $Mota = $_POST['txtmota'];
						
						 $LaptopController->add($id,$CPU,$RAM,$ManHinh,$CardMH,$HDH,$BoNhoDem,$OCung,$ODia,$CongKetNoi,$AmThanh,$Pin,$ChucNangKhac,$Mota);
						 break;					
					}
					
					case 'edit':{
						 $id = $_POST['txtid'];
						 $CPU = $_POST['txtcpu'];
						 $RAM = $_POST['txtram'];				
						 $ManHinh = $_POST['txtmh'];
						 $CardMH = $_POST['txtcardmh'];
						 $HDH = $_POST['txthdh'];
						 $BoNhoDem = $_POST['txtbonhodem'];
						 $OCung = $_POST['txtocung'];
						 $ODia = $_POST['txtodia'];
						 $CongKetNoi = $_POST['txtcongketnoi'];
						 $AmThanh = $_POST['txtamthanh'];
						 $Pin = $_POST['txtpin'];
						 $ChucNangKhac = $_POST['txtchucnangkhac'];
						 $Mota = $_POST['txtmota'];
						
						 $LaptopController->edit($id,$CPU,$RAM,$ManHinh,$CardMH,$HDH,$BoNhoDem,$OCung,$ODia,$CongKetNoi,$AmThanh,$Pin,$ChucNangKhac,$Mota);
						 break;	
					
					}case 'delete': {
						$id = $_GET['id'];
						$LaptopController->delete($id);
						break;		
					}
					default:
						$LaptopController->getlist($start);
				}
?>