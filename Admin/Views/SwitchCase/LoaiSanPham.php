<?php
   include('Views/SanPham/LoaiSanPham/Pagination.php');
				$action = '';
		
				if(isset($_GET['action']))
					$action = $_GET['action'];
				
				//cau truc switch/case ben ngoai dung de lua chon action	
				switch($action)
				 {
					case 'getlist': {
						$LSPController->getlist($start);
						break;		
					}
					case 'viewadd': {
						$LSPController->viewadd();
						break;		
					}
					case 'viewedit': {
						$id = $_GET['id'];
						$LSPController->viewedit($id);
						break;		
					}
					case 'add':{
					     $name = $_POST['txtname'];						 
						 $inf = $_POST['txtinf'];
						 $LSPController->add($name,$inf);
						 break;
					
					}
					case 'edit':{
						 $id = $_POST['txtid'];
					     $name = $_POST['txtname'];						 
						 $inf = $_POST['txtinf'];
						 $LSPController->edit($id,$name,$inf);
						 break;
					
					}
					case 'delete': {
						$id = $_GET['id'];
						$LSPController->delete($id);
						break;		
					}
					default:
						$LSPController->getlist($start);
				}

?>