<?php
                include('Views/SanPham/SanPhamChung/Pagination.php');
				$action = '';
		
				if(isset($_GET['action']))
					$action = $_GET['action'];
				
				//cau truc switch/case ben ngoai dung de lua chon action	
				switch($action)
				 {
					case 'getlist': {
						$SPCController->getlist($start);
						break;		
					}
					case 'viewadd': {
						$SPCController->viewadd();
						break;		
					}
					case 'viewedit': {
						$id = $_GET['id'];
						$SPCController->viewedit($id);
						break;		
					}
					case 'viewdetail': {
						$id = $_GET['id'];
						$malsp = $_GET['malsp'];
						$SPCController->viewdetail($id,$malsp);
						break;		
					}
					case 'add':{
					     $name = $_POST['txtname'];
						 $productid = $_POST['txtproductid'];
						 $producerid = $_POST['txtproducerid'];
						 $status = $_POST['txtstatus'];							
						 $km = $_POST['txtkm'];
						 $price = $_POST['txtprice'];
						
						 $SPCController->add($productid,$producerid,$name,$status,$km,$price);
						 break;					
					}
					
					case 'edit':{
						 $id = $_POST['txtid'];
					     $name = $_POST['txtname'];
						 $productid = $_POST['txtproductid'];
						 $producerid = $_POST['txtproducerid'];
						 $status = $_POST['txtstatus'];
						 $km = $_POST['txtkm'];
						 $price = $_POST['txtprice'];
						 
						 
						 $SPCController->edit($id,$productid,$producerid,$name,$status,$km,$price);
						 break;
					
					}case 'delete': {
						$id = $_GET['id'];
						$SPCController->delete($id);
						break;		
					}
					default:
						$SPCController->getlist($start);
				}

?>