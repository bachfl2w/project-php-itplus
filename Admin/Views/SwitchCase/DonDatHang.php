<?php
    include('Views/DonDatHang/Pagination.php');
				$action = '';
		
				if(isset($_GET['action']))
					$action = $_GET['action'];
				
				//cau truc switch/case ben ngoai dung de lua chon action	
				switch($action)
				 {
					case 'getlist': {
						$DDHController->getlist($start);
						break;		
					}
					case 'viewadd': {
						$DDHController->viewadd();
						break;		
					}
					case 'viewedit': {
						$id = $_GET['id'];
						$DDHController->viewedit($id);
						break;		
					}
					case 'viewdetail': {
						$id = $_GET['id'];
						$DDHController->viewdetail($id);						
						break;		
					}
					case 'add':{
					     $mkh = $_POST['txtmkh'];
						 $nguoinhan = $_POST['txtnguoinhan'];
						 $diachi = $_POST['txtdiachi'];
						 $sdt = $_POST['txtsdt'];
						 $ndh = $_POST['txtndh'];
						 $nnh = $_POST['txtnnh'];
						 $tt = $_POST['txttt'];
						 $DDHController->add($mkh,$nguoinhan,$diachi,$sdt,$ndh,$nnh,$tt);
						 break;
					
					}
					case 'edit':{
						 $id = $_POST['txtid'];
						 $mkh = $_POST['txtmkh'];
						 $nguoinhan = $_POST['txtnguoinhan'];
						 $diachi = $_POST['txtdiachi'];
						 $sdt = $_POST['txtsdt'];
						 $ndh = $_POST['txtndh'];
						 $nnh = $_POST['txtnnh'];
						 $tt = $_POST['txttt'];
						 $DDHController->edit($id,$mkh,$nguoinhan,$diachi,$sdt,$ndh,$nnh,$tt);
						 break;
					
					}
					case 'delete': {
						$id = $_GET['id'];
						$DDHController->delete($id);
						break;		
					}
					default:
						$DDHController->getlist($start);
						
				}


?>