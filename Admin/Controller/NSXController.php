<?php
include('Model/NSXModel.php');

class NSXController {
		var $model;
		
		public function __construct() {
			$this->model = new NSXModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/NSX/ListNSX.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/NSX/FormAddNSX.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/NSX/FormEditNSX.php');			
			
			}	
		public function add($name,$address,$phone,$email,$web,$inf)
		{
			$this->model->add($name,$address,$phone,$email,$web,$inf);
			$this->getlist(0);
			
			}
		public function edit($id,$name,$address,$phone,$email,$web,$inf)
		{
			$this->model->edit($id,$name,$address,$phone,$email,$web,$inf);
			$this->getlist(0);
			
			}		

}
?>