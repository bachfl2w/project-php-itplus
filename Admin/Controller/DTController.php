<?php
include('Model/DTModel.php');

class DTController {
		var $model;
		
		public function __construct() {
			$this->model = new DTModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/DoanhThu/ListDT.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function getbyid($id) {
			$recordset = $this->model->getbyid($id);
			include('Views/DoanhThu/ListDT.php');
		}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('ViewsDoanhThu/ListDT.php');			
			
			}	

		
}
?>