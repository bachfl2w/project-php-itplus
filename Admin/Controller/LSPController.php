<?php
include('model/LSPModel.php');

class LSPController {
		var $model;
		
		public function __construct() {
			$this->model = new LSPModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/SanPham/LoaiSanPham/ListLSP.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/SanPham/LoaiSanPham/FormAddLSP.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/SanPham/LoaiSanPham/FormEditLSP.php');			
			
			}	
		public function add($name,$inf)
		{
			$this->model->add($name,$inf);
			$this->getlist(0);
			
			}
		public function edit($id,$name,$inf)
		{
			$this->model->edit($id,$name,$inf);
			$this->getlist(0);
			
			}		

}
?>