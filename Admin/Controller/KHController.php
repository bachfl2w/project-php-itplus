<?php
include('Model/KHModel.php');

class KHController {
		var $model;
		
		public function __construct() {
			$this->model = new KHModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/TaiKhoan/KhachHang/ListKH.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/TaiKhoan/KhachHang/FormAddKH.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/TaiKhoan/KhachHang/FormEditKH.php');			
			
			}	
		public function add($hoten,$diachi,$sdt,$email,$tk,$mk,$trangthai)
		{
			$this->model->add($hoten,$diachi,$sdt,$email,$tk,$mk);
			$this->getlist(0);
			
			}
		public function edit($id,$hoten,$diachi,$sdt,$email,$tk,$mk,$trangthai)
		{
			$this->model->edit($id,$hoten,$diachi,$sdt,$email,$tk,$mk,$trangthai);
			$this->getlist(0);
			
			}		

}
?>