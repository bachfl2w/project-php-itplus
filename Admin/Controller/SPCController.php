<?php
include('Model/SPCModel.php');

class SPCController {
		var $model;
		
		public function __construct() {
			$this->model = new SPCModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/SanPham/SanPhamChung/ListSPChung.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/SanPham/SanPhamChung/FormAddSPChung.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/SanPham/SanPhamChung/FormEditSPChung.php');			
			
			}
		public function viewdetail($id,$malsp)
		{
			if($malsp == 1)
			{
				$recordset = $this->model->getbyidlaptop($id);
				include('Views/SanPham/Laptop/ViewDetail.php');	
				}
			else if($malsp == 2)
			{ 
				$recordset = $this->model->getbyidpk($id);
				include('Views/SanPham/PhuKien/ViewDetail.php');	 
				}
			
			}	
				
		public function add($productid,$producerid,$name,$status,$km,$price)
		{
			$this->model->add($productid,$producerid,$name,$status,$km,$price);
			$this->getlist(0);
			
			}
		public function edit($id,$productid,$producerid,$name,$status,$km,$price)
		{
			$this->model->edit($id,$productid,$producerid,$name,$status,$km,$price);
			$this->getlist(0);
			
			}		

}
?>