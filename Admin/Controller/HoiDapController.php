<?php
include('model/HoiDapModel.php');

class HoiDapController {
		var $model;
		
		public function __construct() {
			$this->model = new HoiDapModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/HoiDap/ListHoiDap.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/HoiDap/FormAddHoiDap.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/HoiDap/FormEditHoiDap.php');			
			
			}	
			
		public function viewdetail($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/HoiDap/ViewDetail.php');			
			
			}		
		public function add($td,$email,$nd,$ctl,$ng,$nph,$tt)
		{
			$this->model->add($td,$email,$nd,$ctl,$ng,$nph,$tt);
			$this->getlist(0);
			
			}
		public function edit($id,$td,$email,$nd,$ctl,$ng,$nph,$tt)
		{
			$this->model->edit($id,$td,$email,$nd,$ctl,$ng,$nph,$tt);
			$this->getlist(0);
			
			}		

}
?>