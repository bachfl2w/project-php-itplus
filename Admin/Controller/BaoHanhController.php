<?php
include('Model/BaoHanhModel.php');

class BaoHanhController {
		var $model;
		
		public function __construct() {
			$this->model = new BaoHanhModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/SanPham/BaoHanh/ListBH.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/SanPham/BaoHanh/FormAddBH.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/SanPham/BaoHanh/FormEditBH.php');			
			
			}	
		public function add($id,$th,$nbd,$nht,$phi,$ht,$mota)
		{
			$this->model->add($id,$th,$nbd,$nht,$phi,$ht,$mota);
			$this->getlist(0);
			
			}
		public function edit($mbh,$id,$th,$nbd,$nht,$phi,$ht,$mota)
		{
			$this->model->edit($mbh,$id,$th,$nbd,$nht,$phi,$ht,$mota);
			$this->getlist(0);
			
			}		

}
?>