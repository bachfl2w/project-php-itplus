<?php
include('model/TinTucModel.php');

class TinTucController {
		var $model;
		
		public function __construct() {
			$this->model = new TinTucModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/TinTuc/ListTT.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/TinTuc/FormAddTT.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/TinTuc/FormEditTT.php');			
			
			}	
		public function add($td,$ngayviet,$nd,$image)
		{
			$this->model->add($td,$ngayviet,$nd,$image);
			$this->getlist(0);
			
			}
		public function edit($id,$td,$ngayviet,$nd,$image)
		{
			$this->model->edit($id,$td,$ngayviet,$nd,$image);
			$this->getlist(0);
			
			}		

}
?>