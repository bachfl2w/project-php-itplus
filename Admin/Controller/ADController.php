<?php
include('Model/ADModel.php');

class ADController {
		var $model;
		
		public function __construct() {
			$this->model = new ADModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/TaiKhoan/Admin/ListAD.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/TaiKhoan/Admin/FormAddAD.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/TaiKhoan/Admin/FormEditAD.php');			
			
			}	
		public function add($hoten,$tk,$mk,$gt,$email,$sdt,$diachi,$pq)
		{
			$this->model->add($hoten,$tk,$mk,$gt,$email,$sdt,$diachi,$pq);
			$this->getlist(0);
			
			}
		public function edit($id,$hoten,$tk,$mk,$gt,$email,$sdt,$diachi,$pq)
		{
			$this->model->edit($id,$hoten,$tk,$mk,$gt,$email,$sdt,$diachi,$pq);
			$this->getlist(0);
			
			}		

}
?>