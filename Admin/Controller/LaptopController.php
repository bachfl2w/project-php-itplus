<?php
include('Model/LaptopModel.php');

class LaptopController {
		var $model;
		
		public function __construct() {
			$this->model = new LaptopModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/SanPham/Laptop/ListLaptop.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/SanPham/Laptop/FormAddLaptop.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/SanPham/Laptop/FormEditLaptop.php');			
			
			}	
		public function add($id,$CPU,$RAM,$ManHinh,$CardMH,$HDH,$BoNhoDem,$OCung,$ODia,$CongKetNoi,$AmThanh,$Pin,$ChucNangKhac ,$Mota)
		{
			$this->model->add($id,$CPU,$RAM,$ManHinh,$CardMH,$HDH,$BoNhoDem,$OCung,$ODia,$CongKetNoi,$AmThanh,$Pin,$ChucNangKhac ,$Mota);
			$this->getlist(0);
			
			}
		public function edit($id,$CPU,$RAM,$ManHinh,$CardMH,$HDH,$BoNhoDem,$OCung,$ODia,$CongKetNoi,$AmThanh,$Pin,$ChucNangKhac ,$Mota)
		{
			$this->model->edit($id,$CPU,$RAM,$OCung,$ManHinh,$CardMH,$HDH,$BoNhoDem,$ODia,$CongKetNoi,$AmThanh,$Pin,$ChucNangKhac ,$Mota);
			$this->getlist(0);
			
			}		

}
?>