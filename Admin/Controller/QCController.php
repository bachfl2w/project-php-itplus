<?php
include('model/QCModel.php');

class QCController {
		var $model;
		
		public function __construct() {
			$this->model = new QCModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/QuangCao/ListQC.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/QuangCao/FormAddQC.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/QuangCao/FormEditQC.php');			
			
			}	
		public function add($td,$nbd,$nkt,$nd,$image)
		{
			$this->model->add($td,$nbd,$nkt,$nd,$image);
			$this->getlist(0);
			
			}
		public function edit($id,$td,$nbd,$nkt,$nd,$image)
		{
			$this->model->edit($id,$td,$nbd,$nkt,$nd,$image);
			$this->getlist(0);
			
			}		

}
?>