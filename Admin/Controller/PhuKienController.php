<?php
include('Model/PhuKienModel.php');

class PhuKienController {
		var $model;
		
		public function __construct() {
			$this->model = new PhuKienModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/SanPham/PhuKien/ListPK.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/SanPham/PhuKien/FormAddPK.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/SanPham/PhuKien/FormEditPK.php');			
			
			}	
		public function add($msp,$mausac,$dpi,$doben,$loaichuot,$des)
		{
			$this->model->add($msp,$mausac,$dpi,$doben,$loaichuot,$des);
			$this->getlist(0);
			
			}
		public function edit($msp,$mausac,$dpi,$doben,$loaichuot,$des)
		{
			$this->model->edit($msp,$mausac,$dpi,$doben,$loaichuot,$des);
			$this->getlist(0);
			
			}		

}
?>