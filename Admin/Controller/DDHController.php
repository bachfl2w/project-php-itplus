<?php
include('Model/DDHModel.php');

class DDHController {
		var $model;
		
		public function __construct() {
			$this->model = new DDHModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/DonDatHang/ListDDH.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		
		public function viewadd()
		{
			include('Views/DonDatHang/FormAddDDH.php');			
			
			}
		public function viewdetail($id)
		{
			$recordset = $this->model->viewdetail($id);
			include('Views/DonDatHang/ChiTietDDH/ViewDetail.php');			
			
			}	
		
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/DonDatHang/FormEditDDH.php');			
			
			}	
		public function add($mkh,$nguoinhan,$diachi,$sdt,$ndh,$nnh,$tt)
		{
			$this->model->add($mkh,$nguoinhan,$diachi,$sdt,$ndh,$nnh,$tt);
			$this->getlist(0);
			
			}
		public function edit($id,$mkh,$nguoinhan,$diachi,$sdt,$ndh,$nnh,$tt)
		{
			$this->model->edit($id,$mkh,$nguoinhan,$diachi,$sdt,$ndh,$nnh,$tt);
			$this->getlist(0);
			
			}		

}
?>