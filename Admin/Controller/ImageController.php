<?php
include('model/ImageModel.php');

class ImageController {
		var $model;
		
		public function __construct() {
			$this->model = new ImageModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/Image/ListImage.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function viewadd()
		{
			include('Views/Image/FormAddImage.php');			
			
			}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/Image/FormEditImage.php');			
			
			}	
		public function add($name,$masp,$lsp)
		{
			$this->model->add($name,$masp,$lsp);
			$this->getlist(0);
			
			}
		public function edit($id,$name,$masp,$lsp)
		{
			$this->model->edit($id,$name,$masp,$lsp);
			$this->getlist(0);
			
			}	
			

}
?>