<?php
include('Model/CTDDHModel.php');

class CTDDHController {
		var $model;
		
		public function __construct() {
			$this->model = new CTDDHModel();
		}
		
		public function getlist($start) {
			$recordset = $this->model->getlist($start);
			
			include('Views/DonDatHang/ChiTietDDH/ListCTDDH.php');
		}
		public function delete($id) {
			$this->model->delete($id);
			
			$this->getlist(0);
		}
		public function getbyid($id) {
			$recordset = $this->model->getbyid($id);
			
		}
		public function viewedit($id)
		{
			$recordset = $this->model->getbyid($id);
			include('Views/DonDatHang/ChiTietDDH/FormEditCTDDH.php');						
			
			}
		public function viewadd()
		{
			include('Views/DonDatHang/ChiTietDDH/FormAddCTDDH.php');						
			}		

        public function add($mdh,$msp,$sl)
		{
			$this->model->add($mdh,$msp,$sl);
			$this->getlist(0);
			
			}	
		
		public function edit($id,$mdh,$msp,$sl)
		{
			$this->model->edit($id,$mdh,$msp,$sl);
			$this->getlist(0);
			
			}		

}
?>