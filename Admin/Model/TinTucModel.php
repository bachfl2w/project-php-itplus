<?php
 class TinTucModel{
	var $con ; 
    public function __construct()
	{
	     $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));				
		}
    public function getlist($start)
	{
		$sql = "select *,DATE_FORMAT(TT_NgayViet,'%d-%m-%Y') AS TT_NgayViet from tin_tuc order by TT_Ma ASC LIMIT :start ,4";
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from tin_tuc where TT_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from tin_tuc where TT_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($td,$ngayviet,$nd,$image)
	{
		$sql = "insert into tin_tuc( TT_TieuDe ,TT_NgayViet,TT_NoiDung ,TT_HinhAnh) values (:td ,:ngayviet ,:nd ,:image )";
		$sta = $this->con->prepare($sql);
		
		
		$sta->bindParam(":td", $td, PDO::PARAM_STR);		
		$sta->bindParam(":ngayviet", $ngayviet, PDO::PARAM_STR);
		$sta->bindParam(":nd", $nd, PDO::PARAM_STR);
		$sta->bindParam(":image", $image, PDO::PARAM_STR);
		$sta->execute();
	} 
	
	
    public function edit($id,$td,$ngayviet,$nd,$image)
	{
		$sql = "update tin_tuc set TT_TieuDe= :td, TT_NgayViet= :ngayviet,TT_NoiDung = :nd ,TT_HinhAnh = :image  where TT_Ma=:id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":td", $td, PDO::PARAM_STR);		
		$sta->bindParam(":ngayviet", $ngayviet, PDO::PARAM_STR);
		$sta->bindParam(":nd", $nd, PDO::PARAM_STR);
		$sta->bindParam(":image", $image, PDO::PARAM_STR);
		$sta->execute();
		
		
	} 
 }


?>