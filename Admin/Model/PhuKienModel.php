<?php
 class PhuKienModel{
	var $con ; 
    public function __construct()
	{
	     $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));				
		}
    public function getlist($start)
	{
		$sql = "select * from san_pham a inner join phu_kien b on a.SP_Ma = b.SP_Ma inner join nha_san_xuat c on a.NSX_Ma = c.NSX_Ma order by a.SP_Ma ASC LIMIT :start ,7";
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from phu_kien where SP_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from phu_kien where SP_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($msp,$mausac,$dpi,$doben,$loaichuot,$des)
	{
		$sql = "insert into phu_kien values (:msp ,:mausac ,:dpi ,:doben ,:loaichuot,:des)";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":msp", $msp, PDO::PARAM_INT);
		$sta->bindParam(":mausac", $mausac, PDO::PARAM_STR);
		$sta->bindParam(":dpi", $dpi, PDO::PARAM_STR);
		$sta->bindParam(":doben", $doben, PDO::PARAM_STR);
		$sta->bindParam(":loaichuot", $loaichuot, PDO::PARAM_STR);
		$sta->bindParam(":des", $des, PDO::PARAM_STR);
		$sta->execute();
	} 
	
	
    public function edit($msp,$mausac,$dpi,$doben,$loaichuot,$des)
	{
		$sql = "update phu_kien set PK_MauSac= :mausac, PK_DPI= :dpi, PK_DoBen= :doben ,PK_LoaiChuot = :loaichuot ,PK_MoTaChiTiet =:des where SP_Ma= :msp";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":msp", $msp, PDO::PARAM_INT);
		$sta->bindParam(":mausac", $mausac, PDO::PARAM_STR);
		$sta->bindParam(":dpi", $dpi, PDO::PARAM_STR);
		$sta->bindParam(":doben", $doben, PDO::PARAM_STR);
		$sta->bindParam(":loaichuot", $loaichuot, PDO::PARAM_STR);
		$sta->bindParam(":des", $des, PDO::PARAM_STR);
		$sta->execute();
	} 
 }


?>