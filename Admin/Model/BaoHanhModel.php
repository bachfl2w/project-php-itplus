<?php
 class BaoHanhModel{
	var $con ; 
    public function __construct()
	{
	     $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));			
		}
    public function getlist($start)
	{
		$sql = "select * from bao_hanh order by BH_Ma ASC LIMIT :start ,4";
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from bao_hanh where BH_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from bao_hanh where BH_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($id,$th,$nbd,$nht,$phi,$ht,$mota)
	{
		$sql = "insert into bao_hanh( DDH_Ma, BH_ThoiHanBH, BH_NgayBD, BH_NgayHT, BH_Phi ,BH_HinhThuc ,BH_Mota) values (:id ,:th ,:nbd ,:nht, :phi ,:ht ,:mota)";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":th", $th, PDO::PARAM_STR);
		$sta->bindParam(":nbd", $nbd, PDO::PARAM_STR);
		$sta->bindParam(":nht", $nht, PDO::PARAM_STR);
		$sta->bindParam(":phi", $phi, PDO::PARAM_STR);
		$sta->bindParam(":ht", $ht, PDO::PARAM_STR);
		$sta->bindParam(":mota", $mota, PDO::PARAM_STR);
		$sta->execute();
	} 
	
	
    public function edit($mbh,$id,$th,$nbd,$nht,$phi,$ht,$mota)
	{
		$sql = "update bao_hanh set DDH_Ma= :id, BH_ThoiHanBH= :th, BH_NgayBD= :nbd, BH_NgayHT= :nht, BH_Phi= :phi, BH_HinhThuc= :ht ,BH_Mota =:mota where BH_Ma=:mbh";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":mbh", $mbh, PDO::PARAM_INT);
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":th", $th, PDO::PARAM_STR);
		$sta->bindParam(":nbd", $nbd, PDO::PARAM_STR);
		$sta->bindParam(":nht", $nht, PDO::PARAM_STR);
		$sta->bindParam(":phi", $phi, PDO::PARAM_STR);
		$sta->bindParam(":ht", $ht, PDO::PARAM_STR);
		$sta->bindParam(":mota", $mota, PDO::PARAM_STR);
		$sta->execute();
	} 
 }


?>