<?php
 class NSXModel{
	var $con,$dsn,$username,$options ; 
    public function __construct()
	{
	     
		$this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));		
		}
    public function getlist($start)
	{
		$sql = "select * from nha_san_xuat order by NSX_Ma ASC LIMIT :start ,5";
		
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from nha_san_xuat where NSX_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from nha_san_xuat where NSX_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($name,$address,$phone,$email,$web,$inf)
	{
		$sql = "insert into nha_san_xuat(NSX_Ten, NSX_DiaChi, NSX_SDT, NSX_Email, NSX_Website, NSX_Thongtin) values (:name ,:address ,:phone ,:email,:web ,:inf)";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":name", $name, PDO::PARAM_STR);
		$sta->bindParam(":address", $address, PDO::PARAM_STR);
		$sta->bindParam(":phone", $phone, PDO::PARAM_INT);
		$sta->bindParam(":email", $email, PDO::PARAM_STR);
		$sta->bindParam(":web", $web, PDO::PARAM_STR);
		$sta->bindParam(":inf", $inf, PDO::PARAM_STR);
		$sta->execute();
	} 
	
	
    public function edit($id,$name,$address,$phone,$email,$web,$inf)
	{
		$sql = "update nha_san_xuat set NSX_Ten=:name, NSX_DiaChi=:address, NSX_SDT=:phone, NSX_Email=:email, NSX_Website=:web, NSX_Thongtin=:inf where NSX_Ma=:id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":name", $name, PDO::PARAM_STR);
		$sta->bindParam(":address", $address, PDO::PARAM_STR);
		$sta->bindParam(":phone", $phone, PDO::PARAM_INT);
		$sta->bindParam(":email", $email, PDO::PARAM_STR);
		$sta->bindParam(":web", $web, PDO::PARAM_STR);
		$sta->bindParam(":inf", $inf, PDO::PARAM_STR);
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
 }


?>