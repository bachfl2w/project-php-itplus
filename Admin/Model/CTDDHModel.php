<?php
 class CTDDHModel{
	var $con,$dsn,$username,$options ; 
    public function __construct()
	{
	     
		$this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));		
		}
    public function getlist($start)
	{
		$sql = "select *,DATE_FORMAT(DDH_NgayNhan ,'%d-%m-%Y') AS DDH_NgayNhan from ct_don_dat_hang a inner join san_pham b on a.SP_Ma= b.SP_Ma inner join don_dat_hang c on a.DDH_Ma = c.DDH_Ma order by a.DDH_Ma ASC LIMIT :start ,6";
		
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from ct_don_dat_hang where ID_CTDDH = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	public function viewdetail($id)
	{
		$sql = "select * from ct_don_dat_hang a inner join san_pham b on a.SP_Ma= b.SP_Ma where a.DDH_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	}
	
	public function delete($id)
	{
		$sql = "delete from ct_don_dat_hang where DDH_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 	
	
	public function add($mdh,$msp,$sl)
	{	
		$sql = "insert into ct_don_dat_hang (DDH_Ma,SP_Ma ,So_Luong ) values ( :mdh ,:msp ,:sl )";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":mdh", $mdh, PDO::PARAM_INT);
		$sta->bindParam(":msp", $msp, PDO::PARAM_INT);
		$sta->bindParam(":sl", $sl, PDO::PARAM_INT);
		
		
		$sta->execute();
	} 
    public function edit($id,$mdh,$msp,$sl)
	{	
		$sql =  "update ct_don_dat_hang set DDH_Ma =:mdh, SP_Ma =:msp ,So_Luong = :sl where ID_CTDDH =:id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":mdh", $mdh, PDO::PARAM_INT);
		$sta->bindParam(":msp", $msp, PDO::PARAM_INT);
		$sta->bindParam(":sl", $sl, PDO::PARAM_INT);
		
		$sta->execute();
	} 
 }


?>