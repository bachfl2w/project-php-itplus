<?php
 class DTModel{
	var $con,$dsn,$username,$options ; 
    public function __construct()
	{
	     
		$this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));		
		}
    public function getlist($start)
	{
		$sql = "select *,DATE_FORMAT(DDH_NgayNhan,'%d-%m-%Y') AS DDH_NgayNhan from ct_don_dat_hang a inner join don_dat_hang b on a.DDH_Ma = b.DDH_Ma inner join san_pham c on a.SP_Ma = c.SP_Ma LIMIT :start ,8 ";
		
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from DT_Ma where DT_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from doanh_thu where DT_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 	
		   
 }


?>