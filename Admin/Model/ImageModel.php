<?php
 class ImageModel{
	var $con ; 
    public function __construct()
	{
	     $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));				
		}
    public function getlist($start)
	{
		$sql = "select * from anh a inner join loai_san_pham b on a.LSP_Ma = b.LSP_Ma  LIMIT :start ,8";		
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from anh where Anh_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from anh where Anh_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($name,$masp,$lsp)
	{
		$sql = "insert into anh( Anh_Ten ,SP_Ma, LSP_Ma) values (:name ,:masp ,:lsp)";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":name", $name, PDO::PARAM_STR);		
		$sta->bindParam(":masp", $masp, PDO::PARAM_INT);
		$sta->bindParam(":lsp", $lsp, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
    public function edit($id,$name,$masp,$lsp)
	{
		$sql = "update anh set Anh_Ten= :name, SP_Ma= :masp ,LSP_Ma =:lsp where Anh_Ma=:id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":name", $name, PDO::PARAM_STR);		
		$sta->bindParam(":masp", $masp, PDO::PARAM_INT);
		$sta->bindParam(":lsp", $lsp, PDO::PARAM_INT);			
		$sta->execute();	
	} 
	
 }


?>