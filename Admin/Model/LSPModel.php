<?php
 class LSPModel{
	var $con ; 
    public function __construct()
	{
	     $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));				
		}
    public function getlist($start)
	{
		$sql = "select * from loai_san_pham order by LSP_Ma ASC LIMIT :start ,4";
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from loai_san_pham where LSP_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from loai_san_pham where LSP_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($name,$inf)
	{
		$sql = "insert into loai_san_pham( LSP_Ten ,LSP_ThongTinThem) values (:name ,:inf)";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":name", $name, PDO::PARAM_STR);		
		$sta->bindParam(":inf", $inf, PDO::PARAM_STR);
		$sta->execute();
	} 
	
	
    public function edit($id,$name,$inf)
	{
		$sql = "update loai_san_pham set LSP_Ten= :name, LSP_ThongtinThem= :inf where LSP_Ma=:id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":name", $name, PDO::PARAM_STR);		
		$sta->bindParam(":inf", $inf, PDO::PARAM_STR);
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
 }


?>