<?php
 class DDHModel{
	var $con,$dsn,$username,$options ; 
    public function __construct()
	{
	     
		$this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));		
		}
    public function getlist($start)
	{
		$sql = "select *,DATE_FORMAT(DDH_NgayDat ,'%d-%m-%Y') AS DDH_NgayDat,DATE_FORMAT(DDH_NgayNhan ,'%d-%m-%Y') AS DDH_NgayNhan from don_dat_hang a inner join khach_hang b on a.KH_Ma = b.KH_Ma  LIMIT :start ,8";
		
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from don_dat_hang where DDH_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	public function viewdetail($id)
	{
		$sql = "select * from ct_don_dat_hang a inner join don_dat_hang b on a.DDH_Ma = b.DDH_Ma inner join san_pham c on a.SP_Ma = c.SP_Ma where a.DDH_Ma= :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from don_dat_hang where DDH_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($mkh,$nguoinhan,$diachi,$sdt,$ndh,$nnh,$tt)
	{
		$sql = "insert into don_dat_hang(KH_Ma,DDH_NguoiNhan,DDH_DiaChi,DDH_SDT, DDH_NgayDat, DDH_NgayNhan, DDH_TinhTrang) values (:mkh ,:nguoinhan,:diachi,:sdt,:ndh ,:nnh ,:tt)";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":mkh", $mkh, PDO::PARAM_INT);
		$sta->bindParam(":nguoinhan", $nguoinhan, PDO::PARAM_INT);
		$sta->bindParam(":diachi", $diachi, PDO::PARAM_INT);
		$sta->bindParam(":sdt", $sdt, PDO::PARAM_INT);
		$sta->bindParam(":ndh", $ndh, PDO::PARAM_STR);
		$sta->bindParam(":nnh", $nnh, PDO::PARAM_STR);
		$sta->bindParam(":tt", $tt, PDO::PARAM_STR);

		$sta->execute();
	} 
	
	
    public function edit($id,$mkh,$nguoinhan,$diachi,$sdt,$ndh,$nnh,$tt)
	{	
		$sql = "update don_dat_hang set KH_Ma=:mkh,DDH_NguoiNhan = :nguoinhan ,DDH_DiaChi =:diachi ,DDH_SDT =:sdt, DDH_NgayDat=:ndh, DDH_NgayNhan=:nnh, DDH_TinhTrang=:tt  where DDH_Ma=:id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":mkh", $mkh, PDO::PARAM_INT);
		$sta->bindParam(":nguoinhan", $nguoinhan, PDO::PARAM_INT);
		$sta->bindParam(":diachi", $diachi, PDO::PARAM_INT);
		$sta->bindParam(":sdt", $sdt, PDO::PARAM_INT);
		$sta->bindParam(":ndh", $ndh, PDO::PARAM_STR);
		$sta->bindParam(":nnh", $nnh, PDO::PARAM_STR);
		$sta->bindParam(":tt", $tt, PDO::PARAM_STR);
		
		$sta->execute();
	} 
 }


?>