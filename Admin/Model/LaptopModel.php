<?php
 class LaptopModel{
	var $con ; 
    public function __construct()
	{
	     $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));			
		}
    public function getlist($start)
	{
		$sql = "select * from sp_laptop order by SP_Ma ASC LIMIT :start ,4";
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from sp_laptop where SP_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from sp_laptop where SP_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($id,$CPU,$RAM,$ManHinh,$CardMH,$HDH,$BoNhoDem,$OCung,$ODia,$CongKetNoi,$AmThanh,$Pin,$ChucNangKhac ,$Mota)
	{
		$sql = "insert into sp_laptop values (:id ,:CPU ,:RAM  ,:ManHinh ,:CardMH,:HDH ,:BoNhoDem ,:OCung ,:ODia ,:CongKetNoi ,:AmThanh ,:Pin ,:ChucNangKhac,:Mota)";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":CPU", $CPU, PDO::PARAM_STR);
		$sta->bindParam(":RAM", $RAM, PDO::PARAM_STR);		
		$sta->bindParam(":ManHinh", $ManHinh, PDO::PARAM_STR);
		$sta->bindParam(":CardMH", $CardMH, PDO::PARAM_STR);
		$sta->bindParam(":HDH", $HDH, PDO::PARAM_STR);
		$sta->bindParam(":BoNhoDem", $BoNhoDem, PDO::PARAM_STR);
		$sta->bindParam(":OCung", $OCung, PDO::PARAM_STR);
		$sta->bindParam(":ODia", $ODia, PDO::PARAM_STR);
		$sta->bindParam(":CongKetNoi", $CongKetNoi, PDO::PARAM_STR);
		$sta->bindParam(":AmThanh", $AmThanh, PDO::PARAM_STR);
		$sta->bindParam(":Pin", $Pin, PDO::PARAM_STR);
		$sta->bindParam(":ChucNangKhac", $ChucNangKhac, PDO::PARAM_STR);
		$sta->bindParam(":Mota", $Mota, PDO::PARAM_STR);
		$sta->execute();
	} 
	
	
    public function edit($id,$CPU,$RAM,$ManHinh,$CardMH,$HDH,$BoNhoDem,$OCung,$ODia,$CongKetNoi,$AmThanh,$Pin,$ChucNangKhac ,$Mota)
	{
		$sql = "update sp_laptop set Lap_CPU = :CPU, Lap_RAM = :RAM, Lap_ManHinh = :ManHinh,Lap_CardMH = :CardMH , Lap_HDH = :HDH ,Lap_BoNhoDem = :BoNhoDem , Lap_OCung = :OCung ,Lap_ODia = :ODia,Lap_CongKetNoi = :CongKetNoi,Lap_AmThanh = :AmThanh,Lap_Pin =:Pin, Lap_ChucNangKhac =:ChucNangKhac ,Lap_MoTa = :Mota where SP_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":CPU", $CPU, PDO::PARAM_STR);
		$sta->bindParam(":RAM", $RAM, PDO::PARAM_STR);		
		$sta->bindParam(":ManHinh", $ManHinh, PDO::PARAM_STR);
		$sta->bindParam(":CardMH", $CardMH, PDO::PARAM_STR);
		$sta->bindParam(":HDH", $HDH, PDO::PARAM_STR);
		$sta->bindParam(":BoNhoDem", $BoNhoDem, PDO::PARAM_STR);
		$sta->bindParam(":OCung", $OCung, PDO::PARAM_STR);
		$sta->bindParam(":ODia", $ODia, PDO::PARAM_STR);
		$sta->bindParam(":CongKetNoi", $CongKetNoi, PDO::PARAM_STR);
		$sta->bindParam(":AmThanh", $AmThanh, PDO::PARAM_STR);
		$sta->bindParam(":Pin", $Pin, PDO::PARAM_STR);
		$sta->bindParam(":ChucNangKhac", $ChucNangKhac, PDO::PARAM_STR);
		$sta->bindParam(":Mota", $Mota, PDO::PARAM_STR);
		$sta->execute();

	} 
 }


?>