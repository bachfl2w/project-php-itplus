<?php
 class KHModel{
	var $con ; 
    public function __construct()
	{
	     $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));			
		}
    public function getlist($start)
	{
		$sql = "select * from khach_hang order by KH_Ma ASC LIMIT :start ,8";
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from khach_hang where KH_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from khach_hang where KH_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($hoten,$diachi,$sdt,$email,$tk,$mk,$trangthai)
	{
		$sql = "insert into khach_hang( KH_HoTen, KH_DiaChi, KH_SDT, KH_Email ,KH_TaiKhoan ,KH_MatKhau,KH_TrangThaiTK) values (:hoten ,:diachi ,:sdt ,:email, :tk ,:mk,:trangthai )";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":hoten", $hoten, PDO::PARAM_INT);
		$sta->bindParam(":diachi", $diachi, PDO::PARAM_STR);
		$sta->bindParam(":sdt", $sdt, PDO::PARAM_STR);
		$sta->bindParam(":email", $email, PDO::PARAM_STR);
		$sta->bindParam(":tk", $tk, PDO::PARAM_STR);
		$sta->bindParam(":mk", $mk, PDO::PARAM_STR);	
		$sta->bindParam(":trangthai", $trangthai, PDO::PARAM_STR);	
		$sta->execute();
	} 
	
	
    public function edit($id,$hoten,$diachi,$sdt,$email,$tk,$mk,$trangthai)
	{
		$sql = "update khach_hang set KH_HoTen =:hoten, KH_DiaChi =:diachi ,KH_SDT = :sdt	, KH_Email =:email ,KH_TaiKhoan =:tk ,KH_MatKhau =:mk ,KH_TrangThaiTK =:trangthai where KH_Ma =:id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":hoten", $hoten, PDO::PARAM_STR);
		$sta->bindParam(":diachi", $diachi, PDO::PARAM_STR);
		$sta->bindParam(":sdt", $sdt, PDO::PARAM_STR);
		$sta->bindParam(":email", $email, PDO::PARAM_STR);
		$sta->bindParam(":tk", $tk, PDO::PARAM_STR);
		$sta->bindParam(":mk", $mk, PDO::PARAM_STR);	
		$sta->bindParam(":trangthai", $trangthai, PDO::PARAM_STR);
		$sta->execute();
	} 
 }


?>