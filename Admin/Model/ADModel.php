<?php
 class ADModel{
	var $con ; 
    public function __construct()
	{
	     $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));			
		}
    public function getlist($start)
	{
		$sql = "select * from admin order by AD_Ma ASC LIMIT :start ,8";
		$sta = $this->con->prepare($sql);
        $sta->bindParam(":start", $start, PDO::PARAM_INT);
 
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from admin where AD_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from admin where AD_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($hoten,$tk,$mk,$gt,$email,$sdt,$diachi,$pq)
	{
		$sql = "insert into admin( AD_HoTen, AD_TaiKhoan, AD_MatKhau , AD_GioiTinh ,AD_Email ,AD_SDT ,AD_DiaChi ,PQ_Ma) values (:hoten ,:tk ,:mk ,:gt, :email ,:sdt ,:diachi ,:pq )";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":hoten", $hoten, PDO::PARAM_STR);
		$sta->bindParam(":tk", $tk, PDO::PARAM_STR);
		$sta->bindParam(":mk", $mk, PDO::PARAM_STR);
		$sta->bindParam(":gt", $gt, PDO::PARAM_STR);
		$sta->bindParam(":sdt", $sdt, PDO::PARAM_STR);
		$sta->bindParam(":email", $email, PDO::PARAM_STR);
		$sta->bindParam(":diachi", $diachi, PDO::PARAM_STR);
		$sta->bindParam(":pq", $pq, PDO::PARAM_STR);		
		$sta->execute();
	} 
	
	
    public function edit($id,$hoten,$tk,$mk,$gt,$email,$sdt,$diachi,$pq)
	{
		$sql = "update admin set AD_HoTen = :hoten, AD_TaiKhoan =:tk , AD_MatKhau = :mk , AD_GioiTinh = :gt,AD_Email =:email ,AD_SDT =:sdt ,AD_DiaChi =:diachi  ,PQ_Ma =:pq where AD_Ma =:id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_STR);
		$sta->bindParam(":hoten", $hoten, PDO::PARAM_STR);
		$sta->bindParam(":tk", $tk, PDO::PARAM_STR);
		$sta->bindParam(":mk", $mk, PDO::PARAM_STR);
		$sta->bindParam(":gt", $gt, PDO::PARAM_STR);
		$sta->bindParam(":sdt", $sdt, PDO::PARAM_STR);
		$sta->bindParam(":email", $email, PDO::PARAM_STR);
		$sta->bindParam(":diachi", $diachi, PDO::PARAM_STR);
		$sta->bindParam(":pq", $pq, PDO::PARAM_STR);		
		$sta->execute();
	} 
 }


?>