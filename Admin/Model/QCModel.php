<?php
 class QCModel{
	var $con ; 
    public function __construct()
	{
	     $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));				
		}
    public function getlist($start)
	{
		$sql = "select * from quang_cao order by QC_Ma ASC LIMIT :start ,8";
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from quang_cao where QC_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from quang_cao where QC_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($td,$nbd,$nkt,$nd,$image)
	{
		$sql = "insert into quang_cao(QC_TieuDe ,QC_NgayBatDau,QC_NgayKetThuc,QC_NoiDung ,QC_HinhAnh) values (:td ,:nbd,:nkt ,:nd ,:image )";
		$sta = $this->con->prepare($sql);
		
		
		$sta->bindParam(":td", $td, PDO::PARAM_STR);		
		$sta->bindParam(":nbd", $nbd, PDO::PARAM_STR);
		$sta->bindParam(":nkt", $nkt, PDO::PARAM_STR);		
		$sta->bindParam(":nd", $nd, PDO::PARAM_STR);
		$sta->bindParam(":image", $image, PDO::PARAM_STR);
		$sta->execute();
	} 
	
	
    public function edit($id,$td,$nbd,$nkt,$nd,$image)
	{
		$sql = "update quang_cao set QC_TieuDe= :td, QC_NgayBatDau= :nbd,QC_NgayKetThuc= :nkt,QC_NoiDung = :nd ,QC_HinhAnh = :image  where QC_Ma=:id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":td", $td, PDO::PARAM_STR);		
		$sta->bindParam(":nbd", $nbd, PDO::PARAM_STR);
		$sta->bindParam(":nkt", $nkt, PDO::PARAM_STR);		
		$sta->bindParam(":nd", $nd, PDO::PARAM_STR);
		$sta->bindParam(":image", $image, PDO::PARAM_STR);
		$sta->execute();
		
		
	} 
 }


?>