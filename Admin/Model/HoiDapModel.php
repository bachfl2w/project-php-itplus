<?php
 class HoiDapModel{
	var $con ; 
    public function __construct()
	{
	     $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));				
		}
    public function getlist($start)
	{
		$sql = "select *,DATE_FORMAT(HD_NgayGui ,'%d-%m-%Y') AS HD_NgayGui,DATE_FORMAT(HD_NgayPhanHoi,'%d-%m-%Y') AS HD_NgayPhanHoi from hoi_dap order by HD_Ma ASC LIMIT :start ,7";
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from hoi_dap where HD_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from hoi_dap where HD_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($td,$email,$nd,$ctl,$ng,$nph,$tt)
	{
		$sql = "insert into hoi_dap(HD_TieuDe ,Email ,HD_NoiDung ,HD_CauTraLoi ,HD_NgayGui ,HD_NgayPhanHoi ,HD_TrangThai) values (:td ,:email ,:nd ,:ctl ,:ng ,:nph ,:tt)";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":td", $td, PDO::PARAM_STR);		
		$sta->bindParam(":email", $email, PDO::PARAM_STR);
		$sta->bindParam(":nd", $nd, PDO::PARAM_STR);
		$sta->bindParam(":ctl", $ctl, PDO::PARAM_STR);
		$sta->bindParam(":ng", $ng, PDO::PARAM_STR);
		$sta->bindParam(":nph", $nph, PDO::PARAM_STR);
		$sta->bindParam(":tt", $tt, PDO::PARAM_STR);
		
		$sta->execute();
	} 
	
	
    public function edit($id,$td,$email,$nd,$ctl,$ng,$nph,$tt)
	{
		$sql = "update hoi_dap set HD_TieuDe =:td, Email=:email , HD_NoiDung = :nd, HD_CauTraLoi= :ctl, HD_NgayGui =:ng , HD_NgayPhanHoi =:nph, HD_TrangThai =:tt where HD_Ma= :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->bindParam(":td", $td, PDO::PARAM_STR);		
		$sta->bindParam(":email", $email, PDO::PARAM_STR);
		$sta->bindParam(":nd", $nd, PDO::PARAM_STR);
		$sta->bindParam(":ctl", $ctl, PDO::PARAM_STR);
		$sta->bindParam(":ng", $ng, PDO::PARAM_STR);
		$sta->bindParam(":nph", $nph, PDO::PARAM_STR);
		$sta->bindParam(":tt", $tt, PDO::PARAM_STR);
	
		$sta->execute();
		
	} 
 }


?>