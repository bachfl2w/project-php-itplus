<?php
 class SPCModel{
	var $con ; 
    public function __construct()
	{
	    $this->con = new PDO('mysql:host=localhost;dbname=shoplaptop','root' ,'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));			
		}
    public function getlist($start)
	{
		$sql = "select *,b.NSX_Ten ,c.LSP_Ten from san_pham a
				inner join nha_san_xuat b ON a.NSX_Ma = b.NSX_Ma 
				inner join loai_san_pham c ON  a.LSP_Ma = c.LSP_Ma order by SP_Ma ASC LIMIT :start ,10" ;
		$sta = $this->con->prepare($sql);
		$sta->bindParam(":start", $start, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
	} 
	
	public function getbyid($id)
	{
		$sql = "select * from san_pham where SP_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	public function getbyidlaptop($id)
	{
		$sql = "select * from sp_laptop where SP_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	public function getbyidpk($id)
	{
		$sql = "select * from san_pham a inner join phu_kien b on a.SP_Ma = b.SP_Ma inner join nha_san_xuat c on a.NSX_Ma = c.NSX_Ma where a.SP_Ma = :id ";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
		
		$recordset = $sta->fetchAll(PDO::FETCH_OBJ);
		return $recordset;
		
	} 
	
	public function delete($id)
	{
		$sql = "delete from san_pham where SP_Ma = :id";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
		$sta->execute();
	} 
	
	
	public function add($productid,$producerid,$name,$status,$km,$price)
	{
		$sql = "insert into san_pham(LSP_Ma, NSX_Ma, SP_Ten, SP_TinhTrang ,SP_KhuyenMai ,SP_GiaNiemYet) values (:lsp_id ,:nsx_id ,:name,:status ,:km  ,:price )";
		$sta = $this->con->prepare($sql);
		
		
		$sta->bindParam(":lsp_id", $productid, PDO::PARAM_INT);
		$sta->bindParam(":nsx_id", $producerid, PDO::PARAM_INT);
		$sta->bindParam(":name", $name, PDO::PARAM_STR);
		$sta->bindParam(":status", $status, PDO::PARAM_STR);
		$sta->bindParam(":km", $km, PDO::PARAM_STR);
		$sta->bindParam(":price", $price, PDO::PARAM_STR);
		
		$sta->execute();
	} 
	
	
    public function edit($id,$productid,$producerid,$name,$status,$km,$price)
	{
		$sql = "update san_pham set LSP_Ma = :lsp_id, NSX_Ma = :nsx_id, SP_Ten = :name, SP_TinhTrang = :status ,SP_KhuyenMai = :km ,SP_GiaNiemYet = :price where SP_Ma = :id ";
		$sta = $this->con->prepare($sql);
		
		$sta->bindParam(":lsp_id", $productid, PDO::PARAM_INT);
		$sta->bindParam(":nsx_id", $producerid, PDO::PARAM_INT);
		$sta->bindParam(":name", $name, PDO::PARAM_STR);
		$sta->bindParam(":status", $status, PDO::PARAM_STR);
		$sta->bindParam(":km", $km, PDO::PARAM_STR);
		$sta->bindParam(":price", $price, PDO::PARAM_INT);
		$sta->bindParam(":id", $id, PDO::PARAM_INT);
				
		
		$sta->execute();
	} 
 }


?>