-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2017 at 12:42 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shoplaptop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`AD_Ma` int(11) NOT NULL,
  `AD_HoTen` varchar(300) NOT NULL,
  `AD_TaiKhoan` varchar(300) NOT NULL,
  `AD_MatKhau` varchar(300) NOT NULL,
  `AD_GioiTinh` bit(1) NOT NULL,
  `AD_Email` varchar(300) NOT NULL,
  `AD_SDT` varchar(20) NOT NULL,
  `AD_DiaChi` varchar(300) NOT NULL,
  `PQ_Ma` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`AD_Ma`, `AD_HoTen`, `AD_TaiKhoan`, `AD_MatKhau`, `AD_GioiTinh`, `AD_Email`, `AD_SDT`, `AD_DiaChi`, `PQ_Ma`) VALUES
(1, 'Hoàng Văn Dương', 'hduong1998bn', '12071998', b'0', 'hduong1998bn@gmail.com', '1665749738', 'Bắc Ninh', 1),
(2, 'Nguyễn Văn Bách', 'Bach1998', '123', b'1', 'Bachc1998@gmail.com', '19008198', 'Hải Dương', 1),
(3, 'Nguyễn Minh Đức', 'Duc1998', '123', b'0', 'Duc1998@gmail.com', '1234567', 'Nam Định', 2),
(4, 'Nguyễn Thế Hùng', 'hung1998', '123', b'0', 'hung1998@gmail.com', '19008291', '', 2),
(5, 'Nguyễn Tuyết Như', 'tuyetnhu98888', '0108921105', b'1', 'tuyetnhu98@gmail.com', '016354963', 'Hà Nội', 2);

-- --------------------------------------------------------

--
-- Table structure for table `anh`
--

CREATE TABLE IF NOT EXISTS `anh` (
`Anh_Ma` int(11) NOT NULL,
  `Anh_Ten` text NOT NULL,
  `SP_Ma` int(11) NOT NULL,
  `LSP_Ma` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `anh`
--

INSERT INTO `anh` (`Anh_Ma`, `Anh_Ten`, `SP_Ma`, `LSP_Ma`) VALUES
(1, 'laptop/apple/apple1.jpg', 1, 1),
(2, 'laptop/apple/apple2.jpg', 2, 1),
(3, 'laptop/apple/apple3.jpg', 3, 1),
(4, 'laptop/apple/apple4.jpg', 4, 1),
(5, 'laptop/apple/apple5.jpg', 5, 1),
(6, 'laptop/apple/apple1.jpg', 6, 1),
(7, 'laptop/apple/apple2.jpg', 7, 1),
(8, 'laptop/apple/apple3.jpg', 8, 1),
(9, 'laptop/dell/dell1.png', 9, 1),
(10, 'laptop/dell/dell2.png', 10, 1),
(11, 'laptop/dell/dell3.jpg', 11, 1),
(12, 'laptop/dell/dell4.png', 12, 1),
(13, 'laptop/dell/dell5.png', 13, 1),
(14, 'laptop/dell/dell6.png', 14, 1),
(15, 'laptop/dell/dell7.jpg', 15, 1),
(16, 'laptop/dell/dell8.jpg', 16, 1),
(17, 'laptop/asus/asus1.jpg', 17, 1),
(18, 'laptop/asus/asus2.jpg', 18, 1),
(19, 'laptop/asus/asus3.png', 19, 1),
(20, 'laptop/asus/asus4.jpg', 20, 1),
(21, 'laptop/asus/asus5.jpg', 21, 1),
(22, 'laptop/asus/asus6.jpg', 22, 1),
(23, 'laptop/asus/asus7.jpg', 23, 1),
(24, 'laptop/asus/asus8.png', 24, 1),
(25, 'laptop/hp/hp1.png', 25, 1),
(26, 'laptop/hp/hp2.png', 26, 1),
(27, 'laptop/hp/hp3.png', 27, 1),
(28, 'laptop/hp/hp4.png', 28, 1),
(29, 'laptop/hp/hp5.jpg', 29, 1),
(30, 'laptop/hp/hp6.png', 30, 1),
(31, 'laptop/hp/hp7.jpg', 31, 1),
(32, 'laptop/hp/hp8.jpg', 32, 1),
(33, 'mouse/apple/apple1.jpg', 33, 2),
(34, 'mouse/apple/apple2.jpg', 34, 2),
(35, 'mouse/apple/apple3.jpg', 35, 2),
(36, 'mouse/apple/apple4.png', 36, 2),
(37, 'mouse/apple/apple5.jpg', 37, 2),
(38, 'mouse/apple/apple6.jpg', 38, 2),
(39, 'mouse/apple/apple7.jpg', 39, 2),
(40, 'mouse/apple/apple1.jpg', 40, 2),
(41, 'mouse/logitech/logitech1.png', 41, 2),
(42, 'mouse/logitech/logitech2.png', 42, 2),
(43, 'mouse/logitech/logitech3.png', 43, 2),
(44, 'mouse/logitech/logitech4.png', 44, 2),
(45, 'mouse/logitech/logitech5.png', 45, 2),
(46, 'mouse/logitech/logitech6.png', 46, 2),
(47, 'mouse/logitech/logitech7.png', 47, 2),
(48, 'mouse/logitech/logitech8.png', 48, 2),
(49, 'mouse/razer/razer_1.png', 49, 2),
(50, 'mouse/razer/razer_2.png', 50, 2),
(51, 'mouse/razer/razer_3.png', 51, 2),
(52, 'mouse/razer/razer_4.png', 52, 2),
(53, 'mouse/razer/razer_5.jpg', 53, 2),
(54, 'mouse/razer/razer_6.jpg', 54, 2),
(55, 'mouse/razer/razer_7.jpg', 55, 2),
(56, 'mouse/razer/razer_8.jpg', 56, 2),
(57, 'mouse/steelseris/steelseris_1.jpg', 57, 2),
(58, 'mouse/steelseris/steelseris_2.jpg', 58, 2),
(59, 'mouse/steelseris/steelseris_3.jpg', 59, 2),
(60, 'mouse/steelseris/steelseris_4.jpg', 60, 2),
(61, 'mouse/steelseris/steelseris_5.jpg', 61, 2),
(62, 'mouse/steelseris/steelseris_6.jpg', 62, 2),
(63, 'mouse/steelseris/steelseris_7.jpg', 63, 2),
(64, 'mouse/steelseris/steelseris_8.jpg', 64, 2);

-- --------------------------------------------------------

--
-- Table structure for table `bao_hanh`
--

CREATE TABLE IF NOT EXISTS `bao_hanh` (
`BH_Ma` int(11) NOT NULL,
  `DDH_Ma` int(11) NOT NULL,
  `BH_ThoiHanBH` varchar(20) NOT NULL,
  `BH_NgayBD` date NOT NULL,
  `BH_NgayHT` date NOT NULL,
  `BH_Phi` int(11) NOT NULL,
  `BH_HinhThuc` bit(1) NOT NULL,
  `BH_Mota` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bao_hanh`
--

INSERT INTO `bao_hanh` (`BH_Ma`, `DDH_Ma`, `BH_ThoiHanBH`, `BH_NgayBD`, `BH_NgayHT`, `BH_Phi`, `BH_HinhThuc`, `BH_Mota`) VALUES
(1, 1, '2 năm', '2017-10-03', '2017-10-06', 0, b'1', 'Bảo Hành ổ Đĩa Bị lỗi'),
(2, 2, '1 năm', '2017-10-19', '2017-10-13', 0, b'1', 'Lỗi màn hình không bật được'),
(3, 3, '12 tháng', '2017-10-05', '2017-10-08', 2000000, b'1', 'Sửa cổng kết nối USB');

-- --------------------------------------------------------

--
-- Table structure for table `ct_don_dat_hang`
--

CREATE TABLE IF NOT EXISTS `ct_don_dat_hang` (
`ID_CTDDH` int(11) NOT NULL,
  `DDH_Ma` int(11) DEFAULT NULL,
  `SP_Ma` int(11) DEFAULT NULL,
  `So_Luong` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ct_don_dat_hang`
--

INSERT INTO `ct_don_dat_hang` (`ID_CTDDH`, `DDH_Ma`, `SP_Ma`, `So_Luong`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 2),
(3, 1, 3, 2),
(4, 2, 4, 1),
(5, 2, 5, 3),
(6, 2, 6, 2),
(7, 3, 7, 1),
(8, 3, 8, 3),
(9, 3, 9, 2),
(10, 9, 26, 1),
(11, 9, 14, 2),
(12, 10, 61, 2);

-- --------------------------------------------------------

--
-- Table structure for table `don_dat_hang`
--

CREATE TABLE IF NOT EXISTS `don_dat_hang` (
`DDH_Ma` int(11) NOT NULL,
  `KH_Ma` int(11) NOT NULL,
  `DDH_NguoiNhan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `DDH_DiaChi` varchar(255) CHARACTER SET utf32 COLLATE utf32_unicode_ci DEFAULT NULL,
  `DDH_SDT` int(11) NOT NULL,
  `DDH_NgayDat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DDH_NgayNhan` date DEFAULT NULL,
  `DDH_TinhTrang` int(2) NOT NULL DEFAULT '1',
  `Luu_Y` text
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `don_dat_hang`
--

INSERT INTO `don_dat_hang` (`DDH_Ma`, `KH_Ma`, `DDH_NguoiNhan`, `DDH_DiaChi`, `DDH_SDT`, `DDH_NgayDat`, `DDH_NgayNhan`, `DDH_TinhTrang`, `Luu_Y`) VALUES
(1, 1, 'Nguyễn Minh Đức', 'KTX Mỹ Đình', 123456789, '2016-10-08 17:00:00', '2016-10-13', 2, ''),
(2, 2, 'Hoàng Văn Dương', 'KTX Mỹ Đình', 989721330, '2016-12-04 17:00:00', '2016-12-10', 2, 'Giao hàng vào buổi chiều'),
(3, 3, 'Nguyễn Văn Bách', 'KTX Mỹ Đình', 1669401136, '2016-11-18 17:00:00', '2016-11-23', 2, ''),
(6, 4, 'Bách', 'Hải Dương', 132, '2017-11-19 17:00:00', '2017-11-22', 1, NULL),
(7, 6, 'Park JiYeon', 'Xuân Lâm -Hà Nội', 363339996, '2017-11-04 17:00:00', '2017-11-08', 1, NULL),
(8, 3, 'Hoàng Kiều', 'Hà Nội', 1667849844, '2017-11-10 17:00:00', '2017-11-19', 1, NULL),
(9, 1, 'Nguyễn Văn Bách', 'Hải Dương', 654987321, '2017-11-21 12:53:48', NULL, 1, 'Không'),
(10, 1, 'Hoàng Văn Dương', 'Bắc Ninh', 654987321, '2017-11-21 09:43:06', NULL, 1, 'không');

-- --------------------------------------------------------

--
-- Table structure for table `hoi_dap`
--

CREATE TABLE IF NOT EXISTS `hoi_dap` (
`HD_Ma` int(11) NOT NULL,
  `HD_TieuDe` varchar(200) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `HD_NoiDung` text NOT NULL,
  `HD_CauTraLoi` text,
  `HD_NgayGui` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `HD_NgayPhanHoi` date DEFAULT NULL,
  `HD_TrangThai` bit(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `hoi_dap`
--

INSERT INTO `hoi_dap` (`HD_Ma`, `HD_TieuDe`, `Email`, `HD_NoiDung`, `HD_CauTraLoi`, `HD_NgayGui`, `HD_NgayPhanHoi`, `HD_TrangThai`) VALUES
(1, 'Lỗi Giao Hàng', 'hduong1998bn@gmail.com', 'Một vài lỗi giao hàng cần sửa Đơn Hàng 01 quá chậm so với thời hạn', 'Đã nhắc nhở và xem xét', '2017-09-30 17:00:00', '2017-10-03', b'1'),
(2, 'Asus VivoBook S15 ', 'bachbd@gmail.com', 'Asus VivoBook S15 có dùng thiết kế đồ họa mượt không', 'Với chip intel Core I5 & Card đồ họa VGA 2G ,máy có thể dùng thiết kế đồ họa mức nhẹ bạn nhé', '2017-10-03 17:00:00', '2017-10-10', b'1'),
(3, 'Khuyến mại Iphone 7s Plus', 'minhduc@gmail.com', 'khi nào có khuyến mại Iphone 7s Plus vậy shop?', 'Hiện tại chưa có thông tin về Khuyến mại Iphone 7s Plus bạn nhé!', '2017-09-30 17:00:00', '2017-10-06', b'1'),
(4, 'Iphone X và Samsung Galaxy S8 Plus', 'hduong1998bn@gmail.com', 'So Sánh Giữa Iphone X và Samsung Galaxy S8 Plus', '', '2017-10-06 17:00:00', '2017-10-11', b'0'),
(5, 'So Sánh Asus UM6 Và Panasonic S6 ', 'minhduc89@gmail.com', 'Giúp tôi So Sánh Asus UM6 Và Panasonic S6 để thấy sự khác biết', NULL, '2017-11-01 17:00:00', '2017-11-04', b'0'),
(6, 'Mua Laptop nào tốt tháng 7 năm 2017', 'bach1998hd@gmail.com', 'Giúp tôi chọn Mua laptop nào tốt tháng 7 năm 2017 ?', 'Bạn nên mua với tầm giá trung là Asus vivoBook S15 nhé', '2017-11-10 17:00:00', '2017-11-18', b'1'),
(7, 'Chọn Chip Laptop', 'quyet80@gmail.com', 'giúp tôi so sánh So Sánh Intel 6200DU và 6200U  để xem cái nào đáng mua hơn ', 'Intel 6200DU nhanh hơn bạn nhé', '2017-11-14 17:00:00', '2017-11-17', b'1'),
(8, 'Nên Dùng ổ SSD hay là HDD', 'minhcong@gmail.com', 'cho tôi hỏi là hiện nay Nên Dùng ổ cứng SSD hay là HDD thì tốt hơn', 'Ổ SSD luôn tốt hơn HDD nếu cùng dung lượng nhé bạn', '2017-11-03 17:00:00', '2017-11-11', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `khach_hang`
--

CREATE TABLE IF NOT EXISTS `khach_hang` (
`KH_Ma` int(11) NOT NULL,
  `KH_HoTen` varchar(150) NOT NULL,
  `KH_DiaChi` varchar(300) NOT NULL,
  `KH_SDT` varchar(20) DEFAULT NULL,
  `KH_Email` varchar(100) NOT NULL,
  `KH_TaiKhoan` varchar(100) NOT NULL,
  `KH_MatKhau` varchar(100) NOT NULL,
  `KH_TrangThaiTK` bit(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `khach_hang`
--

INSERT INTO `khach_hang` (`KH_Ma`, `KH_HoTen`, `KH_DiaChi`, `KH_SDT`, `KH_Email`, `KH_TaiKhoan`, `KH_MatKhau`, `KH_TrangThaiTK`) VALUES
(1, 'Nguyễn Văn Bách', 'Hải Dương', '01669401136', 'bach@gmail.com', 'bn1998', 'bach', b'0'),
(2, 'Nguyễn Quốc Cường', 'Thái Bình', '1293001', 'tbonline@gmail.com', 'tbonline1', '123', b'0'),
(3, 'Nguyễn Bá Kiến', 'Hưng Yên', '8131200', 'hyonlin1@gmail.com', 'hyonline1998', '12345', b'0'),
(4, 'Cao bình Lâm', 'Hà Nội', '32134', 'Tank1990@gmail.com', 'Tank1998VIP', '54321', b'0'),
(5, 'Tòng Thị Phóng', 'Côn Sơn', '1658498499', 'ttp@gmail.com', 'ttp2000', '12', b'0'),
(6, 'Nguyễn Thúy Nụ', 'Thái Bình', '124123', 'ntn1998@gmail.com', 'ntt1998', '1', b'0'),
(9, 'Thúy Bính', 'Thái Bình', '12412312', 'ntb1998@gmail.com', 'ntb1998', '123', b'0'),
(10, 'Lò Văn A', 'Uông Bí', '293341', 'LoVanA@gmail.com', 'LoVanA1998', '123', b'0'),
(11, 'Nguyễn Văn Bách', 'Hải Dương', '1669401136', 'test@gmail.com', 'bach1998', 'bach', b'0'),
(12, 'Hoàng Văn Dương', 'Bắc Ninh', '123456789', 'duong@gmail.com', 'duong98', 'duong', b'0'),
(13, 'Nguyễn Minh Đức', 'Nam Định', '123654987', 'duc@gmail.com', 'duc98', 'duc', b'0'),
(14, 'Nguyễn Thế Hùng', 'Quảng Ninh', '987654310', 'hung@gmail.com', 'hung98', 'hung', b'0'),
(15, 'Chu Thị Hoài', 'Việt Nam', '456789132', 'hoai@gmail.com', 'hoai98', 'hoai', b'0'),
(18, 'Nguyễn Hùng Cường', 'Hà Nội', '132465978', 'hcuong@gmail.com', 'hcuong', 'hcuong', b'0'),
(19, 'Nhận Email', 'Nhận Email', NULL, 'testnhanmail@gmail.com', 'Nhận Email', 'Nhận Email', b'0'),
(22, 'Nhận Email', 'Nhận Email', NULL, 'mail2@gmail.com', 'Nhận Email', 'Nhận Email', b'0'),
(23, 'Nhận Email', 'Nhận Email', NULL, 'test2mail@gmail.com', 'Nhận Email', 'Nhận Email', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `loai_san_pham`
--

CREATE TABLE IF NOT EXISTS `loai_san_pham` (
`LSP_Ma` int(11) NOT NULL,
  `LSP_Ten` varchar(50) NOT NULL,
  `LSP_ThongTinThem` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `loai_san_pham`
--

INSERT INTO `loai_san_pham` (`LSP_Ma`, `LSP_Ten`, `LSP_ThongTinThem`) VALUES
(1, 'Laptop', 'Không'),
(2, 'mouse', 'Không');

-- --------------------------------------------------------

--
-- Table structure for table `nha_san_xuat`
--

CREATE TABLE IF NOT EXISTS `nha_san_xuat` (
`NSX_Ma` int(11) NOT NULL,
  `NSX_Ten` varchar(300) NOT NULL,
  `NSX_DiaChi` varchar(300) NOT NULL,
  `NSX_SDT` varchar(20) NOT NULL,
  `NSX_Email` varchar(300) NOT NULL,
  `NSX_Website` varchar(300) NOT NULL,
  `NSX_ThongTin` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `nha_san_xuat`
--

INSERT INTO `nha_san_xuat` (`NSX_Ma`, `NSX_Ten`, `NSX_DiaChi`, `NSX_SDT`, `NSX_Email`, `NSX_Website`, `NSX_ThongTin`) VALUES
(1, 'APPLE', 'Mỹ', '2112112', 'APPLE@gmail.com', 'APPLE.vn', 'Apple Là một trong những thương hiệu giá trị nhất thế giới.Apple đang phát triển mạnh trong vài năm qua, đặc biệt là trong thế giới PCs. Số người tiêu dùng yêu thích hệ điều hành Mac OS X gia tăng đáng kể, đây là một hệ điều hành dễ sử dụng, được phát triển dựa trên hệ điều hành Linux.'),
(2, 'Dell', 'Mỹ', '3645645', 'dell@gmail.com', 'dell.com', 'Dell là một công ty đa quốc gia của Hoa Kỳ về phát triển và thương mại hóa công nghệ máy tính có trụ sở tại Round Rock, Texas, Hoa Kỳ. Dell được thành lập năm 1984 do chủ quản gia Michael Dell đồng sáng lập. Đây là công ty có thu nhập lớn thứ 28 tại Hoa Kỳ.'),
(3, 'ASUS', 'Đài Loan', '212321', 'asus@gmail.com', 'asus.com', 'Được thành lập năm 1989, ASUS là công ty điện tử tiêu dùng và phần cứng máy tính đa quốc gia có trụ sở tại Đài Loan. Luôn cam kết tạo nên những sản phẩm thông minh cho cuộc sống hiện tại và tương lai, ASUS là thương hiệu gaming và bo mạch chủ Số 1 thế giới, đồng thời thuộc top 3 nhà cung cấp máy tính xách tay tiêu dùng hàng đầu.'),
(4, 'HP', 'Mỹ', '1982346', 'hp@gmail.com', 'Hp.com', 'Hewlett-Packard (viết tắt HP) là tập đoàn công nghệ thông tin lớn nhất thế giới tính theo doanh thu. HP thành lập năm 1939 tại Palo Alto, California, Hoa Kỳ. HP hiện có trụ sở tại Cupertino, California, Hoa Kỳ. Năm 2006, tổng doanh thu của HP đạt 9.4 tỉ đô la, vượt đối thủ IBM với 9.1 tỉ, chính thức vươn lên vị trí số 1 trong các công ty công nghệ thông tin.'),
(5, 'Logitech', 'Thụy Sĩ', '3123123', 'Logitech@gmail.com', 'Logitech.com', 'Logitech International S.A. (thường được gọi là Logitech hay gọi cách điệu là Logi) là doanh nghiệp toàn cầu có trụ sở đặt tại Lausanne, Thụy Sĩ. Logitech chuyên sản xuất và cung ứng các thiết bị phụ kiện của máy tính cá nhân hay máy tính bảng như bàn phím, chuột máy tính, microphone, thiết bị chơi game, webcam. Ngoài ra, danh nghiệp này cũng sản xuất các thiết bị âm thanh dùng kèm máy tính như loa, tai nghe, thậm chí cả máy MP3 và điện thoại.'),
(6, 'Razer', 'Hoa Kỳ', '56468', 'Razer@gmail.com', 'Razer.vn', 'Razer Inc. (cách điệu là RΛZΞR) là một công ty của Mỹ được thành lập bởi Min-Liang Tan, và Robert Krakoff có trụ sở ở Irvine, California, chuyên kinh doanh các sản phẩm dành cho game thủ. Razer chuyên tâm vào tạo và phát triển các sản phẩm hướng tới game thủ PC như laptop chơi game, máy tính bảng chơi game, thiết bị ngoại vi máy tính chơi game khác nhau, thiết bị đeo và phụ kiện. Thương hiệu Razer hiện tại thuộc sở hữu của Razer USA Ltd.'),
(7, 'Steelseris', 'Mỹ', '546546', 'steelseris@gmail.com', 'steelseris.com', 'SteelSeries (được gọi là loạt thép) là nhà sản xuất thiết bị ngoại vi và phụ kiện chơi game của Đan Mạch, bao gồm tai nghe, bàn phím, chuột và bề mặt chơi game.');

-- --------------------------------------------------------

--
-- Table structure for table `phan_quyen`
--

CREATE TABLE IF NOT EXISTS `phan_quyen` (
`PQ_Ma` int(11) NOT NULL,
  `PQ_Ten` varchar(200) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `phan_quyen`
--

INSERT INTO `phan_quyen` (`PQ_Ma`, `PQ_Ten`) VALUES
(1, 'Admin'),
(2, 'Nhân Viên');

-- --------------------------------------------------------

--
-- Table structure for table `phu_kien`
--

CREATE TABLE IF NOT EXISTS `phu_kien` (
  `SP_Ma` int(11) NOT NULL,
  `PK_MauSac` varchar(30) NOT NULL,
  `PK_DPI` int(11) NOT NULL,
  `PK_DoBen` int(20) NOT NULL,
  `PK_TuongThich` varchar(150) NOT NULL,
  `PK_LoaiChuot` varchar(150) NOT NULL,
  `PK_MoTaChiTiet` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phu_kien`
--

INSERT INTO `phu_kien` (`SP_Ma`, `PK_MauSac`, `PK_DPI`, `PK_DoBen`, `PK_TuongThich`, `PK_LoaiChuot`, `PK_MoTaChiTiet`) VALUES
(33, 'Trắng', 1500, 100000000, 'Mac OS', 'Quang, không dây', 'Hôm nay, Apple đã làm mới loạt phụ kiện bàn phím, trackpad và chuột Magic Mouse của hãng.\n\n Trong đó, thay đổi đáng kể nhất chính là việc "Táo Khuyết" đã trang bị cổng sạc Lightning cho các sản phẩm này, thay vì sử dụng chuẩn pin AA truyền thống như trước đây.\n\n\n\n Được biết, khi sử dụng cổng sạc Lightning thì loạt phụ kiện mới của Apple có thể mang đến thời lượng pin kéo dài khoảng 1 tháng sau mỗi lần sạc đầy.\n\n Theo Apple, với chỉ hai phút kết nối với bộ sạc thì người dùng đã có thể sử dụng liên tục trong khoảng 9 giờ đồng hồ.\n\n Magic Mouse 2 vẫn giữ nguyên kiểu dáng như thế hệ trước.\n\n Thay vào đó, mặt sau chính là nơi được Apple thay đổi nhiều nhất khi hãng đã loại bỏ phần nắp đậy bằng nhôm để thay pin AA. '),
(34, 'Trắng', 1500, 100000000, 'Mac OS', 'Quang, không dây', 'Hôm nay, Apple đã làm mới loạt phụ kiện bàn phím, trackpad và chuột Magic Mouse của hãng.\n\n Trong đó, thay đổi đáng kể nhất chính là việc "Táo Khuyết" đã trang bị cổng sạc Lightning cho các sản phẩm này, thay vì sử dụng chuẩn pin AA truyền thống như trước đây.\n\n\n\n Được biết, khi sử dụng cổng sạc Lightning thì loạt phụ kiện mới của Apple có thể mang đến thời lượng pin kéo dài khoảng 1 tháng sau mỗi lần sạc đầy.\n\n Theo Apple, với chỉ hai phút kết nối với bộ sạc thì người dùng đã có thể sử dụng liên tục trong khoảng 9 giờ đồng hồ.\n\n Magic Mouse 2 vẫn giữ nguyên kiểu dáng như thế hệ trước.\n\n Thay vào đó, mặt sau chính là nơi được Apple thay đổi nhiều nhất khi hãng đã loại bỏ phần nắp đậy bằng nhôm để thay pin AA. '),
(35, 'Trắng', 1500, 100000000, 'Mac OS', 'Quang, không dây', 'Hôm nay, Apple đã làm mới loạt phụ kiện bàn phím, trackpad và chuột Magic Mouse của hãng.\n\n Trong đó, thay đổi đáng kể nhất chính là việc "Táo Khuyết" đã trang bị cổng sạc Lightning cho các sản phẩm này, thay vì sử dụng chuẩn pin AA truyền thống như trước đây.\n\n\n\n Được biết, khi sử dụng cổng sạc Lightning thì loạt phụ kiện mới của Apple có thể mang đến thời lượng pin kéo dài khoảng 1 tháng sau mỗi lần sạc đầy.\n\n Theo Apple, với chỉ hai phút kết nối với bộ sạc thì người dùng đã có thể sử dụng liên tục trong khoảng 9 giờ đồng hồ.\n\n Magic Mouse 2 vẫn giữ nguyên kiểu dáng như thế hệ trước.\n\n Thay vào đó, mặt sau chính là nơi được Apple thay đổi nhiều nhất khi hãng đã loại bỏ phần nắp đậy bằng nhôm để thay pin AA. '),
(36, 'Xám', 1500, 100000000, 'Mac OS', 'Quang, không dây', 'Hôm nay, Apple đã làm mới loạt phụ kiện bàn phím, trackpad và chuột Magic Mouse của hãng.\n\n Trong đó, thay đổi đáng kể nhất chính là việc "Táo Khuyết" đã trang bị cổng sạc Lightning cho các sản phẩm này, thay vì sử dụng chuẩn pin AA truyền thống như trước đây.\n\n\n\n Được biết, khi sử dụng cổng sạc Lightning thì loạt phụ kiện mới của Apple có thể mang đến thời lượng pin kéo dài khoảng 1 tháng sau mỗi lần sạc đầy.\n\n\n\n Theo Apple, với chỉ hai phút kết nối với bộ sạc thì người dùng đã có thể sử dụng liên tục trong khoảng 9 giờ đồng hồ.\n\n Magic Mouse 2 vẫn giữ nguyên kiểu dáng như thế hệ trước.\n\n Thay vào đó, mặt sau chính là nơi được Apple thay đổi nhiều nhất khi hãng đã loại bỏ phần nắp đậy bằng nhôm để thay pin AA. '),
(37, 'Trắng', 1500, 100000000, 'Mac OS', 'Quang, không dây', 'Hôm nay, Apple đã làm mới loạt phụ kiện bàn phím, trackpad và chuột Magic Mouse của hãng.\n\n Trong đó, thay đổi đáng kể nhất chính là việc "Táo Khuyết" đã trang bị cổng sạc Lightning cho các sản phẩm này, thay vì sử dụng chuẩn pin AA truyền thống như trước đây.\n\n\n\n Được biết, khi sử dụng cổng sạc Lightning thì loạt phụ kiện mới của Apple có thể mang đến thời lượng pin kéo dài khoảng 1 tháng sau mỗi lần sạc đầy.\n\n Theo Apple, với chỉ hai phút kết nối với bộ sạc thì người dùng đã có thể sử dụng liên tục trong khoảng 9 giờ đồng hồ.\n\n Magic Mouse 2 vẫn giữ nguyên kiểu dáng như thế hệ trước.\n\n Thay vào đó, mặt sau chính là nơi được Apple thay đổi nhiều nhất khi hãng đã loại bỏ phần nắp đậy bằng nhôm để thay pin AA. '),
(38, 'Trắng', 1500, 100000000, 'Mac OS', 'Quang, không dây', 'Hôm nay, Apple đã làm mới loạt phụ kiện bàn phím, trackpad và chuột Magic Mouse của hãng.\n\n Trong đó, thay đổi đáng kể nhất chính là việc "Táo Khuyết" đã trang bị cổng sạc Lightning cho các sản phẩm này, thay vì sử dụng chuẩn pin AA truyền thống như trước đây.\n\n\n\n Được biết, khi sử dụng cổng sạc Lightning thì loạt phụ kiện mới của Apple có thể mang đến thời lượng pin kéo dài khoảng 1 tháng sau mỗi lần sạc đầy.\n\n Theo Apple, với chỉ hai phút kết nối với bộ sạc thì người dùng đã có thể sử dụng liên tục trong khoảng 9 giờ đồng hồ.\n\n Magic Mouse 2 vẫn giữ nguyên kiểu dáng như thế hệ trước.\n\n Thay vào đó, mặt sau chính là nơi được Apple thay đổi nhiều nhất khi hãng đã loại bỏ phần nắp đậy bằng nhôm để thay pin AA. '),
(39, 'Trắng', 1500, 100000000, 'Mac OS', 'Quang, không dây', 'Hôm nay, Apple đã làm mới loạt phụ kiện bàn phím, trackpad và chuột Magic Mouse của hãng.\n\n Trong đó, thay đổi đáng kể nhất chính là việc "Táo Khuyết" đã trang bị cổng sạc Lightning cho các sản phẩm này, thay vì sử dụng chuẩn pin AA truyền thống như trước đây.\n\n\n\n Được biết, khi sử dụng cổng sạc Lightning thì loạt phụ kiện mới của Apple có thể mang đến thời lượng pin kéo dài khoảng 1 tháng sau mỗi lần sạc đầy.\n\n Theo Apple, với chỉ hai phút kết nối với bộ sạc thì người dùng đã có thể sử dụng liên tục trong khoảng 9 giờ đồng hồ.\n\n Magic Mouse 2 vẫn giữ nguyên kiểu dáng như thế hệ trước.\n\n Thay vào đó, mặt sau chính là nơi được Apple thay đổi nhiều nhất khi hãng đã loại bỏ phần nắp đậy bằng nhôm để thay pin AA. '),
(40, 'Trắng', 1500, 100000000, 'Mac OS', 'Quang, không dây', 'Hôm nay, Apple đã làm mới loạt phụ kiện bàn phím, trackpad và chuột Magic Mouse của hãng.\n\n Trong đó, thay đổi đáng kể nhất chính là việc "Táo Khuyết" đã trang bị cổng sạc Lightning cho các sản phẩm này, thay vì sử dụng chuẩn pin AA truyền thống như trước đây.\n\n\n\n Được biết, khi sử dụng cổng sạc Lightning thì loạt phụ kiện mới của Apple có thể mang đến thời lượng pin kéo dài khoảng 1 tháng sau mỗi lần sạc đầy.\n\n Theo Apple, với chỉ hai phút kết nối với bộ sạc thì người dùng đã có thể sử dụng liên tục trong khoảng 9 giờ đồng hồ.\n\n Magic Mouse 2 vẫn giữ nguyên kiểu dáng như thế hệ trước.\n\n Thay vào đó, mặt sau chính là nơi được Apple thay đổi nhiều nhất khi hãng đã loại bỏ phần nắp đậy bằng nhôm để thay pin AA. '),
(41, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Sản phẩm cho phép bạn hoàn thành nhiệm vụ một cách hiệu quả và không có tiếng ồn.\n\n Sử dụng Logitech FLOW, con chuột có khả năng kỳ diệu là điều hướng liền mạch trên hai máy tính và sao chép-dán từ máy này sang máy khác.\n\n  Đặc điểm giảm 90% tiếng nhấp chuột đảm bảo bạn sẽ không làm phiền những người xung quanh. '),
(42, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Sản phẩm cho phép bạn hoàn thành nhiệm vụ một cách hiệu quả và không có tiếng ồn.\n\n Sử dụng Logitech FLOW, con chuột có khả năng kỳ diệu là điều hướng liền mạch trên hai máy tính và sao chép-dán từ máy này sang máy khác.\n\n  Đặc điểm giảm 90% tiếng nhấp chuột đảm bảo bạn sẽ không làm phiền những người xung quanh.'),
(43, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Sản phẩm cho phép bạn hoàn thành nhiệm vụ một cách hiệu quả và không có tiếng ồn.\n\n Sử dụng Logitech FLOW, con chuột có khả năng kỳ diệu là điều hướng liền mạch trên hai máy tính và sao chép-dán từ máy này sang máy khác.\n\n  Đặc điểm giảm 90% tiếng nhấp chuột đảm bảo bạn sẽ không làm phiền những người xung quanh.'),
(44, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Sản phẩm cho phép bạn hoàn thành nhiệm vụ một cách hiệu quả và không có tiếng ồn.\n\n Sử dụng Logitech FLOW, con chuột có khả năng kỳ diệu là điều hướng liền mạch trên hai máy tính và sao chép-dán từ máy này sang máy khác.\n\n  Đặc điểm giảm 90% tiếng nhấp chuột đảm bảo bạn sẽ không làm phiền những người xung quanh.'),
(45, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Sản phẩm cho phép bạn hoàn thành nhiệm vụ một cách hiệu quả và không có tiếng ồn.\n\n Sử dụng Logitech FLOW, con chuột có khả năng kỳ diệu là điều hướng liền mạch trên hai máy tính và sao chép-dán từ máy này sang máy khác.\n\n  Đặc điểm giảm 90% tiếng nhấp chuột đảm bảo bạn sẽ không làm phiền những người xung quanh.'),
(46, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Sản phẩm cho phép bạn hoàn thành nhiệm vụ một cách hiệu quả và không có tiếng ồn.\n\n Sử dụng Logitech FLOW, con chuột có khả năng kỳ diệu là điều hướng liền mạch trên hai máy tính và sao chép-dán từ máy này sang máy khác.\n\n  Đặc điểm giảm 90% tiếng nhấp chuột đảm bảo bạn sẽ không làm phiền những người xung quanh.'),
(47, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Sản phẩm cho phép bạn hoàn thành nhiệm vụ một cách hiệu quả và không có tiếng ồn.\n\n Sử dụng Logitech FLOW, con chuột có khả năng kỳ diệu là điều hướng liền mạch trên hai máy tính và sao chép-dán từ máy này sang máy khác.\n\n  Đặc điểm giảm 90% tiếng nhấp chuột đảm bảo bạn sẽ không làm phiền những người xung quanh.'),
(48, 'Đen đỏ', 1500, 100000000, 'Window', 'Quang, có dây', 'Sản phẩm cho phép bạn hoàn thành nhiệm vụ một cách hiệu quả và không có tiếng ồn.\n\n Sử dụng Logitech FLOW, con chuột có khả năng kỳ diệu là điều hướng liền mạch trên hai máy tính và sao chép-dán từ máy này sang máy khác.\n\n  Đặc điểm giảm 90% tiếng nhấp chuột đảm bảo bạn sẽ không làm phiền những người xung quanh.'),
(49, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Được trang bị cảm biến quang học esports mới, có độ phân giải thực 16.000 điểm ảnh và tracking 450 Inches Per Second (IPS), Razer DeathAdder Elite mang lại cho bạn ưu thế tuyệt đối khi có cảm biến nhanh nhất trên thế giới.\n\n Được thiết kế để xác định lại các tiêu chuẩn về độ chính xác và tốc độ, bộ cảm biến chuột đáng kinh ngạc này sẽ phá vỡ sự cạnh tranh với độ phân giải chính xác là 99,4%, vì vậy bạn có thể hạ gục đối thủ với độ chính xác cao.'),
(50, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Được trang bị cảm biến quang học esports mới, có độ phân giải thực 16.000 điểm ảnh và tracking 450 Inches Per Second (IPS), Razer DeathAdder Elite mang lại cho bạn ưu thế tuyệt đối khi có cảm biến nhanh nhất trên thế giới.\n\n Được thiết kế để xác định lại các tiêu chuẩn về độ chính xác và tốc độ, bộ cảm biến chuột đáng kinh ngạc này sẽ phá vỡ sự cạnh tranh với độ phân giải chính xác là 99,4%, vì vậy bạn có thể hạ gục đối thủ với độ chính xác cao.'),
(51, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Được trang bị cảm biến quang học esports mới, có độ phân giải thực 16.000 điểm ảnh và tracking 450 Inches Per Second (IPS), Razer DeathAdder Elite mang lại cho bạn ưu thế tuyệt đối khi có cảm biến nhanh nhất trên thế giới.\n\n Được thiết kế để xác định lại các tiêu chuẩn về độ chính xác và tốc độ, bộ cảm biến chuột đáng kinh ngạc này sẽ phá vỡ sự cạnh tranh với độ phân giải chính xác là 99,4%, vì vậy bạn có thể hạ gục đối thủ với độ chính xác cao.'),
(52, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Được trang bị cảm biến quang học esports mới, có độ phân giải thực 16.000 điểm ảnh và tracking 450 Inches Per Second (IPS), Razer DeathAdder Elite mang lại cho bạn ưu thế tuyệt đối khi có cảm biến nhanh nhất trên thế giới.\n\n Được thiết kế để xác định lại các tiêu chuẩn về độ chính xác và tốc độ, bộ cảm biến chuột đáng kinh ngạc này sẽ phá vỡ sự cạnh tranh với độ phân giải chính xác là 99,4%, vì vậy bạn có thể hạ gục đối thủ với độ chính xác cao.'),
(53, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Được trang bị cảm biến quang học esports mới, có độ phân giải thực 16.000 điểm ảnh và tracking 450 Inches Per Second (IPS), Razer DeathAdder Elite mang lại cho bạn ưu thế tuyệt đối khi có cảm biến nhanh nhất trên thế giới.\n\n Được thiết kế để xác định lại các tiêu chuẩn về độ chính xác và tốc độ, bộ cảm biến chuột đáng kinh ngạc này sẽ phá vỡ sự cạnh tranh với độ phân giải chính xác là 99,4%, vì vậy bạn có thể hạ gục đối thủ với độ chính xác cao.'),
(54, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Được trang bị cảm biến quang học esports mới, có độ phân giải thực 16.000 điểm ảnh và tracking 450 Inches Per Second (IPS), Razer DeathAdder Elite mang lại cho bạn ưu thế tuyệt đối khi có cảm biến nhanh nhất trên thế giới.\n\n Được thiết kế để xác định lại các tiêu chuẩn về độ chính xác và tốc độ, bộ cảm biến chuột đáng kinh ngạc này sẽ phá vỡ sự cạnh tranh với độ phân giải chính xác là 99,4%, vì vậy bạn có thể hạ gục đối thủ với độ chính xác cao.'),
(55, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Được trang bị cảm biến quang học esports mới, có độ phân giải thực 16.000 điểm ảnh và tracking 450 Inches Per Second (IPS), Razer DeathAdder Elite mang lại cho bạn ưu thế tuyệt đối khi có cảm biến nhanh nhất trên thế giới.\n\n Được thiết kế để xác định lại các tiêu chuẩn về độ chính xác và tốc độ, bộ cảm biến chuột đáng kinh ngạc này sẽ phá vỡ sự cạnh tranh với độ phân giải chính xác là 99,4%, vì vậy bạn có thể hạ gục đối thủ với độ chính xác cao.'),
(56, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'Được trang bị cảm biến quang học esports mới, có độ phân giải thực 16.000 điểm ảnh và tracking 450 Inches Per Second (IPS), Razer DeathAdder Elite mang lại cho bạn ưu thế tuyệt đối khi có cảm biến nhanh nhất trên thế giới.\n\n Được thiết kế để xác định lại các tiêu chuẩn về độ chính xác và tốc độ, bộ cảm biến chuột đáng kinh ngạc này sẽ phá vỡ sự cạnh tranh với độ phân giải chính xác là 99,4%, vì vậy bạn có thể hạ gục đối thủ với độ chính xác cao.'),
(57, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'SteelSeries chú chuột đến từ nhà sản xuất Đan Mạch, là một chú chuột với giá cả phải chăng, nhưng lại được SteelSeries trang bị cho một vài tính năng cũng rất ấn tượng: chuột được thiết kế dành cho cả 2 loại người dùng, thuận tay trái hoặc tay phải, trang bị đèn RGB, nút thay đổi DPI, bề mặt nhám… \n\nNếu bạn đang tìm kiếm 1 chú chuột để dùng cho hoạt động chơi game/làm việc hàng ngày với một chi phí phù hợp thì sản phẩm này có thể đáp ứng tốt điều kiện, nhu cầu của bạn.\n\nSteelSeries sử dụng “thiết kế thông dụng” cho chú chuột của mình, với những đường nét cân đối phù hợp với người sử dụng tay trái hoặc tay phải.\n\n Tuy nhiên chú chuột này vẫn được SteelSeries ưu ái thiết kế cho người dùng thuận tay phải hơn khi thiết kế 2 nút chức năng bên hông trái chuột.'),
(58, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'SteelSeries chú chuột đến từ nhà sản xuất Đan Mạch, là một chú chuột với giá cả phải chăng, nhưng lại được SteelSeries trang bị cho một vài tính năng cũng rất ấn tượng: chuột được thiết kế dành cho cả 2 loại người dùng, thuận tay trái hoặc tay phải, trang bị đèn RGB, nút thay đổi DPI, bề mặt nhám… \n\nNếu bạn đang tìm kiếm 1 chú chuột để dùng cho hoạt động chơi game/làm việc hàng ngày với một chi phí phù hợp thì sản phẩm này có thể đáp ứng tốt điều kiện, nhu cầu của bạn.\n\nSteelSeries sử dụng “thiết kế thông dụng” cho chú chuột của mình, với những đường nét cân đối phù hợp với người sử dụng tay trái hoặc tay phải.\n\n Tuy nhiên chú chuột này vẫn được SteelSeries ưu ái thiết kế cho người dùng thuận tay phải hơn khi thiết kế 2 nút chức năng bên hông trái chuột.'),
(59, 'Đen Trắng', 1500, 100000000, 'Window', 'Quang, có dây', 'SteelSeries chú chuột đến từ nhà sản xuất Đan Mạch, là một chú chuột với giá cả phải chăng, nhưng lại được SteelSeries trang bị cho một vài tính năng cũng rất ấn tượng: chuột được thiết kế dành cho cả 2 loại người dùng, thuận tay trái hoặc tay phải, trang bị đèn RGB, nút thay đổi DPI, bề mặt nhám… \n\nNếu bạn đang tìm kiếm 1 chú chuột để dùng cho hoạt động chơi game/làm việc hàng ngày với một chi phí phù hợp thì sản phẩm này có thể đáp ứng tốt điều kiện, nhu cầu của bạn.\n\nSteelSeries sử dụng “thiết kế thông dụng” cho chú chuột của mình, với những đường nét cân đối phù hợp với người sử dụng tay trái hoặc tay phải.\n\n Tuy nhiên chú chuột này vẫn được SteelSeries ưu ái thiết kế cho người dùng thuận tay phải hơn khi thiết kế 2 nút chức năng bên hông trái chuột.'),
(60, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'SteelSeries chú chuột đến từ nhà sản xuất Đan Mạch, là một chú chuột với giá cả phải chăng, nhưng lại được SteelSeries trang bị cho một vài tính năng cũng rất ấn tượng: chuột được thiết kế dành cho cả 2 loại người dùng, thuận tay trái hoặc tay phải, trang bị đèn RGB, nút thay đổi DPI, bề mặt nhám… \n\nNếu bạn đang tìm kiếm 1 chú chuột để dùng cho hoạt động chơi game/làm việc hàng ngày với một chi phí phù hợp thì sản phẩm này có thể đáp ứng tốt điều kiện, nhu cầu của bạn.\n\nSteelSeries sử dụng “thiết kế thông dụng” cho chú chuột của mình, với những đường nét cân đối phù hợp với người sử dụng tay trái hoặc tay phải.\n\n Tuy nhiên chú chuột này vẫn được SteelSeries ưu ái thiết kế cho người dùng thuận tay phải hơn khi thiết kế 2 nút chức năng bên hông trái chuột.'),
(61, 'Đen Cam', 1500, 100000000, 'Window', 'Quang, có dây', 'SteelSeries chú chuột đến từ nhà sản xuất Đan Mạch, là một chú chuột với giá cả phải chăng, nhưng lại được SteelSeries trang bị cho một vài tính năng cũng rất ấn tượng: chuột được thiết kế dành cho cả 2 loại người dùng, thuận tay trái hoặc tay phải, trang bị đèn RGB, nút thay đổi DPI, bề mặt nhám… \n\nNếu bạn đang tìm kiếm 1 chú chuột để dùng cho hoạt động chơi game/làm việc hàng ngày với một chi phí phù hợp thì sản phẩm này có thể đáp ứng tốt điều kiện, nhu cầu của bạn.\n\nSteelSeries sử dụng “thiết kế thông dụng” cho chú chuột của mình, với những đường nét cân đối phù hợp với người sử dụng tay trái hoặc tay phải.\n\n Tuy nhiên chú chuột này vẫn được SteelSeries ưu ái thiết kế cho người dùng thuận tay phải hơn khi thiết kế 2 nút chức năng bên hông trái chuột.'),
(62, 'Đen', 1500, 100000000, 'Window', 'Quang, có dây', 'SteelSeries chú chuột đến từ nhà sản xuất Đan Mạch, là một chú chuột với giá cả phải chăng, nhưng lại được SteelSeries trang bị cho một vài tính năng cũng rất ấn tượng: chuột được thiết kế dành cho cả 2 loại người dùng, thuận tay trái hoặc tay phải, trang bị đèn RGB, nút thay đổi DPI, bề mặt nhám… \n\nNếu bạn đang tìm kiếm 1 chú chuột để dùng cho hoạt động chơi game/làm việc hàng ngày với một chi phí phù hợp thì sản phẩm này có thể đáp ứng tốt điều kiện, nhu cầu của bạn.\n\nSteelSeries sử dụng “thiết kế thông dụng” cho chú chuột của mình, với những đường nét cân đối phù hợp với người sử dụng tay trái hoặc tay phải.\n\n Tuy nhiên chú chuột này vẫn được SteelSeries ưu ái thiết kế cho người dùng thuận tay phải hơn khi thiết kế 2 nút chức năng bên hông trái chuột.'),
(63, 'Đen Trắng', 1500, 100000000, 'Window', 'Quang, có dây', 'SteelSeries chú chuột đến từ nhà sản xuất Đan Mạch, là một chú chuột với giá cả phải chăng, nhưng lại được SteelSeries trang bị cho một vài tính năng cũng rất ấn tượng: chuột được thiết kế dành cho cả 2 loại người dùng, thuận tay trái hoặc tay phải, trang bị đèn RGB, nút thay đổi DPI, bề mặt nhám… \n\nNếu bạn đang tìm kiếm 1 chú chuột để dùng cho hoạt động chơi game/làm việc hàng ngày với một chi phí phù hợp thì sản phẩm này có thể đáp ứng tốt điều kiện, nhu cầu của bạn.\n\nSteelSeries sử dụng “thiết kế thông dụng” cho chú chuột của mình, với những đường nét cân đối phù hợp với người sử dụng tay trái hoặc tay phải.\n\n Tuy nhiên chú chuột này vẫn được SteelSeries ưu ái thiết kế cho người dùng thuận tay phải hơn khi thiết kế 2 nút chức năng bên hông trái chuột.'),
(64, 'Trắng', 1500, 100000000, 'Window', 'Quang, có dây', 'SteelSeries chú chuột đến từ nhà sản xuất Đan Mạch, là một chú chuột với giá cả phải chăng, nhưng lại được SteelSeries trang bị cho một vài tính năng cũng rất ấn tượng: chuột được thiết kế dành cho cả 2 loại người dùng, thuận tay trái hoặc tay phải, trang bị đèn RGB, nút thay đổi DPI, bề mặt nhám… \n\nNếu bạn đang tìm kiếm 1 chú chuột để dùng cho hoạt động chơi game/làm việc hàng ngày với một chi phí phù hợp thì sản phẩm này có thể đáp ứng tốt điều kiện, nhu cầu của bạn.\n\nSteelSeries sử dụng “thiết kế thông dụng” cho chú chuột của mình, với những đường nét cân đối phù hợp với người sử dụng tay trái hoặc tay phải.\n\n Tuy nhiên chú chuột này vẫn được SteelSeries ưu ái thiết kế cho người dùng thuận tay phải hơn khi thiết kế 2 nút chức năng bên hông trái chuột.');

-- --------------------------------------------------------

--
-- Table structure for table `quang_cao`
--

CREATE TABLE IF NOT EXISTS `quang_cao` (
`QC_Ma` int(11) NOT NULL,
  `QC_TieuDe` varchar(200) NOT NULL,
  `QC_NgayBatDau` date NOT NULL,
  `QC_NgayKetThuc` date NOT NULL,
  `QC_NoiDung` varchar(200) NOT NULL,
  `QC_HinhAnh` varchar(200) NOT NULL,
  `QC_Loai` int(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `quang_cao`
--

INSERT INTO `quang_cao` (`QC_Ma`, `QC_TieuDe`, `QC_NgayBatDau`, `QC_NgayKetThuc`, `QC_NoiDung`, `QC_HinhAnh`, `QC_Loai`) VALUES
(1, 'Giảm giá', '2017-10-03', '2017-10-05', 'Mua tại cửa hàng -5%', 'Carousel/slide-1a.jpg', 1),
(2, 'Xả hàng', '2017-10-04', '2017-10-07', 'Giảm tới 30%', 'Carousel/slide-2a.jpg', 1),
(3, 'ASUS', '2017-10-12', '2017-10-20', 'TRANFORMER PRO', 'Carousel/slide-3a.jpg', 1),
(4, 'Laptop', '2017-10-05', '2017-10-05', 'Gaming', 'Carousel/slide-6a.jpg', 1),
(5, 'Asus ban quyen', '2017-11-19', '2018-11-19', 'mua asus ban quyen nhan khuyen mai', 'Carousel/QC2.jpg', 2),
(6, 'Asus ban quyen', '2017-11-19', '2018-11-19', 'mua asus ban quyen nhan khuyen mai', 'Carousel/QC3.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `san_pham`
--

CREATE TABLE IF NOT EXISTS `san_pham` (
`SP_Ma` int(11) NOT NULL,
  `LSP_Ma` int(11) DEFAULT NULL,
  `NSX_Ma` int(11) DEFAULT NULL,
  `SP_Ten` varchar(150) NOT NULL,
  `SP_TinhTrang` int(1) NOT NULL,
  `SP_KhuyenMai` int(3) DEFAULT NULL,
  `SP_GiaNiemYet` int(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `san_pham`
--

INSERT INTO `san_pham` (`SP_Ma`, `LSP_Ma`, `NSX_Ma`, `SP_Ten`, `SP_TinhTrang`, `SP_KhuyenMai`, `SP_GiaNiemYet`) VALUES
(1, 1, 1, 'Apple Macbook Air MQD32SA/A', 1, 0, 32460000),
(2, 1, 1, 'Apple Macbook Air MMGF2ZP/A', 1, 0, 29750000),
(3, 1, 1, 'Apple Macbook Air MQD42SA/A', 1, 8, 29950000),
(4, 1, 1, 'Apple Macbook Air MQD32SA/A', 1, 5, 28750000),
(5, 1, 1, 'Apple Macbook Air MQD42SA/A', 1, 0, 32500000),
(6, 1, 1, 'Apple Macbook Air MQD32SA/A', 1, 0, 35690000),
(7, 1, 1, 'Apple Macbook Air MMGF2ZP/A', 1, 3, 27988000),
(8, 1, 1, 'Apple Macbook Air MQD42SA/A', 1, 0, 30490000),
(9, 1, 2, 'Dell Inspiron N3467', 1, 0, 22500000),
(10, 1, 2, 'Dell Inspiron N3467C', 1, 7, 29750000),
(11, 1, 2, 'Dell Inspiron N3464N', 1, 8, 16400000),
(12, 1, 2, 'Dell Inspiron N3465A', 1, 0, 12500000),
(13, 1, 2, 'Dell Inspiron N3466E', 1, 0, 19720000),
(14, 1, 2, 'Dell Inspiron N3461Z', 1, 0, 12500000),
(15, 1, 2, 'Dell Inspiron N3469K', 1, 3, 29750000),
(16, 1, 2, 'Dell Inspiron N346B', 1, 0, 22500000),
(17, 1, 3, 'Asus E200HA-FD0123TS', 1, 0, 19570000),
(18, 1, 3, 'Asus E200HA-FD0242TS', 1, 7, 29750000),
(19, 1, 3, 'Asus E200HA-FD0346TS', 1, 0, 20500000),
(20, 1, 3, 'Asus E200HA-FD0463TS', 1, 5, 16900000),
(21, 1, 3, 'Asus E200HA-FD0547TS', 1, 0, 22500000),
(22, 1, 3, 'Asus E200HA-FD0641TS', 1, 0, 30900000),
(23, 1, 3, 'Asus E200HA-FD0723TS', 1, 3, 29750000),
(24, 1, 3, 'Asus E200HA-FD0843TS', 1, 0, 15900000),
(25, 1, 4, 'HP 15-ks753TU', 1, 0, 11800000),
(26, 1, 4, 'HP 16-bs587TU', 1, 5, 12450000),
(27, 1, 4, 'HP 14-bk557TU', 1, 0, 13900000),
(28, 1, 4, 'HP 12-bs593TU', 1, 5, 16420000),
(29, 1, 4, 'HP 18-bk573TU', 1, 0, 10200000),
(30, 1, 4, 'HP 19-bs553TU', 1, 0, 11200000),
(31, 1, 4, 'HP 17-ks453TU', 1, 3, 18050000),
(32, 1, 4, 'HP 14-bs577TU', 1, 0, 20400000),
(33, 2, 1, 'Apple Macgic Mouse Gen 2', 1, 0, 1299000),
(34, 2, 1, 'Apple Macgic Mouse 1', 1, 7, 1130000),
(35, 2, 1, 'Apple Macgic Mouse 2', 1, 0, 1590000),
(36, 2, 1, 'Apple Macgic Mouse Gen 1', 1, 5, 989000),
(37, 2, 1, 'Apple Macgic Mouse 2', 1, 0, 1459000),
(38, 2, 1, 'Apple Macgic Mouse Gen 2', 1, 0, 1269000),
(39, 2, 1, 'Apple Macgic Mouse 2', 1, 3, 1659000),
(40, 2, 1, 'Apple Macgic Mouse Gen 1', 1, 0, 1899000),
(41, 2, 5, 'Logitech G132 Prodigy RGB LED', 1, 0, 1590000),
(42, 2, 5, 'Logitech G503 Prodigy RGB LED', 1, 0, 1130000),
(43, 2, 5, 'Logitech G921 Prodigy RGB LED', 1, 8, 1050000),
(44, 2, 5, 'Logitech G332 Prodigy RGB LED', 1, 5, 989000),
(45, 2, 5, 'Logitech G671 Prodigy RGB LED', 1, 10, 1459000),
(46, 2, 5, 'Logitech G997 Prodigy RGB LED', 1, 0, 1260000),
(47, 2, 5, 'Logitech G645 Prodigy RGB LED', 1, 3, 900000),
(48, 2, 5, 'Logitech G563 Prodigy RGB LED', 1, 0, 792000),
(49, 2, 6, 'Razer Lancehead Wireless', 1, 0, 1669000),
(50, 2, 6, 'Razer Lancehead Tournament Edition 1', 1, 7, 2430000),
(51, 2, 6, 'Razer Lancehead Wireless 2', 1, 0, 1290000),
(52, 2, 6, 'Razer Lancehead Tournament Edition', 1, 5, 950000),
(53, 2, 6, 'Razer Lancehead Wireless 3', 1, 0, 1459000),
(54, 2, 6, 'Razer Lancehead Tournament Edition 2', 1, 0, 1269000),
(55, 2, 6, 'Razer Lancehead Wireless 4', 1, 3, 1690000),
(56, 2, 6, 'Razer Lancehead Tournament Edition 3', 1, 0, 1899000),
(57, 2, 7, 'steelseries Rival 100', 1, 0, 1709000),
(58, 2, 7, 'steelseries Rival 570', 1, 7, 1130000),
(59, 2, 7, 'steelseries Rival 460', 1, 8, 1250000),
(60, 2, 7, 'steelseries Rival 320', 1, 5, 989000),
(61, 2, 7, 'steelseries Rival 504', 1, 0, 1550000),
(62, 2, 7, 'steelseries Rival 420', 1, 0, 1269000),
(63, 2, 7, 'steelseries Rival 302', 1, 3, 1000000),
(64, 2, 7, 'steelseries Rival 207', 1, 0, 1779000),
(65, 2, 1, 'Pro Mousse', 1, 5, 360000);

-- --------------------------------------------------------

--
-- Table structure for table `sp_laptop`
--

CREATE TABLE IF NOT EXISTS `sp_laptop` (
  `SP_Ma` int(11) NOT NULL,
  `Lap_CPU` varchar(70) CHARACTER SET utf8 NOT NULL,
  `Lap_RAM` varchar(70) CHARACTER SET utf8 NOT NULL,
  `Lap_ManHinh` varchar(70) CHARACTER SET utf8 NOT NULL,
  `Lap_CardMH` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Lap_HDH` varchar(70) CHARACTER SET utf8 NOT NULL,
  `Lap_BoNhoDem` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Lap_OCung` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Lap_ODia` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Lap_CongKetNoi` varchar(200) CHARACTER SET utf8 NOT NULL,
  `Lap_AmThanh` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Lap_Pin` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Lap_ChucNangKhac` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `Lap_MoTa` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `sp_laptop`
--

INSERT INTO `sp_laptop` (`SP_Ma`, `Lap_CPU`, `Lap_RAM`, `Lap_ManHinh`, `Lap_CardMH`, `Lap_HDH`, `Lap_BoNhoDem`, `Lap_OCung`, `Lap_ODia`, `Lap_CongKetNoi`, `Lap_AmThanh`, `Lap_Pin`, `Lap_ChucNangKhac`, `Lap_MoTa`) VALUES
(1, 'CPU Intel Core i5-5257U 2.9GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Mac OS', '  3 MB (L3 Cache)', 'HDD 500GB', 'Ổ Đĩa Quang', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', 'Multi Touchpad', 'Apple Macbook Air MQD32SA/A với thiết kế không thay đổi, vỏ nhôm sang trọng, siêu mỏng và siêu nhẹ, hiệu năng được nâng cấp, thời lượng pin cực lâu, phù hợp cho nhu cầu làm việc văn phòng nhẹ nhàng, không cần quá chú trọng vào hiển thị của màn hình.'),
(2, 'CPU Intel Core i3-5257U 3.1GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Mac OS', '  3 MB (L3 Cache)', '  SSD 500GB', 'Ổ Đĩa Quang', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', 'Không', 'Apple Macbook Air MMGF2ZP/A với thiết kế không thay đổi, vỏ nhôm sang trọng, siêu mỏng và siêu nhẹ, hiệu năng được nâng cấp, thời lượng pin cực lâu, phù hợp cho nhu cầu làm việc văn phòng nhẹ nhàng, không cần quá chú trọng vào hiển thị của màn hình.'),
(3, 'CPU Intel Core i7-5257U 3.0GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Mac OS', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '4 Cell', 'Multi Touchpad', 'Apple Macbook Air MQD32SA/A với thiết kế không thay đổi, vỏ nhôm sang trọng, siêu mỏng và siêu nhẹ, hiệu năng được nâng cấp, thời lượng pin cực lâu, phù hợp cho nhu cầu làm việc văn phòng nhẹ nhàng, không cần quá chú trọng vào hiển thị của màn hình.'),
(4, 'CPU Intel Core i3-5257U 2.8GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Mac OS', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', 'Multi Touchpad', 'Apple Macbook Air MMGF2ZP/A với thiết kế không thay đổi, vỏ nhôm sang trọng, siêu mỏng và siêu nhẹ, hiệu năng được nâng cấp, thời lượng pin cực lâu, phù hợp cho nhu cầu làm việc văn phòng nhẹ nhàng, không cần quá chú trọng vào hiển thị của màn hình.'),
(5, 'CPU Intel Core i3-5257U 3.2GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Mac OS', '  3 MB (L3 Cache)', '  SSD 128GB', 'Không ', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', 'Multi Touchpad,Có đèn bàn phím', 'Apple Macbook Air MQD32SA/A với thiết kế không thay đổi, vỏ nhôm sang trọng, siêu mỏng và siêu nhẹ, hiệu năng được nâng cấp, thời lượng pin cực lâu, phù hợp cho nhu cầu làm việc văn phòng nhẹ nhàng, không cần quá chú trọng vào hiển thị của màn hình.'),
(6, 'CPU Intel Core i7-5257U 2.9GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Mac OS', '  3 MB (L3 Cache)', '  SSD 128GB', 'Không', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '4 Cell', 'Multi Touchpad,Có đèn bàn phím', 'Apple Macbook Air MMGF2ZP/A với thiết kế không thay đổi, vỏ nhôm sang trọng, siêu mỏng và siêu nhẹ, hiệu năng được nâng cấp, thời lượng pin cực lâu, phù hợp cho nhu cầu làm việc văn phòng nhẹ nhàng, không cần quá chú trọng vào hiển thị của màn hình.'),
(7, 'CPU Intel Core i7-5257U 2.9GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Mac OS', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', 'Không', 'Apple Macbook Air MQD32SA/A với thiết kế không thay đổi, vỏ nhôm sang trọng, siêu mỏng và siêu nhẹ, hiệu năng được nâng cấp, thời lượng pin cực lâu, phù hợp cho nhu cầu làm việc văn phòng nhẹ nhàng, không cần quá chú trọng vào hiển thị của màn hình.'),
(8, 'CPU Intel Core i5-5257U 2.8GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Mac OS', '  3 MB (L3 Cache)', '  SSD 128GB', 'Không', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', 'Multi Touchpad,Có đèn bàn phím', 'Apple Macbook Air MMGF2ZP/A với thiết kế không thay đổi, vỏ nhôm sang trọng, siêu mỏng và siêu nhẹ, hiệu năng được nâng cấp, thời lượng pin cực lâu, phù hợp cho nhu cầu làm việc văn phòng nhẹ nhàng, không cần quá chú trọng vào hiển thị của màn hình.'),
(9, 'CPU Intel Core i5-5257U 2.9GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', 'Multi Touchpad,Có đèn bàn phím', 'Laptop Dell Inspiron N3467 vẫn thừa hưởng những đường nét mạnh mẽ và chắc chắn mang dấu ấn thương hiệu Dell tiền nhiệm. Hơn nữa, dòng sản phẩm này còn được nhà sản xuất trang bị bộ vi xử lý mạnh mẽ, mang đến hiệu năng vượt trội, đáp ứng nhu cầu học tập và làm việc của người dùng, đi kèm với giá thành hợp lý phù hợp với túi tiền của các bạn học sinh, sinh viên.'),
(10, 'CPU Intel Core i5-5257U 3.0GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 500GB', 'Ổ Đĩa Quang', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', 'Multi Touchpad,Có đèn bàn phím', 'Laptop Dell Inspiron N3467 vẫn thừa hưởng những đường nét mạnh mẽ và chắc chắn mang dấu ấn thương hiệu Dell tiền nhiệm. Hơn nữa, dòng sản phẩm này còn được nhà sản xuất trang bị bộ vi xử lý mạnh mẽ, mang đến hiệu năng vượt trội, đáp ứng nhu cầu học tập và làm việc của người dùng, đi kèm với giá thành hợp lý phù hợp với túi tiền của các bạn học sinh, sinh viên.'),
(11, 'CPU Intel Core i5-5257U 2.7GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '4 Cell', 'Multi Touchpad,Có đèn bàn phím', 'Laptop Dell Inspiron N3467 vẫn thừa hưởng những đường nét mạnh mẽ và chắc chắn mang dấu ấn thương hiệu Dell tiền nhiệm. Hơn nữa, dòng sản phẩm này còn được nhà sản xuất trang bị bộ vi xử lý mạnh mẽ, mang đến hiệu năng vượt trội, đáp ứng nhu cầu học tập và làm việc của người dùng, đi kèm với giá thành hợp lý phù hợp với túi tiền của các bạn học sinh, sinh viên.'),
(12, 'CPU Intel Core i5-5257U 2.9GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', 'Multi Touchpad,Có đèn bàn phím', 'Laptop Dell Inspiron N3467 vẫn thừa hưởng những đường nét mạnh mẽ và chắc chắn mang dấu ấn thương hiệu Dell tiền nhiệm. Hơn nữa, dòng sản phẩm này còn được nhà sản xuất trang bị bộ vi xử lý mạnh mẽ, mang đến hiệu năng vượt trội, đáp ứng nhu cầu học tập và làm việc của người dùng, đi kèm với giá thành hợp lý phù hợp với túi tiền của các bạn học sinh, sinh viên.'),
(13, 'CPU Intel Core i5-5257U 3.0GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Không ', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', '', 'Laptop Dell Inspiron N3467 vẫn thừa hưởng những đường nét mạnh mẽ và chắc chắn mang dấu ấn thương hiệu Dell tiền nhiệm. Hơn nữa, dòng sản phẩm này còn được nhà sản xuất trang bị bộ vi xử lý mạnh mẽ, mang đến hiệu năng vượt trội, đáp ứng nhu cầu học tập và làm việc của người dùng, đi kèm với giá thành hợp lý phù hợp với túi tiền của các bạn học sinh, sinh viên.'),
(14, 'CPU Intel Core i5-5257U 3.0GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Không', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '4 Cell', 'Multi Touchpad,Có đèn bàn phím', 'Laptop Dell Inspiron N3467 vẫn thừa hưởng những đường nét mạnh mẽ và chắc chắn mang dấu ấn thương hiệu Dell tiền nhiệm. Hơn nữa, dòng sản phẩm này còn được nhà sản xuất trang bị bộ vi xử lý mạnh mẽ, mang đến hiệu năng vượt trội, đáp ứng nhu cầu học tập và làm việc của người dùng, đi kèm với giá thành hợp lý phù hợp với túi tiền của các bạn học sinh, sinh viên.'),
(15, 'CPU Intel Core i5-5257U 2.7GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '3 x USB 3.0 ; 1 x HDMI ; 1 x RJ45 ; 1 x Microphone-in/Headphone-out Combo', '  Stereo speakers', '3 Cell', '', 'Laptop Dell Inspiron N3467 vẫn thừa hưởng những đường nét mạnh mẽ và chắc chắn mang dấu ấn thương hiệu Dell tiền nhiệm. Hơn nữa, dòng sản phẩm này còn được nhà sản xuất trang bị bộ vi xử lý mạnh mẽ, mang đến hiệu năng vượt trội, đáp ứng nhu cầu học tập và làm việc của người dùng, đi kèm với giá thành hợp lý phù hợp với túi tiền của các bạn học sinh, sinh viên.'),
(16, 'CPU Intel Core i5-5257U 2.9GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Không', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Laptop Dell Inspiron N3467 vẫn thừa hưởng những đường nét mạnh mẽ và chắc chắn mang dấu ấn thương hiệu Dell tiền nhiệm. Hơn nữa, dòng sản phẩm này còn được nhà sản xuất trang bị bộ vi xử lý mạnh mẽ, mang đến hiệu năng vượt trội, đáp ứng nhu cầu học tập và làm việc của người dùng, đi kèm với giá thành hợp lý phù hợp với túi tiền của các bạn học sinh, sinh viên.'),
(17, 'CPU Intel Core i5-5257U 3.1GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', ' SSD 128GB', 'Ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Với thiết kế cổ điển và màu xanh đen ấn tượng bên ngoài, chiếc Laptop Asus E402NA-GA034 thu hút người dùng, nhất là các bạn trẻ ngay từ cái nhìn đầu tiên. Đặc biệt, thiết bị này còn sở hữu kiểu dáng gọn gàng cùng cấu hình cực kỳ mạnh mẽ bên trong, cho hiệu năng cực tốt, phục vụ nhu cầu học tập, làm việc và giải trí đa phương tiện. '),
(18, 'CPU Intel Core i5-5257U 2.7GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 500GB', 'Ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Với thiết kế cổ điển và màu xanh đen ấn tượng bên ngoài, chiếc Laptop Asus E402NA-GA034 thu hút người dùng, nhất là các bạn trẻ ngay từ cái nhìn đầu tiên. Đặc biệt, thiết bị này còn sở hữu kiểu dáng gọn gàng cùng cấu hình cực kỳ mạnh mẽ bên trong, cho hiệu năng cực tốt, phục vụ nhu cầu học tập, làm việc và giải trí đa phương tiện. '),
(19, 'CPU Intel Core i5-5257U 3.0GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '4 Cell', 'Không', 'Với thiết kế cổ điển và màu xanh đen ấn tượng bên ngoài, chiếc Laptop Asus E402NA-GA034 thu hút người dùng, nhất là các bạn trẻ ngay từ cái nhìn đầu tiên. Đặc biệt, thiết bị này còn sở hữu kiểu dáng gọn gàng cùng cấu hình cực kỳ mạnh mẽ bên trong, cho hiệu năng cực tốt, phục vụ nhu cầu học tập, làm việc và giải trí đa phương tiện. '),
(20, 'CPU Intel Core i5-5257U 2.7GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Với thiết kế cổ điển và màu xanh đen ấn tượng bên ngoài, chiếc Laptop Asus E402NA-GA034 thu hút người dùng, nhất là các bạn trẻ ngay từ cái nhìn đầu tiên. Đặc biệt, thiết bị này còn sở hữu kiểu dáng gọn gàng cùng cấu hình cực kỳ mạnh mẽ bên trong, cho hiệu năng cực tốt, phục vụ nhu cầu học tập, làm việc và giải trí đa phương tiện. '),
(21, 'CPU Intel Core i5-5257U 3.0GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Không ', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Với thiết kế cổ điển và màu xanh đen ấn tượng bên ngoài, chiếc Laptop Asus E402NA-GA034 thu hút người dùng, nhất là các bạn trẻ ngay từ cái nhìn đầu tiên. Đặc biệt, thiết bị này còn sở hữu kiểu dáng gọn gàng cùng cấu hình cực kỳ mạnh mẽ bên trong, cho hiệu năng cực tốt, phục vụ nhu cầu học tập, làm việc và giải trí đa phương tiện. '),
(22, 'CPU Intel Core i5-5257U 2.7GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Không', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '4 Cell', 'Không', 'Với thiết kế cổ điển và màu xanh đen ấn tượng bên ngoài, chiếc Laptop Asus E402NA-GA034 thu hút người dùng, nhất là các bạn trẻ ngay từ cái nhìn đầu tiên. Đặc biệt, thiết bị này còn sở hữu kiểu dáng gọn gàng cùng cấu hình cực kỳ mạnh mẽ bên trong, cho hiệu năng cực tốt, phục vụ nhu cầu học tập, làm việc và giải trí đa phương tiện. '),
(23, 'CPU Intel Core i5-5257U 3.1GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Với thiết kế cổ điển và màu xanh đen ấn tượng bên ngoài, chiếc Laptop Asus E402NA-GA034 thu hút người dùng, nhất là các bạn trẻ ngay từ cái nhìn đầu tiên. Đặc biệt, thiết bị này còn sở hữu kiểu dáng gọn gàng cùng cấu hình cực kỳ mạnh mẽ bên trong, cho hiệu năng cực tốt, phục vụ nhu cầu học tập, làm việc và giải trí đa phương tiện. '),
(24, 'CPU Intel Core i5-5257U 2.7GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Không', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Với thiết kế cổ điển và màu xanh đen ấn tượng bên ngoài, chiếc Laptop Asus E402NA-GA034 thu hút người dùng, nhất là các bạn trẻ ngay từ cái nhìn đầu tiên. Đặc biệt, thiết bị này còn sở hữu kiểu dáng gọn gàng cùng cấu hình cực kỳ mạnh mẽ bên trong, cho hiệu năng cực tốt, phục vụ nhu cầu học tập, làm việc và giải trí đa phương tiện. '),
(25, 'CPU Intel Core i5-5257U 2.7GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Do được thừa hưởng nhiều ưu điểm của dòng sản phẩm HP 15, chiếc Laptop HP 15-bs576TU có thiết kế tuyệt đẹp, sang trọng và màn hình hiển thị hình ảnh cực nét. Bên cạnh đó, một lý do nữa khiến bạn không thể bỏ qua thiết bị này chính là cấu hình vô cùng mạnh mẽ, phục vụ cho nhu cầu xử lý các công việc văn phòng hay giải trí sau những giờ làm việc căng thẳng. Hơn thế nữa, sản phẩm này được thương hiệu HP trình làng với giá thành hết sức hợp lý, vừa túi tiền của phần đông người dùng.'),
(26, 'CPU Intel Core i5-5257U 3.1GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 500GB', 'Ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Do được thừa hưởng nhiều ưu điểm của dòng sản phẩm HP 15, chiếc Laptop HP 15-bs576TU có thiết kế tuyệt đẹp, sang trọng và màn hình hiển thị hình ảnh cực nét. Bên cạnh đó, một lý do nữa khiến bạn không thể bỏ qua thiết bị này chính là cấu hình vô cùng mạnh mẽ, phục vụ cho nhu cầu xử lý các công việc văn phòng hay giải trí sau những giờ làm việc căng thẳng. Hơn thế nữa, sản phẩm này được thương hiệu HP trình làng với giá thành hết sức hợp lý, vừa túi tiền của phần đông người dùng.'),
(27, 'CPU Intel Core i5-5257U 2.7GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '4 Cell', 'Không', 'Do được thừa hưởng nhiều ưu điểm của dòng sản phẩm HP 15, chiếc Laptop HP 15-bs576TU có thiết kế tuyệt đẹp, sang trọng và màn hình hiển thị hình ảnh cực nét. Bên cạnh đó, một lý do nữa khiến bạn không thể bỏ qua thiết bị này chính là cấu hình vô cùng mạnh mẽ, phục vụ cho nhu cầu xử lý các công việc văn phòng hay giải trí sau những giờ làm việc căng thẳng. Hơn thế nữa, sản phẩm này được thương hiệu HP trình làng với giá thành hết sức hợp lý, vừa túi tiền của phần đông người dùng.'),
(28, 'CPU Intel Core i5-5257U 3.0GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Do được thừa hưởng nhiều ưu điểm của dòng sản phẩm HP 15, chiếc Laptop HP 15-bs576TU có thiết kế tuyệt đẹp, sang trọng và màn hình hiển thị hình ảnh cực nét. Bên cạnh đó, một lý do nữa khiến bạn không thể bỏ qua thiết bị này chính là cấu hình vô cùng mạnh mẽ, phục vụ cho nhu cầu xử lý các công việc văn phòng hay giải trí sau những giờ làm việc căng thẳng. Hơn thế nữa, sản phẩm này được thương hiệu HP trình làng với giá thành hết sức hợp lý, vừa túi tiền của phần đông người dùng.'),
(29, 'CPU Intel Core i5-5257U 2.7GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '3 Cell', 'Không', 'Do được thừa hưởng nhiều ưu điểm của dòng sản phẩm HP 15, chiếc Laptop HP 15-bs576TU có thiết kế tuyệt đẹp, sang trọng và màn hình hiển thị hình ảnh cực nét. Bên cạnh đó, một lý do nữa khiến bạn không thể bỏ qua thiết bị này chính là cấu hình vô cùng mạnh mẽ, phục vụ cho nhu cầu xử lý các công việc văn phòng hay giải trí sau những giờ làm việc căng thẳng. Hơn thế nữa, sản phẩm này được thương hiệu HP trình làng với giá thành hết sức hợp lý, vừa túi tiền của phần đông người dùng.'),
(30, 'CPU Intel Core i5-5257U 3.0GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', ' NVIDIA Geforce GTX 1050 4 GB', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'ổ Đĩa Quang', '2x USB 2.0, USB 3.0, USB-C, HDMI', '  Stereo speakers', '4 Cell', 'Không', 'Do được thừa hưởng nhiều ưu điểm của dòng sản phẩm HP 15, chiếc Laptop HP 15-bs576TU có thiết kế tuyệt đẹp, sang trọng và màn hình hiển thị hình ảnh cực nét. Bên cạnh đó, một lý do nữa khiến bạn không thể bỏ qua thiết bị này chính là cấu hình vô cùng mạnh mẽ, phục vụ cho nhu cầu xử lý các công việc văn phòng hay giải trí sau những giờ làm việc căng thẳng. Hơn thế nữa, sản phẩm này được thương hiệu HP trình làng với giá thành hết sức hợp lý, vừa túi tiền của phần đông người dùng.'),
(31, 'CPU Intel Core i5-5257U 3.1GHz/3MB', '8GB 1600Mhz', '13.3 inch 1440 x 900 pixels  LED-backlit', '', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Ổ Đĩa Quang', '', '  Stereo speakers', '3 Cell', '', 'Do được thừa hưởng nhiều ưu điểm của dòng sản phẩm HP 15, chiếc Laptop HP 15-bs576TU có thiết kế tuyệt đẹp, sang trọng và màn hình hiển thị hình ảnh cực nét. Bên cạnh đó, một lý do nữa khiến bạn không thể bỏ qua thiết bị này chính là cấu hình vô cùng mạnh mẽ, phục vụ cho nhu cầu xử lý các công việc văn phòng hay giải trí sau những giờ làm việc căng thẳng. Hơn thế nữa, sản phẩm này được thương hiệu HP trình làng với giá thành hết sức hợp lý, vừa túi tiền của phần đông người dùng.'),
(32, 'CPU Intel Core i5-5257U 2.7GHz/3MB', '8GB 1600Mhz', '13.3 inch 1366 x 768 pixels  LED-backlit', '', 'Window, Linux', '  3 MB (L3 Cache)', '  SSD 128GB', 'Không', '', '  Stereo speakers', '3 Cell', '', 'Do được thừa hưởng nhiều ưu điểm của dòng sản phẩm HP 15, chiếc Laptop HP 15-bs576TU có thiết kế tuyệt đẹp, sang trọng và màn hình hiển thị hình ảnh cực nét. Bên cạnh đó, một lý do nữa khiến bạn không thể bỏ qua thiết bị này chính là cấu hình vô cùng mạnh mẽ, phục vụ cho nhu cầu xử lý các công việc văn phòng hay giải trí sau những giờ làm việc căng thẳng. Hơn thế nữa, sản phẩm này được thương hiệu HP trình làng với giá thành hết sức hợp lý, vừa túi tiền của phần đông người dùng.');

-- --------------------------------------------------------

--
-- Table structure for table `tin_tuc`
--

CREATE TABLE IF NOT EXISTS `tin_tuc` (
`TT_Ma` int(11) NOT NULL,
  `TT_TieuDe` varchar(200) NOT NULL,
  `TT_NgayViet` date NOT NULL,
  `TT_NoiDung` text,
  `TT_HinhAnh` varchar(200) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tin_tuc`
--

INSERT INTO `tin_tuc` (`TT_Ma`, `TT_TieuDe`, `TT_NgayViet`, `TT_NoiDung`, `TT_HinhAnh`) VALUES
(1, 'Chọn laptop nào phù hợp với lập trình viên?', '2017-11-07', '<b>Cách đây không lâu, trong một cuộc phỏng vấn, anh Mark Zúc Zúc gì đó, CEO Facebook đã từng nói rằng: “Laptop lập trình là điều quan trọng nhất, còn những thứ khác có hay không có, không quan trọng!”</b>\r\n<br><br>Bill Gates, ngày vừa sáng lập Microsoft, cũng có một câu tương tự: “ Có hai thứ mà lập trình viên giỏi nào cũng phải có. Thứ đầu tiên là một người thầy giỏi, động viên và dẫn dắt ta đi đúng hướng. Thứ còn chính là một chiếc laptop nhanh, bền, tốt, luôn bên ta trên con đường học nghề và lập trình.”\r\nĐương nhiên là 2 bác ở trên không nói ra câu này, do Code Dạo chế ra cả thôi. Nói có vẻ đùa nhưng sự thật đúng là như vậy!\r\n<br><br>Laptop là một công cụ tuyệt vời vừa để học và làm việc, vừa để xem phim heo và vui chơi giải trí. Đối với developer, chiếc laptop còn là một người bạn, người anh em đồng hành tin cậy trên quãng đường viết code và thực hành nghề. Laptop tốt sẽ khiến công việc lập trình “dễ thở” hơn nhiều!\r\n<br><br>Do vậy, trong bài viết này, mình sẽ chia sẻ một số tiêu chí để chọn laptop lập trình, cùng một số mẫu laptop phù hợp cho các bạn tham khảo nhé\r\n<br><br><b>Laptop nào mà chả lập trình được? Sao phải chọn?</b>\r\n<br><br>Đương nhiên là hầu như laptop nào cũng dùng để lập trình được, nhưng một chiếc laptop mạnh sẽ làm tăng năng suất làm việc của bạn hơn. Đừng tiếc tiền mà dùng máy dỏm, vì một chiếc laptop có hiệu năng cao sẽ tiết kiệm của bạn rất nhiều thời gian và tiền bạc về sau.\r\n<br><br>Giả sử bạn dùng laptop 8-10 tiếng một ngày thì chiếc máy nhanh hơn 10% sẽ giúp bạn tiết kiệm mỗi ngày 1 tiếng, 1 năm tiết kiệm được 365 tiếng... tức hơn 2 tuần.\r\n<br><br>Chọn một chiếc laptop tử tế ngay từ đầu cũng sẽ giúp bạn tiết kiệm được nhiều chi phí để nâng cấp, sửa chữa về sau. Chưa kể, nếu bạn muốn theo nghiệp lập trình game, lập trình di động thì phải có laptop mạnh hoặc chuyên biệt thì mới đáp ứng được nhu cầu.\r\n<br><br>Tất nhiên, khi đi làm thì thường công ty sẽ cung cấp cho bạn một cỗ máy đủ mạnh để code và làm việc. Tuy nhiên, những lúc tự học, tự test công nghệ mới, làm dự án cá nhân, bạn vẫn phải nghịch ngợm trên chiếc laptop nho nhỏ của mình thôi ;).\r\n<br><br><b>Những yếu tố cần lưu ý khi lựa chọn laptop</b>\r\n<br><br>Laptop cũng có 5 - 7 loại, đắt có rẻ có, tha hồ chọn lựa. Sau đây, mình sẽ liệt kê một số tiêu chí các bạn cần lưu ý khi lựa chọn laptop để lập trình:\r\n<br><br>Cấu hình\r\n<br><br> • CPU: Càng nhanh càng tốt, ít nhất lên mua chip i5 hoặc i7, tốc độ xử lý từ 3Ghz lên là ok.\r\n<br><br> • RAM: Càng nhiều càng tốt, ít nhất phải là 8GB vì có một số IDE ăn RAM rất khủng. Nếu được thì mua luôn 16GB sẽ dễ thở hơn, muốn chạy máy ảo cũng không phải lo nghĩ gì.\r\n<br><br> • Ổ cứng: Nên chọn loại có ổ SSD hoặc lắp thêm ổ SSD. Chi phí bỏ ra không nhiều nhưng lại tăng tốc độ của máy lên nhiều lần. Ổ cứng nên kiếm khoảng 500GB-1TB vì sẽ cần lưu trữ nhiều: tài liệu học tập, các phầm mềm khủng,…\r\n<br><br> • Card đồ hoạ: Thật ra cái này không cần lắm, dùng card onboard vẫn code bay tóc rồi. Tuy nhiên, nếu làm lập trình game, làm đồ hoạ hoặc cần encode video thì nên sắm loại có card rời. Nếu muốn chơi game thì kiếm con nào card ổn ổn một tí nhé.\r\n<br><br>Tuy nhiên, nhiều bạn khi mua máy cứ chăm chăm vào cấu hình mà quên các yếu tố khác rất quan trọng.\r\n<br><br><b>Vậy ngoài cấu hình thì cần để ý cái chi?</b>\r\n<br><br> • Trọng lượng: Máy nặng hay nhẹ, có dễ mang đi hay không? Nếu hay di chuyển thì nên mua loại nhẹ, màn hình nhỏ. Nếu ít di chuyển thì nên mua loại to và nặng hơn, code cho sướng.\r\n<br><br> • Pin và tản nhiệt: Pin trâu hay pin yếu, tản nhiệt như thế nào, xài lâu có bị nóng không?\r\n<br><br> • Bàn phím: Bàn phím để code gõ có thoải mái không? Đây là thứ rất quan trọng vì bạn phải gõ code rất nhiều. Ngoài ra, chúng ta thường hay code đêm nên hãy ưu tiên loại có đèn bàn phím.\r\n<br><br> • Độ bền: Máy có bền hay không, bảo hành bao lâu? Laptop sẽ theo bạn ít nhất 4 năm đại học nên hãy hỏi bạn bè xem hãng đó máy xài bền hay mau hỏng, chế độ bảo hành thế nào?\r\n<br><br> • Giá: Giá cả ra sao? Có phù hợp túi tiền không?\r\n<br><br><b>Một số mẫu laptop phù hợp</b>\r\n<br><br>Dưới đây, mình có khuyến khích một số loại laptop dựa theo ý kiến cá nhân, nếu có loại nào ổn hơn anh em cứ giới thiệu nhé:\r\n<br><br>Ngon bổ rẻ\r\n<br><br> • HP 15-AY013NR 15.6-inch: Giá không cao, CPU i5-6200U, RAM 8GB, SSD 128GB (nên gắn thêm), card onboard Intel. Quá đủ để code.\r\nCho các bạn có nhiều tiền\r\n<br><br> • Dòng Macbook Pro: Máy đẹp, chụp tự sướng trông rất bảnh, nhẹ nên tiện dụng dễ mang theo, pin trâu 7-8 tiếng. Máy chạy bền và ổn định, tắt mở rất nhanh, hỗ trợ command line và các công cụ lập trình rất tốt. Nhược điểm là giá hơi cao, đôi khi cần cài thêm Windows để code một số thứ. Ngoài ra Mac không thích hợp chơi game nên game không nhiều, khó chạy các game khủng.\r\n<br><br> • Dòng Alienware: Cấu hình ngon, chơi game và code đã, máy ngầu hầm hố. Tuy nhiên giá hơi cao và do "hầm hố" nên máy và cục sạc hơi nặng, vác theo rất mệt.\r\n<br><br> • Dell Latitude E7470 Business Ultrabook\r\n<br><br> • Asus K501UW-AB78 15.6-inch\r\n<br><br> • Dòng Dell XPS 13\r\n<br><br>Tầm trung\r\n<br><br> • Lenovo Yoga 710 15.6-inch\r\n<br><br> • Asus Q304ua 13.3-inch 2-in-1 Touchscreen Full Hd\r\n<br><br> • Toshiba Satellite L55 15.6-inch\r\n<br><br>Tầm thấp, giá rẻ đủ code\r\n<br><br> • HP 14-AN013NR 14-inch Notebook\r\n<br><br> • Acer Chromebook CB3-131-C3SZ\r\n<br><br> • Acer Aspire ES 15\r\n<br><br>Kết\r\n<br><br>Xét cho cùng, laptop là công cụ nên việc dùng công cụ thế nào còn tùy thuộc rất lớn vào bản thân người sử dụng.\r\n<br><br>Bạn có laptop xịn 40-50 triệu mà chỉ dùng để suốt ngày chơi game, lướt Facebook thì cũng chỉ tổ phí tiền, còn không bằng thằng bạn chỉ có laptop 6-7 triệu nhưng dùng để lướt blog Tôi đi code dạo, đọc Medium, học code online, làm dự án cá nhân để học.\r\n<br><br>Hi vọng bài viết giúp các bạn lựa chọn được người bạn ưng ý trên bước đường lập trình nhé. Có thắc mắc hay góp ý gì mọi người cứ viết trong mục comment nha!\r\n<br><br>Thông tin tác giả\r\n<br><br>Anh Phạm Huy Hoàng hiện đang theo học Thạc sĩ ngành Computer Science tại Đại học Lancaster, UK. Hoàng cũng là chủ blog Tôi Đi Code Dạo khá nổi tiếng tại Việt Nam với hơn 2.5 triệu lượt xem và 30000 lượt follow fanpage.\r\n<br><br>Anh có hơn 3 năm kinh nghiệm trong lĩnh vực phần mềm và đam mê nghiên cứu về bảo mật, công nghệ web, các công nghệ mới. Anh từng phát hiện và công bố lỗ hổng bảo mật của Lotte Cinema và Lozi.vn.\r\n', 'news/tintuc2.jpg'),
(2, 'Món "đồ chơi" này sẽ là cứu tinh cho những ai chuyên vào quán cafe tự kỷ với laptop', '2017-11-17', '<b>Cách đây không lâu, trong một cuộc phỏng vấn, anh Mark Zúc Zúc gì đó, CEO Facebook đã từng nói rằng: “Laptop lập trình là điều quan trọng nhất, còn những thứ khác có hay không có, không quan trọng!”</b>\r\n<br><br>Bill Gates, ngày vừa sáng lập Microsoft, cũng có một câu tương tự: “ Có hai thứ mà lập trình viên giỏi nào cũng phải có. Thứ đầu tiên là một người thầy giỏi, động viên và dẫn dắt ta đi đúng hướng. Thứ còn chính là một chiếc laptop nhanh, bền, tốt, luôn bên ta trên con đường học nghề và lập trình.”\r\nĐương nhiên là 2 bác ở trên không nói ra câu này, do Code Dạo chế ra cả thôi. Nói có vẻ đùa nhưng sự thật đúng là như vậy!\r\n<br><br>Laptop là một công cụ tuyệt vời vừa để học và làm việc, vừa để xem phim heo và vui chơi giải trí. Đối với developer, chiếc laptop còn là một người bạn, người anh em đồng hành tin cậy trên quãng đường viết code và thực hành nghề. Laptop tốt sẽ khiến công việc lập trình “dễ thở” hơn nhiều!\r\n<br><br>Do vậy, trong bài viết này, mình sẽ chia sẻ một số tiêu chí để chọn laptop lập trình, cùng một số mẫu laptop phù hợp cho các bạn tham khảo nhé\r\n<br><br><b>Laptop nào mà chả lập trình được? Sao phải chọn?</b>\r\n<br><br>Đương nhiên là hầu như laptop nào cũng dùng để lập trình được, nhưng một chiếc laptop mạnh sẽ làm tăng năng suất làm việc của bạn hơn. Đừng tiếc tiền mà dùng máy dỏm, vì một chiếc laptop có hiệu năng cao sẽ tiết kiệm của bạn rất nhiều thời gian và tiền bạc về sau.\r\n<br><br>Giả sử bạn dùng laptop 8-10 tiếng một ngày thì chiếc máy nhanh hơn 10% sẽ giúp bạn tiết kiệm mỗi ngày 1 tiếng, 1 năm tiết kiệm được 365 tiếng... tức hơn 2 tuần.\r\n<br><br>Chọn một chiếc laptop tử tế ngay từ đầu cũng sẽ giúp bạn tiết kiệm được nhiều chi phí để nâng cấp, sửa chữa về sau. Chưa kể, nếu bạn muốn theo nghiệp lập trình game, lập trình di động thì phải có laptop mạnh hoặc chuyên biệt thì mới đáp ứng được nhu cầu.\r\n<br><br>Tất nhiên, khi đi làm thì thường công ty sẽ cung cấp cho bạn một cỗ máy đủ mạnh để code và làm việc. Tuy nhiên, những lúc tự học, tự test công nghệ mới, làm dự án cá nhân, bạn vẫn phải nghịch ngợm trên chiếc laptop nho nhỏ của mình thôi ;).\r\n<br><br><b>Những yếu tố cần lưu ý khi lựa chọn laptop</b>\r\n<br><br>Laptop cũng có 5 - 7 loại, đắt có rẻ có, tha hồ chọn lựa. Sau đây, mình sẽ liệt kê một số tiêu chí các bạn cần lưu ý khi lựa chọn laptop để lập trình:\r\n<br><br>Cấu hình\r\n<br><br>&emsp;• CPU: Càng nhanh càng tốt, ít nhất lên mua chip i5 hoặc i7, tốc độ xử lý từ 3Ghz lên là ok.\r\n<br><br>&emsp;• RAM: Càng nhiều càng tốt, ít nhất phải là 8GB vì có một số IDE ăn RAM rất khủng. Nếu được thì mua luôn 16GB sẽ dễ thở hơn, muốn chạy máy ảo cũng không phải lo nghĩ gì.\r\n<br><br>&emsp;• Ổ cứng: Nên chọn loại có ổ SSD hoặc lắp thêm ổ SSD. Chi phí bỏ ra không nhiều nhưng lại tăng tốc độ của máy lên nhiều lần. Ổ cứng nên kiếm khoảng 500GB-1TB vì sẽ cần lưu trữ nhiều: tài liệu học tập, các phầm mềm khủng,…\r\n<br><br>&emsp;• Card đồ hoạ: Thật ra cái này không cần lắm, dùng card onboard vẫn code bay tóc rồi. Tuy nhiên, nếu làm lập trình game, làm đồ hoạ hoặc cần encode video thì nên sắm loại có card rời. Nếu muốn chơi game thì kiếm con nào card ổn ổn một tí nhé.\r\n<br><br>Tuy nhiên, nhiều bạn khi mua máy cứ chăm chăm vào cấu hình mà quên các yếu tố khác rất quan trọng.\r\n<br><br><b>Vậy ngoài cấu hình thì cần để ý cái chi?</b>\r\n<br><br>&emsp;• Trọng lượng: Máy nặng hay nhẹ, có dễ mang đi hay không? Nếu hay di chuyển thì nên mua loại nhẹ, màn hình nhỏ. Nếu ít di chuyển thì nên mua loại to và nặng hơn, code cho sướng.\r\n<br><br>&emsp;• Pin và tản nhiệt: Pin trâu hay pin yếu, tản nhiệt như thế nào, xài lâu có bị nóng không?\r\n<br><br>&emsp;• Bàn phím: Bàn phím để code gõ có thoải mái không? Đây là thứ rất quan trọng vì bạn phải gõ code rất nhiều. Ngoài ra, chúng ta thường hay code đêm nên hãy ưu tiên loại có đèn bàn phím.\r\n<br><br>&emsp;• Độ bền: Máy có bền hay không, bảo hành bao lâu? Laptop sẽ theo bạn ít nhất 4 năm đại học nên hãy hỏi bạn bè xem hãng đó máy xài bền hay mau hỏng, chế độ bảo hành thế nào?\r\n<br><br>&emsp;• Giá: Giá cả ra sao? Có phù hợp túi tiền không?\r\n<br><br><b>Một số mẫu laptop phù hợp</b>\r\n<br><br>Dưới đây, mình có khuyến khích một số loại laptop dựa theo ý kiến cá nhân, nếu có loại nào ổn hơn anh em cứ giới thiệu nhé:\r\n<br><br>Ngon bổ rẻ\r\n<br><br>&emsp;• HP 15-AY013NR 15.6-inch: Giá không cao, CPU i5-6200U, RAM 8GB, SSD 128GB (nên gắn thêm), card onboard Intel. Quá đủ để code.\r\nCho các bạn có nhiều tiền\r\n<br><br>&emsp;• Dòng Macbook Pro: Máy đẹp, chụp tự sướng trông rất bảnh, nhẹ nên tiện dụng dễ mang theo, pin trâu 7-8 tiếng. Máy chạy bền và ổn định, tắt mở rất nhanh, hỗ trợ command line và các công cụ lập trình rất tốt. Nhược điểm là giá hơi cao, đôi khi cần cài thêm Windows để code một số thứ. Ngoài ra Mac không thích hợp chơi game nên game không nhiều, khó chạy các game khủng.\r\n<br><br>&emsp;• Dòng Alienware: Cấu hình ngon, chơi game và code đã, máy ngầu hầm hố. Tuy nhiên giá hơi cao và do "hầm hố" nên máy và cục sạc hơi nặng, vác theo rất mệt.\r\n<br><br>&emsp;• Dell Latitude E7470 Business Ultrabook\r\n<br><br>&emsp;• Asus K501UW-AB78 15.6-inch\r\n<br><br>&emsp;• Dòng Dell XPS 13\r\n<br><br>Tầm trung\r\n<br><br>&emsp;• Lenovo Yoga 710 15.6-inch\r\n<br><br>&emsp;• Asus Q304ua 13.3-inch 2-in-1 Touchscreen Full Hd\r\n<br><br>&emsp;• Toshiba Satellite L55 15.6-inch\r\n<br><br>Tầm thấp, giá rẻ đủ code\r\n<br><br>&emsp;• HP 14-AN013NR 14-inch Notebook\r\n<br><br>&emsp;• Acer Chromebook CB3-131-C3SZ\r\n<br><br>&emsp;• Acer Aspire ES 15\r\n<br><br>Kết\r\n<br><br>Xét cho cùng, laptop là công cụ nên việc dùng công cụ thế nào còn tùy thuộc rất lớn vào bản thân người sử dụng.\r\n<br><br>Bạn có laptop xịn 40-50 triệu mà chỉ dùng để suốt ngày chơi game, lướt Facebook thì cũng chỉ tổ phí tiền, còn không bằng thằng bạn chỉ có laptop 6-7 triệu nhưng dùng để lướt blog Tôi đi code dạo, đọc Medium, học code online, làm dự án cá nhân để học.\r\n<br><br>Hi vọng bài viết giúp các bạn lựa chọn được người bạn ưng ý trên bước đường lập trình nhé. Có thắc mắc hay góp ý gì mọi người cứ viết trong mục comment nha!\r\n<br><br>Thông tin tác giả\r\n<br><br>Anh Phạm Huy Hoàng hiện đang theo học Thạc sĩ ngành Computer Science tại Đại học Lancaster, UK. Hoàng cũng là chủ blog Tôi Đi Code Dạo khá nổi tiếng tại Việt Nam với hơn 2.5 triệu lượt xem và 30000 lượt follow fanpage.\r\n<br><br>Anh có hơn 3 năm kinh nghiệm trong lĩnh vực phần mềm và đam mê nghiên cứu về bảo mật, công nghệ web, các công nghệ mới. Anh từng phát hiện và công bố lỗ hổng bảo mật của Lotte Cinema và Lozi.vn.\r\n', 'news/tintuc3.png'),
(3, 'HP Envy thế hệ mới ra mắt tại Việt Nam: dùng chip Core-I thế hệ thứ 7, gọn nhẹ, sang trọng hơn, giá từ 20,99 triệu đồng', '2017-11-18', '<b>Cách đây không lâu, trong một cuộc phỏng vấn, anh Mark Zúc Zúc gì đó, CEO Facebook đã từng nói rằng: “Laptop lập trình là điều quan trọng nhất, còn những thứ khác có hay không có, không quan trọng!”</b><br><br>Bill Gates, ngày vừa sáng lập Microsoft, cũng có một câu tương tự: “ Có hai thứ mà lập trình viên giỏi nào cũng phải có. Thứ đầu tiên là một người thầy giỏi, động viên và dẫn dắt ta đi đúng hướng. Thứ còn chính là một chiếc laptop nhanh, bền, tốt, luôn bên ta trên con đường học nghề và lập trình.”Đương nhiên là 2 bác ở trên không nói ra câu này, do Code Dạo chế ra cả thôi. Nói có vẻ đùa nhưng sự thật đúng là như vậy!<br><br>Laptop là một công cụ tuyệt vời vừa để học và làm việc, vừa để xem phim heo và vui chơi giải trí. Đối với developer, chiếc laptop còn là một người bạn, người anh em đồng hành tin cậy trên quãng đường viết code và thực hành nghề. Laptop tốt sẽ khiến công việc lập trình “dễ thở” hơn nhiều!<br><br>Do vậy, trong bài viết này, mình sẽ chia sẻ một số tiêu chí để chọn laptop lập trình, cùng một số mẫu laptop phù hợp cho các bạn tham khảo nhé<br><br><b>Laptop nào mà chả lập trình được? Sao phải chọn?</b><br><br>Đương nhiên là hầu như laptop nào cũng dùng để lập trình được, nhưng một chiếc laptop mạnh sẽ làm tăng năng suất làm việc của bạn hơn. Đừng tiếc tiền mà dùng máy dỏm, vì một chiếc laptop có hiệu năng cao sẽ tiết kiệm của bạn rất nhiều thời gian và tiền bạc về sau.<br><br>Giả sử bạn dùng laptop 8-10 tiếng một ngày thì chiếc máy nhanh hơn 10% sẽ giúp bạn tiết kiệm mỗi ngày 1 tiếng, 1 năm tiết kiệm được 365 tiếng... tức hơn 2 tuần.<br><br>Chọn một chiếc laptop tử tế ngay từ đầu cũng sẽ giúp bạn tiết kiệm được nhiều chi phí để nâng cấp, sửa chữa về sau. Chưa kể, nếu bạn muốn theo nghiệp lập trình game, lập trình di động thì phải có laptop mạnh hoặc chuyên biệt thì mới đáp ứng được nhu cầu.<br><br>Tất nhiên, khi đi làm thì thường công ty sẽ cung cấp cho bạn một cỗ máy đủ mạnh để code và làm việc. Tuy nhiên, những lúc tự học, tự test công nghệ mới, làm dự án cá nhân, bạn vẫn phải nghịch ngợm trên chiếc laptop nho nhỏ của mình thôi ;).<br><br><b>Những yếu tố cần lưu ý khi lựa chọn laptop</b><br><br>Laptop cũng có 5 - 7 loại, đắt có rẻ có, tha hồ chọn lựa. Sau đây, mình sẽ liệt kê một số tiêu chí các bạn cần lưu ý khi lựa chọn laptop để lập trình:<br><br>Cấu hình<br><br>&emsp;• CPU: Càng nhanh càng tốt, ít nhất lên mua chip i5 hoặc i7, tốc độ xử lý từ 3Ghz lên là ok.<br><br>&emsp;• RAM: Càng nhiều càng tốt, ít nhất phải là 8GB vì có một số IDE ăn RAM rất khủng. Nếu được thì mua luôn 16GB sẽ dễ thở hơn, muốn chạy máy ảo cũng không phải lo nghĩ gì.<br><br>&emsp;• Ổ cứng: Nên chọn loại có ổ SSD hoặc lắp thêm ổ SSD. Chi phí bỏ ra không nhiều nhưng lại tăng tốc độ của máy lên nhiều lần. Ổ cứng nên kiếm khoảng 500GB-1TB vì sẽ cần lưu trữ nhiều: tài liệu học tập, các phầm mềm khủng,…<br><br>&emsp;• Card đồ hoạ: Thật ra cái này không cần lắm, dùng card onboard vẫn code bay tóc rồi. Tuy nhiên, nếu làm lập trình game, làm đồ hoạ hoặc cần encode video thì nên sắm loại có card rời. Nếu muốn chơi game thì kiếm con nào card ổn ổn một tí nhé.<br><br>Tuy nhiên, nhiều bạn khi mua máy cứ chăm chăm vào cấu hình mà quên các yếu tố khác rất quan trọng.<br><br><b>Vậy ngoài cấu hình thì cần để ý cái chi?</b><br><br>&emsp;• Trọng lượng: Máy nặng hay nhẹ, có dễ mang đi hay không? Nếu hay di chuyển thì nên mua loại nhẹ, màn hình nhỏ. Nếu ít di chuyển thì nên mua loại to và nặng hơn, code cho sướng.<br><br>&emsp;• Pin và tản nhiệt: Pin trâu hay pin yếu, tản nhiệt như thế nào, xài lâu có bị nóng không?<br><br>&emsp;• Bàn phím: Bàn phím để code gõ có thoải mái không? Đây là thứ rất quan trọng vì bạn phải gõ code rất nhiều. Ngoài ra, chúng ta thường hay code đêm nên hãy ưu tiên loại có đèn bàn phím.<br><br>&emsp;• Độ bền: Máy có bền hay không, bảo hành bao lâu? Laptop sẽ theo bạn ít nhất 4 năm đại học nên hãy hỏi bạn bè xem hãng đó máy xài bền hay mau hỏng, chế độ bảo hành thế nào?<br><br>&emsp;• Giá: Giá cả ra sao? Có phù hợp túi tiền không?<br><br><b>Một số mẫu laptop phù hợp</b><br><br>Dưới đây, mình có khuyến khích một số loại laptop dựa theo ý kiến cá nhân, nếu có loại nào ổn hơn anh em cứ giới thiệu nhé:<br><br>Ngon bổ rẻ<br><br>&emsp;• HP 15-AY013NR 15.6-inch: Giá không cao, CPU i5-6200U, RAM 8GB, SSD 128GB (nên gắn thêm), card onboard Intel. Quá đủ để code.Cho các bạn có nhiều tiền<br><br>&emsp;• Dòng Macbook Pro: Máy đẹp, chụp tự sướng trông rất bảnh, nhẹ nên tiện dụng dễ mang theo, pin trâu 7-8 tiếng. Máy chạy bền và ổn định, tắt mở rất nhanh, hỗ trợ command line và các công cụ lập trình rất tốt. Nhược điểm là giá hơi cao, đôi khi cần cài thêm Windows để code một số thứ. Ngoài ra Mac không thích hợp chơi game nên game không nhiều, khó chạy các game khủng.<br><br>&emsp;• Dòng Alienware: Cấu hình ngon, chơi game và code đã, máy ngầu hầm hố. Tuy nhiên giá hơi cao và do "hầm hố" nên máy và cục sạc hơi nặng, vác theo rất mệt.<br><br>&emsp;• Dell Latitude E7470 Business Ultrabook<br><br>&emsp;• Asus K501UW-AB78 15.6-inch<br><br>&emsp;• Dòng Dell XPS 13<br><br>Tầm trung<br><br>&emsp;• Lenovo Yoga 710 15.6-inch<br><br>&emsp;• Asus Q304ua 13.3-inch 2-in-1 Touchscreen Full Hd<br><br>&emsp;• Toshiba Satellite L55 15.6-inch<br><br>Tầm thấp, giá rẻ đủ code<br><br>&emsp;• HP 14-AN013NR 14-inch Notebook<br><br>&emsp;• Acer Chromebook CB3-131-C3SZ<br><br>&emsp;• Acer Aspire ES 15<br><br>Kết<br><br>Xét cho cùng, laptop là công cụ nên việc dùng công cụ thế nào còn tùy thuộc rất lớn vào bản thân người sử dụng.<br><br>Bạn có laptop xịn 40-50 triệu mà chỉ dùng để suốt ngày chơi game, lướt Facebook thì cũng chỉ tổ phí tiền, còn không bằng thằng bạn chỉ có laptop 6-7 triệu nhưng dùng để lướt blog Tôi đi code dạo, đọc Medium, học code online, làm dự án cá nhân để học.<br><br>Hi vọng bài viết giúp các bạn lựa chọn được người bạn ưng ý trên bước đường lập trình nhé. Có thắc mắc hay góp ý gì mọi người cứ viết trong mục comment nha!<br><br>Thông tin tác giả<br><br>Anh Phạm Huy Hoàng hiện đang theo học Thạc sĩ ngành Computer Science tại Đại học Lancaster, UK. Hoàng cũng là chủ blog Tôi Đi Code Dạo khá nổi tiếng tại Việt Nam với hơn 2.5 triệu lượt xem và 30000 lượt follow fanpage.<br><br>Anh có hơn 3 năm kinh nghiệm trong lĩnh vực phần mềm và đam mê nghiên cứu về bảo mật, công nghệ web, các công nghệ mới. Anh từng phát hiện và công bố lỗ hổng bảo mật của Lotte Cinema và Lozi.vn.', 'news/tintuc4.jpg'),
(4, 'Điểm mặt những chiếc laptop “chất" nhất sử dụng CPU Intel thế hệ thứ 8', '2017-06-11', '<b>Cách đây không lâu, trong một cuộc phỏng vấn, anh Mark Zúc Zúc gì đó, CEO Facebook đã từng nói rằng: “Laptop lập trình là điều quan trọng nhất, còn những thứ khác có hay không có, không quan trọng!”</b>\r\n<br><br>Bill Gates, ngày vừa sáng lập Microsoft, cũng có một câu tương tự: “ Có hai thứ mà lập trình viên giỏi nào cũng phải có. Thứ đầu tiên là một người thầy giỏi, động viên và dẫn dắt ta đi đúng hướng. Thứ còn chính là một chiếc laptop nhanh, bền, tốt, luôn bên ta trên con đường học nghề và lập trình.”\r\nĐương nhiên là 2 bác ở trên không nói ra câu này, do Code Dạo chế ra cả thôi. Nói có vẻ đùa nhưng sự thật đúng là như vậy!\r\n<br><br>Laptop là một công cụ tuyệt vời vừa để học và làm việc, vừa để xem phim heo và vui chơi giải trí. Đối với developer, chiếc laptop còn là một người bạn, người anh em đồng hành tin cậy trên quãng đường viết code và thực hành nghề. Laptop tốt sẽ khiến công việc lập trình “dễ thở” hơn nhiều!\r\n<br><br>Do vậy, trong bài viết này, mình sẽ chia sẻ một số tiêu chí để chọn laptop lập trình, cùng một số mẫu laptop phù hợp cho các bạn tham khảo nhé\r\n<br><br><b>Laptop nào mà chả lập trình được? Sao phải chọn?</b>\r\n<br><br>Đương nhiên là hầu như laptop nào cũng dùng để lập trình được, nhưng một chiếc laptop mạnh sẽ làm tăng năng suất làm việc của bạn hơn. Đừng tiếc tiền mà dùng máy dỏm, vì một chiếc laptop có hiệu năng cao sẽ tiết kiệm của bạn rất nhiều thời gian và tiền bạc về sau.\r\n<br><br>Giả sử bạn dùng laptop 8-10 tiếng một ngày thì chiếc máy nhanh hơn 10% sẽ giúp bạn tiết kiệm mỗi ngày 1 tiếng, 1 năm tiết kiệm được 365 tiếng... tức hơn 2 tuần.\r\n<br><br>Chọn một chiếc laptop tử tế ngay từ đầu cũng sẽ giúp bạn tiết kiệm được nhiều chi phí để nâng cấp, sửa chữa về sau. Chưa kể, nếu bạn muốn theo nghiệp lập trình game, lập trình di động thì phải có laptop mạnh hoặc chuyên biệt thì mới đáp ứng được nhu cầu.\r\n<br><br>Tất nhiên, khi đi làm thì thường công ty sẽ cung cấp cho bạn một cỗ máy đủ mạnh để code và làm việc. Tuy nhiên, những lúc tự học, tự test công nghệ mới, làm dự án cá nhân, bạn vẫn phải nghịch ngợm trên chiếc laptop nho nhỏ của mình thôi ;).\r\n<br><br><b>Những yếu tố cần lưu ý khi lựa chọn laptop</b>\r\n<br><br>Laptop cũng có 5 - 7 loại, đắt có rẻ có, tha hồ chọn lựa. Sau đây, mình sẽ liệt kê một số tiêu chí các bạn cần lưu ý khi lựa chọn laptop để lập trình:\r\n<br><br>Cấu hình\r\n<br><br>&emsp;• CPU: Càng nhanh càng tốt, ít nhất lên mua chip i5 hoặc i7, tốc độ xử lý từ 3Ghz lên là ok.\r\n<br><br>&emsp;• RAM: Càng nhiều càng tốt, ít nhất phải là 8GB vì có một số IDE ăn RAM rất khủng. Nếu được thì mua luôn 16GB sẽ dễ thở hơn, muốn chạy máy ảo cũng không phải lo nghĩ gì.\r\n<br><br>&emsp;• Ổ cứng: Nên chọn loại có ổ SSD hoặc lắp thêm ổ SSD. Chi phí bỏ ra không nhiều nhưng lại tăng tốc độ của máy lên nhiều lần. Ổ cứng nên kiếm khoảng 500GB-1TB vì sẽ cần lưu trữ nhiều: tài liệu học tập, các phầm mềm khủng,…\r\n<br><br>&emsp;• Card đồ hoạ: Thật ra cái này không cần lắm, dùng card onboard vẫn code bay tóc rồi. Tuy nhiên, nếu làm lập trình game, làm đồ hoạ hoặc cần encode video thì nên sắm loại có card rời. Nếu muốn chơi game thì kiếm con nào card ổn ổn một tí nhé.\r\n<br><br>Tuy nhiên, nhiều bạn khi mua máy cứ chăm chăm vào cấu hình mà quên các yếu tố khác rất quan trọng.\r\n<br><br><b>Vậy ngoài cấu hình thì cần để ý cái chi?</b>\r\n<br><br>&emsp;• Trọng lượng: Máy nặng hay nhẹ, có dễ mang đi hay không? Nếu hay di chuyển thì nên mua loại nhẹ, màn hình nhỏ. Nếu ít di chuyển thì nên mua loại to và nặng hơn, code cho sướng.\r\n<br><br>&emsp;• Pin và tản nhiệt: Pin trâu hay pin yếu, tản nhiệt như thế nào, xài lâu có bị nóng không?\r\n<br><br>&emsp;• Bàn phím: Bàn phím để code gõ có thoải mái không? Đây là thứ rất quan trọng vì bạn phải gõ code rất nhiều. Ngoài ra, chúng ta thường hay code đêm nên hãy ưu tiên loại có đèn bàn phím.\r\n<br><br>&emsp;• Độ bền: Máy có bền hay không, bảo hành bao lâu? Laptop sẽ theo bạn ít nhất 4 năm đại học nên hãy hỏi bạn bè xem hãng đó máy xài bền hay mau hỏng, chế độ bảo hành thế nào?\r\n<br><br>&emsp;• Giá: Giá cả ra sao? Có phù hợp túi tiền không?\r\n<br><br><b>Một số mẫu laptop phù hợp</b>\r\n<br><br>Dưới đây, mình có khuyến khích một số loại laptop dựa theo ý kiến cá nhân, nếu có loại nào ổn hơn anh em cứ giới thiệu nhé:\r\n<br><br>Ngon bổ rẻ\r\n<br><br>&emsp;• HP 15-AY013NR 15.6-inch: Giá không cao, CPU i5-6200U, RAM 8GB, SSD 128GB (nên gắn thêm), card onboard Intel. Quá đủ để code.\r\nCho các bạn có nhiều tiền\r\n<br><br>&emsp;• Dòng Macbook Pro: Máy đẹp, chụp tự sướng trông rất bảnh, nhẹ nên tiện dụng dễ mang theo, pin trâu 7-8 tiếng. Máy chạy bền và ổn định, tắt mở rất nhanh, hỗ trợ command line và các công cụ lập trình rất tốt. Nhược điểm là giá hơi cao, đôi khi cần cài thêm Windows để code một số thứ. Ngoài ra Mac không thích hợp chơi game nên game không nhiều, khó chạy các game khủng.\r\n<br><br>&emsp;• Dòng Alienware: Cấu hình ngon, chơi game và code đã, máy ngầu hầm hố. Tuy nhiên giá hơi cao và do "hầm hố" nên máy và cục sạc hơi nặng, vác theo rất mệt.\r\n<br><br>&emsp;• Dell Latitude E7470 Business Ultrabook\r\n<br><br>&emsp;• Asus K501UW-AB78 15.6-inch\r\n<br><br>&emsp;• Dòng Dell XPS 13\r\n<br><br>Tầm trung\r\n<br><br>&emsp;• Lenovo Yoga 710 15.6-inch\r\n<br><br>&emsp;• Asus Q304ua 13.3-inch 2-in-1 Touchscreen Full Hd\r\n<br><br>&emsp;• Toshiba Satellite L55 15.6-inch\r\n<br><br>Tầm thấp, giá rẻ đủ code\r\n<br><br>&emsp;• HP 14-AN013NR 14-inch Notebook\r\n<br><br>&emsp;• Acer Chromebook CB3-131-C3SZ\r\n<br><br>&emsp;• Acer Aspire ES 15\r\n<br><br>Kết\r\n<br><br>Xét cho cùng, laptop là công cụ nên việc dùng công cụ thế nào còn tùy thuộc rất lớn vào bản thân người sử dụng.\r\n<br><br>Bạn có laptop xịn 40-50 triệu mà chỉ dùng để suốt ngày chơi game, lướt Facebook thì cũng chỉ tổ phí tiền, còn không bằng thằng bạn chỉ có laptop 6-7 triệu nhưng dùng để lướt blog Tôi đi code dạo, đọc Medium, học code online, làm dự án cá nhân để học.\r\n<br><br>Hi vọng bài viết giúp các bạn lựa chọn được người bạn ưng ý trên bước đường lập trình nhé. Có thắc mắc hay góp ý gì mọi người cứ viết trong mục comment nha!\r\n<br><br>Thông tin tác giả\r\n<br><br>Anh Phạm Huy Hoàng hiện đang theo học Thạc sĩ ngành Computer Science tại Đại học Lancaster, UK. Hoàng cũng là chủ blog Tôi Đi Code Dạo khá nổi tiếng tại Việt Nam với hơn 2.5 triệu lượt xem và 30000 lượt follow fanpage.\r\n<br><br>Anh có hơn 3 năm kinh nghiệm trong lĩnh vực phần mềm và đam mê nghiên cứu về bảo mật, công nghệ web, các công nghệ mới. Anh từng phát hiện và công bố lỗ hổng bảo mật của Lotte Cinema và Lozi.vn.\r\n', 'news/tintuc5.png'),
(5, 'NVIDIA chuẩn bị ra mắt dòng card đồ họa GeForce 1040 cho laptop giá rẻ', '2017-06-11', '<b>Cách đây không lâu, trong một cuộc phỏng vấn, anh Mark Zúc Zúc gì đó, CEO Facebook đã từng nói rằng: “Laptop lập trình là điều quan trọng nhất, còn những thứ khác có hay không có, không quan trọng!”</b>\r\n<br><br>Bill Gates, ngày vừa sáng lập Microsoft, cũng có một câu tương tự: “ Có hai thứ mà lập trình viên giỏi nào cũng phải có. Thứ đầu tiên là một người thầy giỏi, động viên và dẫn dắt ta đi đúng hướng. Thứ còn chính là một chiếc laptop nhanh, bền, tốt, luôn bên ta trên con đường học nghề và lập trình.”\r\nĐương nhiên là 2 bác ở trên không nói ra câu này, do Code Dạo chế ra cả thôi. Nói có vẻ đùa nhưng sự thật đúng là như vậy!\r\n<br><br>Laptop là một công cụ tuyệt vời vừa để học và làm việc, vừa để xem phim heo và vui chơi giải trí. Đối với developer, chiếc laptop còn là một người bạn, người anh em đồng hành tin cậy trên quãng đường viết code và thực hành nghề. Laptop tốt sẽ khiến công việc lập trình “dễ thở” hơn nhiều!\r\n<br><br>Do vậy, trong bài viết này, mình sẽ chia sẻ một số tiêu chí để chọn laptop lập trình, cùng một số mẫu laptop phù hợp cho các bạn tham khảo nhé\r\n<br><br><b>Laptop nào mà chả lập trình được? Sao phải chọn?</b>\r\n<br><br>Đương nhiên là hầu như laptop nào cũng dùng để lập trình được, nhưng một chiếc laptop mạnh sẽ làm tăng năng suất làm việc của bạn hơn. Đừng tiếc tiền mà dùng máy dỏm, vì một chiếc laptop có hiệu năng cao sẽ tiết kiệm của bạn rất nhiều thời gian và tiền bạc về sau.\r\n<br><br>Giả sử bạn dùng laptop 8-10 tiếng một ngày thì chiếc máy nhanh hơn 10% sẽ giúp bạn tiết kiệm mỗi ngày 1 tiếng, 1 năm tiết kiệm được 365 tiếng... tức hơn 2 tuần.\r\n<br><br>Chọn một chiếc laptop tử tế ngay từ đầu cũng sẽ giúp bạn tiết kiệm được nhiều chi phí để nâng cấp, sửa chữa về sau. Chưa kể, nếu bạn muốn theo nghiệp lập trình game, lập trình di động thì phải có laptop mạnh hoặc chuyên biệt thì mới đáp ứng được nhu cầu.\r\n<br><br>Tất nhiên, khi đi làm thì thường công ty sẽ cung cấp cho bạn một cỗ máy đủ mạnh để code và làm việc. Tuy nhiên, những lúc tự học, tự test công nghệ mới, làm dự án cá nhân, bạn vẫn phải nghịch ngợm trên chiếc laptop nho nhỏ của mình thôi ;).\r\n<br><br><b>Những yếu tố cần lưu ý khi lựa chọn laptop</b>\r\n<br><br>Laptop cũng có 5 - 7 loại, đắt có rẻ có, tha hồ chọn lựa. Sau đây, mình sẽ liệt kê một số tiêu chí các bạn cần lưu ý khi lựa chọn laptop để lập trình:\r\n<br><br>Cấu hình\r\n<br><br>&emsp;• CPU: Càng nhanh càng tốt, ít nhất lên mua chip i5 hoặc i7, tốc độ xử lý từ 3Ghz lên là ok.\r\n<br><br>&emsp;• RAM: Càng nhiều càng tốt, ít nhất phải là 8GB vì có một số IDE ăn RAM rất khủng. Nếu được thì mua luôn 16GB sẽ dễ thở hơn, muốn chạy máy ảo cũng không phải lo nghĩ gì.\r\n<br><br>&emsp;• Ổ cứng: Nên chọn loại có ổ SSD hoặc lắp thêm ổ SSD. Chi phí bỏ ra không nhiều nhưng lại tăng tốc độ của máy lên nhiều lần. Ổ cứng nên kiếm khoảng 500GB-1TB vì sẽ cần lưu trữ nhiều: tài liệu học tập, các phầm mềm khủng,…\r\n<br><br>&emsp;• Card đồ hoạ: Thật ra cái này không cần lắm, dùng card onboard vẫn code bay tóc rồi. Tuy nhiên, nếu làm lập trình game, làm đồ hoạ hoặc cần encode video thì nên sắm loại có card rời. Nếu muốn chơi game thì kiếm con nào card ổn ổn một tí nhé.\r\n<br><br>Tuy nhiên, nhiều bạn khi mua máy cứ chăm chăm vào cấu hình mà quên các yếu tố khác rất quan trọng.\r\n<br><br><b>Vậy ngoài cấu hình thì cần để ý cái chi?</b>\r\n<br><br>&emsp;• Trọng lượng: Máy nặng hay nhẹ, có dễ mang đi hay không? Nếu hay di chuyển thì nên mua loại nhẹ, màn hình nhỏ. Nếu ít di chuyển thì nên mua loại to và nặng hơn, code cho sướng.\r\n<br><br>&emsp;• Pin và tản nhiệt: Pin trâu hay pin yếu, tản nhiệt như thế nào, xài lâu có bị nóng không?\r\n<br><br>&emsp;• Bàn phím: Bàn phím để code gõ có thoải mái không? Đây là thứ rất quan trọng vì bạn phải gõ code rất nhiều. Ngoài ra, chúng ta thường hay code đêm nên hãy ưu tiên loại có đèn bàn phím.\r\n<br><br>&emsp;• Độ bền: Máy có bền hay không, bảo hành bao lâu? Laptop sẽ theo bạn ít nhất 4 năm đại học nên hãy hỏi bạn bè xem hãng đó máy xài bền hay mau hỏng, chế độ bảo hành thế nào?\r\n<br><br>&emsp;• Giá: Giá cả ra sao? Có phù hợp túi tiền không?\r\n<br><br><b>Một số mẫu laptop phù hợp</b>\r\n<br><br>Dưới đây, mình có khuyến khích một số loại laptop dựa theo ý kiến cá nhân, nếu có loại nào ổn hơn anh em cứ giới thiệu nhé:\r\n<br><br>Ngon bổ rẻ\r\n<br><br>&emsp;• HP 15-AY013NR 15.6-inch: Giá không cao, CPU i5-6200U, RAM 8GB, SSD 128GB (nên gắn thêm), card onboard Intel. Quá đủ để code.\r\nCho các bạn có nhiều tiền\r\n<br><br>&emsp;• Dòng Macbook Pro: Máy đẹp, chụp tự sướng trông rất bảnh, nhẹ nên tiện dụng dễ mang theo, pin trâu 7-8 tiếng. Máy chạy bền và ổn định, tắt mở rất nhanh, hỗ trợ command line và các công cụ lập trình rất tốt. Nhược điểm là giá hơi cao, đôi khi cần cài thêm Windows để code một số thứ. Ngoài ra Mac không thích hợp chơi game nên game không nhiều, khó chạy các game khủng.\r\n<br><br>&emsp;• Dòng Alienware: Cấu hình ngon, chơi game và code đã, máy ngầu hầm hố. Tuy nhiên giá hơi cao và do "hầm hố" nên máy và cục sạc hơi nặng, vác theo rất mệt.\r\n<br><br>&emsp;• Dell Latitude E7470 Business Ultrabook\r\n<br><br>&emsp;• Asus K501UW-AB78 15.6-inch\r\n<br><br>&emsp;• Dòng Dell XPS 13\r\n<br><br>Tầm trung\r\n<br><br>&emsp;• Lenovo Yoga 710 15.6-inch\r\n<br><br>&emsp;• Asus Q304ua 13.3-inch 2-in-1 Touchscreen Full Hd\r\n<br><br>&emsp;• Toshiba Satellite L55 15.6-inch\r\n<br><br>Tầm thấp, giá rẻ đủ code\r\n<br><br>&emsp;• HP 14-AN013NR 14-inch Notebook\r\n<br><br>&emsp;• Acer Chromebook CB3-131-C3SZ\r\n<br><br>&emsp;• Acer Aspire ES 15\r\n<br><br>Kết\r\n<br><br>Xét cho cùng, laptop là công cụ nên việc dùng công cụ thế nào còn tùy thuộc rất lớn vào bản thân người sử dụng.\r\n<br><br>Bạn có laptop xịn 40-50 triệu mà chỉ dùng để suốt ngày chơi game, lướt Facebook thì cũng chỉ tổ phí tiền, còn không bằng thằng bạn chỉ có laptop 6-7 triệu nhưng dùng để lướt blog Tôi đi code dạo, đọc Medium, học code online, làm dự án cá nhân để học.\r\n<br><br>Hi vọng bài viết giúp các bạn lựa chọn được người bạn ưng ý trên bước đường lập trình nhé. Có thắc mắc hay góp ý gì mọi người cứ viết trong mục comment nha!\r\n<br><br>Thông tin tác giả\r\n<br><br>Anh Phạm Huy Hoàng hiện đang theo học Thạc sĩ ngành Computer Science tại Đại học Lancaster, UK. Hoàng cũng là chủ blog Tôi Đi Code Dạo khá nổi tiếng tại Việt Nam với hơn 2.5 triệu lượt xem và 30000 lượt follow fanpage.\r\n<br><br>Anh có hơn 3 năm kinh nghiệm trong lĩnh vực phần mềm và đam mê nghiên cứu về bảo mật, công nghệ web, các công nghệ mới. Anh từng phát hiện và công bố lỗ hổng bảo mật của Lotte Cinema và Lozi.vn.\r\n', 'news/tintuc6.jpg');
INSERT INTO `tin_tuc` (`TT_Ma`, `TT_TieuDe`, `TT_NgayViet`, `TT_NoiDung`, `TT_HinhAnh`) VALUES
(6, 'Giữa tâm điểm mùa tựu trường, 3 dòng laptop ASUS sẽ bứt phá với khuyến mãi hấp dẫn', '2017-06-11', '<b>Cách đây không lâu, trong một cuộc phỏng vấn, anh Mark Zúc Zúc gì đó, CEO Facebook đã từng nói rằng: “Laptop lập trình là điều quan trọng nhất, còn những thứ khác có hay không có, không quan trọng!”</b>\r\n<br><br>Bill Gates, ngày vừa sáng lập Microsoft, cũng có một câu tương tự: “ Có hai thứ mà lập trình viên giỏi nào cũng phải có. Thứ đầu tiên là một người thầy giỏi, động viên và dẫn dắt ta đi đúng hướng. Thứ còn chính là một chiếc laptop nhanh, bền, tốt, luôn bên ta trên con đường học nghề và lập trình.”\r\nĐương nhiên là 2 bác ở trên không nói ra câu này, do Code Dạo chế ra cả thôi. Nói có vẻ đùa nhưng sự thật đúng là như vậy!\r\n<br><br>Laptop là một công cụ tuyệt vời vừa để học và làm việc, vừa để xem phim heo và vui chơi giải trí. Đối với developer, chiếc laptop còn là một người bạn, người anh em đồng hành tin cậy trên quãng đường viết code và thực hành nghề. Laptop tốt sẽ khiến công việc lập trình “dễ thở” hơn nhiều!\r\n<br><br>Do vậy, trong bài viết này, mình sẽ chia sẻ một số tiêu chí để chọn laptop lập trình, cùng một số mẫu laptop phù hợp cho các bạn tham khảo nhé\r\n<br><br><b>Laptop nào mà chả lập trình được? Sao phải chọn?</b>\r\n<br><br>Đương nhiên là hầu như laptop nào cũng dùng để lập trình được, nhưng một chiếc laptop mạnh sẽ làm tăng năng suất làm việc của bạn hơn. Đừng tiếc tiền mà dùng máy dỏm, vì một chiếc laptop có hiệu năng cao sẽ tiết kiệm của bạn rất nhiều thời gian và tiền bạc về sau.\r\n<br><br>Giả sử bạn dùng laptop 8-10 tiếng một ngày thì chiếc máy nhanh hơn 10% sẽ giúp bạn tiết kiệm mỗi ngày 1 tiếng, 1 năm tiết kiệm được 365 tiếng... tức hơn 2 tuần.\r\n<br><br>Chọn một chiếc laptop tử tế ngay từ đầu cũng sẽ giúp bạn tiết kiệm được nhiều chi phí để nâng cấp, sửa chữa về sau. Chưa kể, nếu bạn muốn theo nghiệp lập trình game, lập trình di động thì phải có laptop mạnh hoặc chuyên biệt thì mới đáp ứng được nhu cầu.\r\n<br><br>Tất nhiên, khi đi làm thì thường công ty sẽ cung cấp cho bạn một cỗ máy đủ mạnh để code và làm việc. Tuy nhiên, những lúc tự học, tự test công nghệ mới, làm dự án cá nhân, bạn vẫn phải nghịch ngợm trên chiếc laptop nho nhỏ của mình thôi ;).\r\n<br><br><b>Những yếu tố cần lưu ý khi lựa chọn laptop</b>\r\n<br><br>Laptop cũng có 5 - 7 loại, đắt có rẻ có, tha hồ chọn lựa. Sau đây, mình sẽ liệt kê một số tiêu chí các bạn cần lưu ý khi lựa chọn laptop để lập trình:\r\n<br><br>Cấu hình\r\n<br><br>&emsp;• CPU: Càng nhanh càng tốt, ít nhất lên mua chip i5 hoặc i7, tốc độ xử lý từ 3Ghz lên là ok.\r\n<br><br>&emsp;• RAM: Càng nhiều càng tốt, ít nhất phải là 8GB vì có một số IDE ăn RAM rất khủng. Nếu được thì mua luôn 16GB sẽ dễ thở hơn, muốn chạy máy ảo cũng không phải lo nghĩ gì.\r\n<br><br>&emsp;• Ổ cứng: Nên chọn loại có ổ SSD hoặc lắp thêm ổ SSD. Chi phí bỏ ra không nhiều nhưng lại tăng tốc độ của máy lên nhiều lần. Ổ cứng nên kiếm khoảng 500GB-1TB vì sẽ cần lưu trữ nhiều: tài liệu học tập, các phầm mềm khủng,…\r\n<br><br>&emsp;• Card đồ hoạ: Thật ra cái này không cần lắm, dùng card onboard vẫn code bay tóc rồi. Tuy nhiên, nếu làm lập trình game, làm đồ hoạ hoặc cần encode video thì nên sắm loại có card rời. Nếu muốn chơi game thì kiếm con nào card ổn ổn một tí nhé.\r\n<br><br>Tuy nhiên, nhiều bạn khi mua máy cứ chăm chăm vào cấu hình mà quên các yếu tố khác rất quan trọng.\r\n<br><br><b>Vậy ngoài cấu hình thì cần để ý cái chi?</b>\r\n<br><br>&emsp;• Trọng lượng: Máy nặng hay nhẹ, có dễ mang đi hay không? Nếu hay di chuyển thì nên mua loại nhẹ, màn hình nhỏ. Nếu ít di chuyển thì nên mua loại to và nặng hơn, code cho sướng.\r\n<br><br>&emsp;• Pin và tản nhiệt: Pin trâu hay pin yếu, tản nhiệt như thế nào, xài lâu có bị nóng không?\r\n<br><br>&emsp;• Bàn phím: Bàn phím để code gõ có thoải mái không? Đây là thứ rất quan trọng vì bạn phải gõ code rất nhiều. Ngoài ra, chúng ta thường hay code đêm nên hãy ưu tiên loại có đèn bàn phím.\r\n<br><br>&emsp;• Độ bền: Máy có bền hay không, bảo hành bao lâu? Laptop sẽ theo bạn ít nhất 4 năm đại học nên hãy hỏi bạn bè xem hãng đó máy xài bền hay mau hỏng, chế độ bảo hành thế nào?\r\n<br><br>&emsp;• Giá: Giá cả ra sao? Có phù hợp túi tiền không?\r\n<br><br><b>Một số mẫu laptop phù hợp</b>\r\n<br><br>Dưới đây, mình có khuyến khích một số loại laptop dựa theo ý kiến cá nhân, nếu có loại nào ổn hơn anh em cứ giới thiệu nhé:\r\n<br><br>Ngon bổ rẻ\r\n<br><br>&emsp;• HP 15-AY013NR 15.6-inch: Giá không cao, CPU i5-6200U, RAM 8GB, SSD 128GB (nên gắn thêm), card onboard Intel. Quá đủ để code.\r\nCho các bạn có nhiều tiền\r\n<br><br>&emsp;• Dòng Macbook Pro: Máy đẹp, chụp tự sướng trông rất bảnh, nhẹ nên tiện dụng dễ mang theo, pin trâu 7-8 tiếng. Máy chạy bền và ổn định, tắt mở rất nhanh, hỗ trợ command line và các công cụ lập trình rất tốt. Nhược điểm là giá hơi cao, đôi khi cần cài thêm Windows để code một số thứ. Ngoài ra Mac không thích hợp chơi game nên game không nhiều, khó chạy các game khủng.\r\n<br><br>&emsp;• Dòng Alienware: Cấu hình ngon, chơi game và code đã, máy ngầu hầm hố. Tuy nhiên giá hơi cao và do "hầm hố" nên máy và cục sạc hơi nặng, vác theo rất mệt.\r\n<br><br>&emsp;• Dell Latitude E7470 Business Ultrabook\r\n<br><br>&emsp;• Asus K501UW-AB78 15.6-inch\r\n<br><br>&emsp;• Dòng Dell XPS 13\r\n<br><br>Tầm trung\r\n<br><br>&emsp;• Lenovo Yoga 710 15.6-inch\r\n<br><br>&emsp;• Asus Q304ua 13.3-inch 2-in-1 Touchscreen Full Hd\r\n<br><br>&emsp;• Toshiba Satellite L55 15.6-inch\r\n<br><br>Tầm thấp, giá rẻ đủ code\r\n<br><br>&emsp;• HP 14-AN013NR 14-inch Notebook\r\n<br><br>&emsp;• Acer Chromebook CB3-131-C3SZ\r\n<br><br>&emsp;• Acer Aspire ES 15\r\n<br><br>Kết\r\n<br><br>Xét cho cùng, laptop là công cụ nên việc dùng công cụ thế nào còn tùy thuộc rất lớn vào bản thân người sử dụng.\r\n<br><br>Bạn có laptop xịn 40-50 triệu mà chỉ dùng để suốt ngày chơi game, lướt Facebook thì cũng chỉ tổ phí tiền, còn không bằng thằng bạn chỉ có laptop 6-7 triệu nhưng dùng để lướt blog Tôi đi code dạo, đọc Medium, học code online, làm dự án cá nhân để học.\r\n<br><br>Hi vọng bài viết giúp các bạn lựa chọn được người bạn ưng ý trên bước đường lập trình nhé. Có thắc mắc hay góp ý gì mọi người cứ viết trong mục comment nha!\r\n<br><br>Thông tin tác giả\r\n<br><br>Anh Phạm Huy Hoàng hiện đang theo học Thạc sĩ ngành Computer Science tại Đại học Lancaster, UK. Hoàng cũng là chủ blog Tôi Đi Code Dạo khá nổi tiếng tại Việt Nam với hơn 2.5 triệu lượt xem và 30000 lượt follow fanpage.\r\n<br><br>Anh có hơn 3 năm kinh nghiệm trong lĩnh vực phần mềm và đam mê nghiên cứu về bảo mật, công nghệ web, các công nghệ mới. Anh từng phát hiện và công bố lỗ hổng bảo mật của Lotte Cinema và Lozi.vn.\r\n', 'news/tintuc7.jpg'),
(7, 'Lenovo giới thiệu laptop IdeaPad 320 chạy vi xử lý AMD tại thị trường Việt Nam, giá từ 8,5 triệu đồng', '2017-11-18', '<b>Cách đây không lâu, trong một cuộc phỏng vấn, anh Mark Zúc Zúc gì đó, CEO Facebook đã từng nói rằng: “Laptop lập trình là điều quan trọng nhất, còn những thứ khác có hay không có, không quan trọng!”</b>\r\n<br><br>Bill Gates, ngày vừa sáng lập Microsoft, cũng có một câu tương tự: “ Có hai thứ mà lập trình viên giỏi nào cũng phải có. Thứ đầu tiên là một người thầy giỏi, động viên và dẫn dắt ta đi đúng hướng. Thứ còn chính là một chiếc laptop nhanh, bền, tốt, luôn bên ta trên con đường học nghề và lập trình.”\r\nĐương nhiên là 2 bác ở trên không nói ra câu này, do Code Dạo chế ra cả thôi. Nói có vẻ đùa nhưng sự thật đúng là như vậy!\r\n<br><br>Laptop là một công cụ tuyệt vời vừa để học và làm việc, vừa để xem phim heo và vui chơi giải trí. Đối với developer, chiếc laptop còn là một người bạn, người anh em đồng hành tin cậy trên quãng đường viết code và thực hành nghề. Laptop tốt sẽ khiến công việc lập trình “dễ thở” hơn nhiều!\r\n<br><br>Do vậy, trong bài viết này, mình sẽ chia sẻ một số tiêu chí để chọn laptop lập trình, cùng một số mẫu laptop phù hợp cho các bạn tham khảo nhé\r\n<br><br><b>Laptop nào mà chả lập trình được? Sao phải chọn?</b>\r\n<br><br>Đương nhiên là hầu như laptop nào cũng dùng để lập trình được, nhưng một chiếc laptop mạnh sẽ làm tăng năng suất làm việc của bạn hơn. Đừng tiếc tiền mà dùng máy dỏm, vì một chiếc laptop có hiệu năng cao sẽ tiết kiệm của bạn rất nhiều thời gian và tiền bạc về sau.\r\n<br><br>Giả sử bạn dùng laptop 8-10 tiếng một ngày thì chiếc máy nhanh hơn 10% sẽ giúp bạn tiết kiệm mỗi ngày 1 tiếng, 1 năm tiết kiệm được 365 tiếng... tức hơn 2 tuần.\r\n<br><br>Chọn một chiếc laptop tử tế ngay từ đầu cũng sẽ giúp bạn tiết kiệm được nhiều chi phí để nâng cấp, sửa chữa về sau. Chưa kể, nếu bạn muốn theo nghiệp lập trình game, lập trình di động thì phải có laptop mạnh hoặc chuyên biệt thì mới đáp ứng được nhu cầu.\r\n<br><br>Tất nhiên, khi đi làm thì thường công ty sẽ cung cấp cho bạn một cỗ máy đủ mạnh để code và làm việc. Tuy nhiên, những lúc tự học, tự test công nghệ mới, làm dự án cá nhân, bạn vẫn phải nghịch ngợm trên chiếc laptop nho nhỏ của mình thôi ;).\r\n<br><br><b>Những yếu tố cần lưu ý khi lựa chọn laptop</b>\r\n<br><br>Laptop cũng có 5 - 7 loại, đắt có rẻ có, tha hồ chọn lựa. Sau đây, mình sẽ liệt kê một số tiêu chí các bạn cần lưu ý khi lựa chọn laptop để lập trình:\r\n<br><br>Cấu hình\r\n<br><br>&emsp;• CPU: Càng nhanh càng tốt, ít nhất lên mua chip i5 hoặc i7, tốc độ xử lý từ 3Ghz lên là ok.\r\n<br><br>&emsp;• RAM: Càng nhiều càng tốt, ít nhất phải là 8GB vì có một số IDE ăn RAM rất khủng. Nếu được thì mua luôn 16GB sẽ dễ thở hơn, muốn chạy máy ảo cũng không phải lo nghĩ gì.\r\n<br><br>&emsp;• Ổ cứng: Nên chọn loại có ổ SSD hoặc lắp thêm ổ SSD. Chi phí bỏ ra không nhiều nhưng lại tăng tốc độ của máy lên nhiều lần. Ổ cứng nên kiếm khoảng 500GB-1TB vì sẽ cần lưu trữ nhiều: tài liệu học tập, các phầm mềm khủng,…\r\n<br><br>&emsp;• Card đồ hoạ: Thật ra cái này không cần lắm, dùng card onboard vẫn code bay tóc rồi. Tuy nhiên, nếu làm lập trình game, làm đồ hoạ hoặc cần encode video thì nên sắm loại có card rời. Nếu muốn chơi game thì kiếm con nào card ổn ổn một tí nhé.\r\n<br><br>Tuy nhiên, nhiều bạn khi mua máy cứ chăm chăm vào cấu hình mà quên các yếu tố khác rất quan trọng.\r\n<br><br><b>Vậy ngoài cấu hình thì cần để ý cái chi?</b>\r\n<br><br>&emsp;• Trọng lượng: Máy nặng hay nhẹ, có dễ mang đi hay không? Nếu hay di chuyển thì nên mua loại nhẹ, màn hình nhỏ. Nếu ít di chuyển thì nên mua loại to và nặng hơn, code cho sướng.\r\n<br><br>&emsp;• Pin và tản nhiệt: Pin trâu hay pin yếu, tản nhiệt như thế nào, xài lâu có bị nóng không?\r\n<br><br>&emsp;• Bàn phím: Bàn phím để code gõ có thoải mái không? Đây là thứ rất quan trọng vì bạn phải gõ code rất nhiều. Ngoài ra, chúng ta thường hay code đêm nên hãy ưu tiên loại có đèn bàn phím.\r\n<br><br>&emsp;• Độ bền: Máy có bền hay không, bảo hành bao lâu? Laptop sẽ theo bạn ít nhất 4 năm đại học nên hãy hỏi bạn bè xem hãng đó máy xài bền hay mau hỏng, chế độ bảo hành thế nào?\r\n<br><br>&emsp;• Giá: Giá cả ra sao? Có phù hợp túi tiền không?\r\n<br><br><b>Một số mẫu laptop phù hợp</b>\r\n<br><br>Dưới đây, mình có khuyến khích một số loại laptop dựa theo ý kiến cá nhân, nếu có loại nào ổn hơn anh em cứ giới thiệu nhé:\r\n<br><br>Ngon bổ rẻ\r\n<br><br>&emsp;• HP 15-AY013NR 15.6-inch: Giá không cao, CPU i5-6200U, RAM 8GB, SSD 128GB (nên gắn thêm), card onboard Intel. Quá đủ để code.\r\nCho các bạn có nhiều tiền\r\n<br><br>&emsp;• Dòng Macbook Pro: Máy đẹp, chụp tự sướng trông rất bảnh, nhẹ nên tiện dụng dễ mang theo, pin trâu 7-8 tiếng. Máy chạy bền và ổn định, tắt mở rất nhanh, hỗ trợ command line và các công cụ lập trình rất tốt. Nhược điểm là giá hơi cao, đôi khi cần cài thêm Windows để code một số thứ. Ngoài ra Mac không thích hợp chơi game nên game không nhiều, khó chạy các game khủng.\r\n<br><br>&emsp;• Dòng Alienware: Cấu hình ngon, chơi game và code đã, máy ngầu hầm hố. Tuy nhiên giá hơi cao và do "hầm hố" nên máy và cục sạc hơi nặng, vác theo rất mệt.\r\n<br><br>&emsp;• Dell Latitude E7470 Business Ultrabook\r\n<br><br>&emsp;• Asus K501UW-AB78 15.6-inch\r\n<br><br>&emsp;• Dòng Dell XPS 13\r\n<br><br>Tầm trung\r\n<br><br>&emsp;• Lenovo Yoga 710 15.6-inch\r\n<br><br>&emsp;• Asus Q304ua 13.3-inch 2-in-1 Touchscreen Full Hd\r\n<br><br>&emsp;• Toshiba Satellite L55 15.6-inch\r\n<br><br>Tầm thấp, giá rẻ đủ code\r\n<br><br>&emsp;• HP 14-AN013NR 14-inch Notebook\r\n<br><br>&emsp;• Acer Chromebook CB3-131-C3SZ\r\n<br><br>&emsp;• Acer Aspire ES 15\r\n<br><br>Kết\r\n<br><br>Xét cho cùng, laptop là công cụ nên việc dùng công cụ thế nào còn tùy thuộc rất lớn vào bản thân người sử dụng.\r\n<br><br>Bạn có laptop xịn 40-50 triệu mà chỉ dùng để suốt ngày chơi game, lướt Facebook thì cũng chỉ tổ phí tiền, còn không bằng thằng bạn chỉ có laptop 6-7 triệu nhưng dùng để lướt blog Tôi đi code dạo, đọc Medium, học code online, làm dự án cá nhân để học.\r\n<br><br>Hi vọng bài viết giúp các bạn lựa chọn được người bạn ưng ý trên bước đường lập trình nhé. Có thắc mắc hay góp ý gì mọi người cứ viết trong mục comment nha!\r\n<br><br>Thông tin tác giả\r\n<br><br>Anh Phạm Huy Hoàng hiện đang theo học Thạc sĩ ngành Computer Science tại Đại học Lancaster, UK. Hoàng cũng là chủ blog Tôi Đi Code Dạo khá nổi tiếng tại Việt Nam với hơn 2.5 triệu lượt xem và 30000 lượt follow fanpage.\r\n<br><br>Anh có hơn 3 năm kinh nghiệm trong lĩnh vực phần mềm và đam mê nghiên cứu về bảo mật, công nghệ web, các công nghệ mới. Anh từng phát hiện và công bố lỗ hổng bảo mật của Lotte Cinema và Lozi.vn.\r\n', 'news/tintuc8.jpg'),
(8, 'AMD ra mắt chip Ryzen Mobile cho laptop: 4 lõi 8 luồng xử lý, tích hợp chip đồ họa Radeon Vega', '2017-06-11', '<b>Cách đây không lâu, trong một cuộc phỏng vấn, anh Mark Zúc Zúc gì đó, CEO Facebook đã từng nói rằng: “Laptop lập trình là điều quan trọng nhất, còn những thứ khác có hay không có, không quan trọng!”</b>\r\n<br><br>Bill Gates, ngày vừa sáng lập Microsoft, cũng có một câu tương tự: “ Có hai thứ mà lập trình viên giỏi nào cũng phải có. Thứ đầu tiên là một người thầy giỏi, động viên và dẫn dắt ta đi đúng hướng. Thứ còn chính là một chiếc laptop nhanh, bền, tốt, luôn bên ta trên con đường học nghề và lập trình.”\r\nĐương nhiên là 2 bác ở trên không nói ra câu này, do Code Dạo chế ra cả thôi. Nói có vẻ đùa nhưng sự thật đúng là như vậy!\r\n<br><br>Laptop là một công cụ tuyệt vời vừa để học và làm việc, vừa để xem phim heo và vui chơi giải trí. Đối với developer, chiếc laptop còn là một người bạn, người anh em đồng hành tin cậy trên quãng đường viết code và thực hành nghề. Laptop tốt sẽ khiến công việc lập trình “dễ thở” hơn nhiều!\r\n<br><br>Do vậy, trong bài viết này, mình sẽ chia sẻ một số tiêu chí để chọn laptop lập trình, cùng một số mẫu laptop phù hợp cho các bạn tham khảo nhé\r\n<br><br><b>Laptop nào mà chả lập trình được? Sao phải chọn?</b>\r\n<br><br>Đương nhiên là hầu như laptop nào cũng dùng để lập trình được, nhưng một chiếc laptop mạnh sẽ làm tăng năng suất làm việc của bạn hơn. Đừng tiếc tiền mà dùng máy dỏm, vì một chiếc laptop có hiệu năng cao sẽ tiết kiệm của bạn rất nhiều thời gian và tiền bạc về sau.\r\n<br><br>Giả sử bạn dùng laptop 8-10 tiếng một ngày thì chiếc máy nhanh hơn 10% sẽ giúp bạn tiết kiệm mỗi ngày 1 tiếng, 1 năm tiết kiệm được 365 tiếng... tức hơn 2 tuần.\r\n<br><br>Chọn một chiếc laptop tử tế ngay từ đầu cũng sẽ giúp bạn tiết kiệm được nhiều chi phí để nâng cấp, sửa chữa về sau. Chưa kể, nếu bạn muốn theo nghiệp lập trình game, lập trình di động thì phải có laptop mạnh hoặc chuyên biệt thì mới đáp ứng được nhu cầu.\r\n<br><br>Tất nhiên, khi đi làm thì thường công ty sẽ cung cấp cho bạn một cỗ máy đủ mạnh để code và làm việc. Tuy nhiên, những lúc tự học, tự test công nghệ mới, làm dự án cá nhân, bạn vẫn phải nghịch ngợm trên chiếc laptop nho nhỏ của mình thôi ;).\r\n<br><br><b>Những yếu tố cần lưu ý khi lựa chọn laptop</b>\r\n<br><br>Laptop cũng có 5 - 7 loại, đắt có rẻ có, tha hồ chọn lựa. Sau đây, mình sẽ liệt kê một số tiêu chí các bạn cần lưu ý khi lựa chọn laptop để lập trình:\r\n<br><br>Cấu hình\r\n<br><br>&emsp;• CPU: Càng nhanh càng tốt, ít nhất lên mua chip i5 hoặc i7, tốc độ xử lý từ 3Ghz lên là ok.\r\n<br><br>&emsp;• RAM: Càng nhiều càng tốt, ít nhất phải là 8GB vì có một số IDE ăn RAM rất khủng. Nếu được thì mua luôn 16GB sẽ dễ thở hơn, muốn chạy máy ảo cũng không phải lo nghĩ gì.\r\n<br><br>&emsp;• Ổ cứng: Nên chọn loại có ổ SSD hoặc lắp thêm ổ SSD. Chi phí bỏ ra không nhiều nhưng lại tăng tốc độ của máy lên nhiều lần. Ổ cứng nên kiếm khoảng 500GB-1TB vì sẽ cần lưu trữ nhiều: tài liệu học tập, các phầm mềm khủng,…\r\n<br><br>&emsp;• Card đồ hoạ: Thật ra cái này không cần lắm, dùng card onboard vẫn code bay tóc rồi. Tuy nhiên, nếu làm lập trình game, làm đồ hoạ hoặc cần encode video thì nên sắm loại có card rời. Nếu muốn chơi game thì kiếm con nào card ổn ổn một tí nhé.\r\n<br><br>Tuy nhiên, nhiều bạn khi mua máy cứ chăm chăm vào cấu hình mà quên các yếu tố khác rất quan trọng.\r\n<br><br><b>Vậy ngoài cấu hình thì cần để ý cái chi?</b>\r\n<br><br>&emsp;• Trọng lượng: Máy nặng hay nhẹ, có dễ mang đi hay không? Nếu hay di chuyển thì nên mua loại nhẹ, màn hình nhỏ. Nếu ít di chuyển thì nên mua loại to và nặng hơn, code cho sướng.\r\n<br><br>&emsp;• Pin và tản nhiệt: Pin trâu hay pin yếu, tản nhiệt như thế nào, xài lâu có bị nóng không?\r\n<br><br>&emsp;• Bàn phím: Bàn phím để code gõ có thoải mái không? Đây là thứ rất quan trọng vì bạn phải gõ code rất nhiều. Ngoài ra, chúng ta thường hay code đêm nên hãy ưu tiên loại có đèn bàn phím.\r\n<br><br>&emsp;• Độ bền: Máy có bền hay không, bảo hành bao lâu? Laptop sẽ theo bạn ít nhất 4 năm đại học nên hãy hỏi bạn bè xem hãng đó máy xài bền hay mau hỏng, chế độ bảo hành thế nào?\r\n<br><br>&emsp;• Giá: Giá cả ra sao? Có phù hợp túi tiền không?\r\n<br><br><b>Một số mẫu laptop phù hợp</b>\r\n<br><br>Dưới đây, mình có khuyến khích một số loại laptop dựa theo ý kiến cá nhân, nếu có loại nào ổn hơn anh em cứ giới thiệu nhé:\r\n<br><br>Ngon bổ rẻ\r\n<br><br>&emsp;• HP 15-AY013NR 15.6-inch: Giá không cao, CPU i5-6200U, RAM 8GB, SSD 128GB (nên gắn thêm), card onboard Intel. Quá đủ để code.\r\nCho các bạn có nhiều tiền\r\n<br><br>&emsp;• Dòng Macbook Pro: Máy đẹp, chụp tự sướng trông rất bảnh, nhẹ nên tiện dụng dễ mang theo, pin trâu 7-8 tiếng. Máy chạy bền và ổn định, tắt mở rất nhanh, hỗ trợ command line và các công cụ lập trình rất tốt. Nhược điểm là giá hơi cao, đôi khi cần cài thêm Windows để code một số thứ. Ngoài ra Mac không thích hợp chơi game nên game không nhiều, khó chạy các game khủng.\r\n<br><br>&emsp;• Dòng Alienware: Cấu hình ngon, chơi game và code đã, máy ngầu hầm hố. Tuy nhiên giá hơi cao và do "hầm hố" nên máy và cục sạc hơi nặng, vác theo rất mệt.\r\n<br><br>&emsp;• Dell Latitude E7470 Business Ultrabook\r\n<br><br>&emsp;• Asus K501UW-AB78 15.6-inch\r\n<br><br>&emsp;• Dòng Dell XPS 13\r\n<br><br>Tầm trung\r\n<br><br>&emsp;• Lenovo Yoga 710 15.6-inch\r\n<br><br>&emsp;• Asus Q304ua 13.3-inch 2-in-1 Touchscreen Full Hd\r\n<br><br>&emsp;• Toshiba Satellite L55 15.6-inch\r\n<br><br>Tầm thấp, giá rẻ đủ code\r\n<br><br>&emsp;• HP 14-AN013NR 14-inch Notebook\r\n<br><br>&emsp;• Acer Chromebook CB3-131-C3SZ\r\n<br><br>&emsp;• Acer Aspire ES 15\r\n<br><br>Kết\r\n<br><br>Xét cho cùng, laptop là công cụ nên việc dùng công cụ thế nào còn tùy thuộc rất lớn vào bản thân người sử dụng.\r\n<br><br>Bạn có laptop xịn 40-50 triệu mà chỉ dùng để suốt ngày chơi game, lướt Facebook thì cũng chỉ tổ phí tiền, còn không bằng thằng bạn chỉ có laptop 6-7 triệu nhưng dùng để lướt blog Tôi đi code dạo, đọc Medium, học code online, làm dự án cá nhân để học.\r\n<br><br>Hi vọng bài viết giúp các bạn lựa chọn được người bạn ưng ý trên bước đường lập trình nhé. Có thắc mắc hay góp ý gì mọi người cứ viết trong mục comment nha!\r\n<br><br>Thông tin tác giả\r\n<br><br>Anh Phạm Huy Hoàng hiện đang theo học Thạc sĩ ngành Computer Science tại Đại học Lancaster, UK. Hoàng cũng là chủ blog Tôi Đi Code Dạo khá nổi tiếng tại Việt Nam với hơn 2.5 triệu lượt xem và 30000 lượt follow fanpage.\r\n<br><br>Anh có hơn 3 năm kinh nghiệm trong lĩnh vực phần mềm và đam mê nghiên cứu về bảo mật, công nghệ web, các công nghệ mới. Anh từng phát hiện và công bố lỗ hổng bảo mật của Lotte Cinema và Lozi.vn.\r\n', 'news/tintuc9.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`AD_Ma`), ADD KEY `AD_MaPQ` (`PQ_Ma`);

--
-- Indexes for table `anh`
--
ALTER TABLE `anh`
 ADD PRIMARY KEY (`Anh_Ma`), ADD KEY `SP_Ma` (`SP_Ma`);

--
-- Indexes for table `bao_hanh`
--
ALTER TABLE `bao_hanh`
 ADD PRIMARY KEY (`BH_Ma`), ADD KEY `SP_Ma` (`DDH_Ma`);

--
-- Indexes for table `ct_don_dat_hang`
--
ALTER TABLE `ct_don_dat_hang`
 ADD PRIMARY KEY (`ID_CTDDH`), ADD KEY `SP_Ma` (`SP_Ma`), ADD KEY `DDH_Ma` (`DDH_Ma`);

--
-- Indexes for table `don_dat_hang`
--
ALTER TABLE `don_dat_hang`
 ADD PRIMARY KEY (`DDH_Ma`), ADD KEY `KH_Ma` (`KH_Ma`);

--
-- Indexes for table `hoi_dap`
--
ALTER TABLE `hoi_dap`
 ADD PRIMARY KEY (`HD_Ma`);

--
-- Indexes for table `khach_hang`
--
ALTER TABLE `khach_hang`
 ADD PRIMARY KEY (`KH_Ma`), ADD UNIQUE KEY `KH_Email` (`KH_Email`), ADD UNIQUE KEY `KH_Email_2` (`KH_Email`);

--
-- Indexes for table `loai_san_pham`
--
ALTER TABLE `loai_san_pham`
 ADD PRIMARY KEY (`LSP_Ma`);

--
-- Indexes for table `nha_san_xuat`
--
ALTER TABLE `nha_san_xuat`
 ADD PRIMARY KEY (`NSX_Ma`);

--
-- Indexes for table `phan_quyen`
--
ALTER TABLE `phan_quyen`
 ADD PRIMARY KEY (`PQ_Ma`);

--
-- Indexes for table `phu_kien`
--
ALTER TABLE `phu_kien`
 ADD PRIMARY KEY (`SP_Ma`);

--
-- Indexes for table `quang_cao`
--
ALTER TABLE `quang_cao`
 ADD PRIMARY KEY (`QC_Ma`);

--
-- Indexes for table `san_pham`
--
ALTER TABLE `san_pham`
 ADD PRIMARY KEY (`SP_Ma`), ADD KEY `LSP_Ma` (`LSP_Ma`), ADD KEY `NSX_Ma` (`NSX_Ma`);

--
-- Indexes for table `sp_laptop`
--
ALTER TABLE `sp_laptop`
 ADD PRIMARY KEY (`SP_Ma`);

--
-- Indexes for table `tin_tuc`
--
ALTER TABLE `tin_tuc`
 ADD PRIMARY KEY (`TT_Ma`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `AD_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `anh`
--
ALTER TABLE `anh`
MODIFY `Anh_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `bao_hanh`
--
ALTER TABLE `bao_hanh`
MODIFY `BH_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ct_don_dat_hang`
--
ALTER TABLE `ct_don_dat_hang`
MODIFY `ID_CTDDH` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `don_dat_hang`
--
ALTER TABLE `don_dat_hang`
MODIFY `DDH_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `hoi_dap`
--
ALTER TABLE `hoi_dap`
MODIFY `HD_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `khach_hang`
--
ALTER TABLE `khach_hang`
MODIFY `KH_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `loai_san_pham`
--
ALTER TABLE `loai_san_pham`
MODIFY `LSP_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `nha_san_xuat`
--
ALTER TABLE `nha_san_xuat`
MODIFY `NSX_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `phan_quyen`
--
ALTER TABLE `phan_quyen`
MODIFY `PQ_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `quang_cao`
--
ALTER TABLE `quang_cao`
MODIFY `QC_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `san_pham`
--
ALTER TABLE `san_pham`
MODIFY `SP_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `tin_tuc`
--
ALTER TABLE `tin_tuc`
MODIFY `TT_Ma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
ADD CONSTRAINT `admin_` FOREIGN KEY (`PQ_Ma`) REFERENCES `phan_quyen` (`PQ_Ma`);

--
-- Constraints for table `anh`
--
ALTER TABLE `anh`
ADD CONSTRAINT `anh_ibfk_1` FOREIGN KEY (`SP_Ma`) REFERENCES `san_pham` (`SP_Ma`);

--
-- Constraints for table `bao_hanh`
--
ALTER TABLE `bao_hanh`
ADD CONSTRAINT `constraintbh` FOREIGN KEY (`DDH_Ma`) REFERENCES `don_dat_hang` (`DDH_Ma`);

--
-- Constraints for table `ct_don_dat_hang`
--
ALTER TABLE `ct_don_dat_hang`
ADD CONSTRAINT `ct_don_dat_hang_ibfk_1` FOREIGN KEY (`DDH_Ma`) REFERENCES `don_dat_hang` (`DDH_Ma`);

--
-- Constraints for table `don_dat_hang`
--
ALTER TABLE `don_dat_hang`
ADD CONSTRAINT `don_dat_hang_ibfk_1` FOREIGN KEY (`KH_Ma`) REFERENCES `khach_hang` (`KH_Ma`);

--
-- Constraints for table `phu_kien`
--
ALTER TABLE `phu_kien`
ADD CONSTRAINT `phu_kien` FOREIGN KEY (`SP_Ma`) REFERENCES `san_pham` (`SP_Ma`);

--
-- Constraints for table `san_pham`
--
ALTER TABLE `san_pham`
ADD CONSTRAINT `san_pham_ibfk_1` FOREIGN KEY (`LSP_Ma`) REFERENCES `loai_san_pham` (`LSP_Ma`),
ADD CONSTRAINT `san_pham_ibfk_2` FOREIGN KEY (`NSX_Ma`) REFERENCES `nha_san_xuat` (`NSX_Ma`);

--
-- Constraints for table `sp_laptop`
--
ALTER TABLE `sp_laptop`
ADD CONSTRAINT `sp_laptop` FOREIGN KEY (`SP_Ma`) REFERENCES `san_pham` (`SP_Ma`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
