<?php
session_start();

$ok=1;

if(isset($_SESSION['cart']))
{
  foreach($_SESSION['cart'] as $k=>$v)
  {
    if(isset($v))
    {
      $ok=2;
    }
  }
}

if ($ok != 2)
{
  $mess = 'Bạn không có sản phẩm nào trong giỏ hàng !';
} else {
  $items = $_SESSION['cart']; // ???
  $mess  = '';
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>iLap</title>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- font -->
  <link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.min.css">

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- font -->

  <!-- bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- bootstrap -->

  <!-- tu viet -->
  <script src="js/js3.js"></script>
  <link rel="stylesheet" type="text/css" href="CSS/stlcss.css?v=8">
  <link rel="stylesheet" type="text/css" href="CSS/ca-pr.css">
  <!-- tu viet -->

</head>
<body>

<?php

if (isset($_GET['function']))
  $function = $_GET['function'];
else
  $function = 'getlist';

// include

if($function != 'login')
  include('includes/banner.php');

include('Controller/Controller.php');

switch ($function) {
  case 'login' : {
    $rsuser = new Controller();
    $rsuser->login();
    break;
  }

  // trang chủ
  case 'getlist' : {
    $getlist = new Controller();
    $getlist->getlist();
    break;
  }

  case 'nhanmail' : {
    $mail = new Controller();
    $mail->nhanmail();
    break;
  }

  case 'account':{
    $user = $_SESSION['user'];

    $acc = new Controller();
    $acc->account($user);
    break;
  }

  case 'update':{
    $user = $_SESSION['user'];

    $acc = new Controller();
    $acc->update($user);
    break;
  }

  case 'ctdh':{
    $dh_ma = $_GET['dh_ma'];
    $user = $_SESSION['user'];

    $acc = new Controller();
    $acc->ctdh($dh_ma);
    break;
  }

  case 'login':{
    include('includes/login.php');
    break;
  }

  case 'phanhoi':{
    $phanhoi = new Controller();
    $phanhoi->phanhoi();
    echo "<script>alert('Cảm ơn đã phản hồi !')</script>";
    exit();
    break;
  }

  case 'cauhoi':{
    include('includes/cauhoi.php');
    break;
  }

  case 'giohang':{
    include('includes/giohang.php');
    break;
  }

  case 'chinhsach': {
    include('includes/chinhsach.php');
    break;
  }

  // trang list sp, tin tức chung
  case 'listlap':{
    $listlap = new Controller();
    $listlap->listlap();
    break;
  }

  case 'search': {
    $rssearch = new Controller();
    $rssearch->search();

    $loai = $_POST['loai'];

    if ($loai == 'laptop')
      $link = 'index.php?function=lapdetail';
    else if ($loai == 'mouse')
      $link = 'index.php?function=mousetail';

    break;
  }

  case 'listmouse':{
    $listmouse = new Controller();
    $listmouse->listmouse();
    break;
  }

  case 'tintuc':{
    $listnews = new Controller();
    $listnews->listnews();
    break;
  }

  // trang chi tiết
  case 'lapdetail':{
    $sp_ma = $_GET['sp_ma'];

    $lapdetail = new Controller();
    $lapdetail->lapdetail($sp_ma);
    break;
  }

  case 'mousedetail': {
    $sp_ma = $_GET['sp_ma'];

    $mousedetail = new Controller();
    $mousedetail->mousedetail($sp_ma);
    break;
  }

  case 'tintuc-chitiet':{
    $newsdetail = new Controller();

    $tt_ma = $_GET['tt_ma'];
    $newsdetail->newsdetail($tt_ma);
    break;
  }


  // thao tác 
  case 'muangay':{
    include('includes/order.php');
    break;
  }

  default: {
    $function = 'getlist';
    $getlist = new Controller();
    $getlist->getlist();
    break;
  }
  //end dafault
}

?>
  <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Lên đầu trang" data-toggle="tooltip" data-placement="left" style="background-color: #283849;border-color: #283849;padding-top:13px ;padding-bottom:13px ;border-radius: 50%"><span class="glyphicon glyphicon-chevron-up"></span></a>

<?php
  if($function != 'login')
    include('includes/footer.php');
?>
</body>
</html>

