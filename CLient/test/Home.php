<!DOCTYPE html>
<html>
<head>
  <title>Home</title>
  <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
  <style type="text/css" media="screen">
    body {
      background: #21303f;
    }
     
    .elements {
      margin-bottom: 100px;
    }
     
    h1 {
      color: #FFF;
      text-align: center;
      font: 900 16px Lato;
      text-transform: uppercase;
      letter-spacing: 15px;
      margin: 80px 0px 20px 0px;
    }
     
    h2 {
      color: #404b5b;
      text-align: center;
      font: 900 11px Lato;
      text-transform: uppercase;
      letter-spacing: 15px;
      margin: 0px 0px 70px 0px;
    }
     
    ul {
      width: 610px;
      margin: 0 auto;
      vertical-align: center;
      padding: 0px;
    }
    ul:after {
      content: "";
      display: table;
      clear: both;
    }
    ul li {
      list-style: none;
      float: left;
      margin: 0px 15px 70px 0px;
    }
    ul a {
      color: black;
      padding: 25px;
      border: 2px solid black;
      border-radius: 50px;
    }
    ul .fa-twitter {
      padding: 0px 2px 3px 2px;
    }
    ul .fa-facebook {
      padding: 0px 4px 3px 3px;
    }
    ul .fa-dribbble {
      padding: 0px 2px 3px 2px;
    }
    ul .fa-linkedin {
      padding: 0px 2px 3px 2px;
    }
    ul .fa-flickr {
      padding: 0px 2px 3px 2px;
    }
     
    .blue a {
      color: #22A7F0;
      padding: 25px;
      border: 2px solid #22A7F0;
      border-radius: 50px;
    }
     
    a:hover {
      color: #d2534d !important;
      border-color: #d2534d !important;
      -webkit-transition: .8s ease;
      -moz-transition: .8s ease;
      -ms-transition: .8s ease;
      -o-transition: .8s ease;
      transition: .8s ease;
    }
     
    a:hover i {
      -webkit-transform: scale(1.2);
      -moz-transform: scale(1.2);
      -o-transform: scale(1.2);
      -ms-transform: scale(1.2);
      transform: scale(1.2);
      color: #d2534d;
      -webkit-transition: .3s ease;
      -moz-transition: .3s ease;
      -ms-transition: .3s ease;
      -o-transition: .3s ease;
      transition: .3s ease;
    }
     
    .green a {
      color: #03C9A9;
      padding: 25px;
      border: 2px solid #03C9A9;
      border-radius: 50px;
    }
     
    .orange a {
      color: #F2784B;
      padding: 25px;
      border: 2px solid #F2784B;
      border-radius: 50px;
    }
     
    .pink a {
      color: #D2527F;
      padding: 25px;
      border: 2px solid #D2527F;
      border-radius: 50px;
    }
  </style>
</head>
<body>

<div class="elements" style="margin-top: 100px">
<ul>
  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
  <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
  <li><a href="#"><i class="fa fa-behance"></i></a></li>
  <li><a href="#"><i class="fa fa-flickr"></i></a></li>
  <li><a href="#"><i class="fa fa-codepen"></i></a></li>
  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
</ul>
 
<ul class="blue">
  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
  <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
  <li><a href="#"><i class="fa fa-behance"></i></a></li>
  <li><a href="#"><i class="fa fa-flickr"></i></a></li>
  <li><a href="#"><i class="fa fa-codepen"></i></a></li>
  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
</ul>
 
<ul class="green">
  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
  <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
  <li><a href="#"><i class="fa fa-behance"></i></a></li>
  <li><a href="#"><i class="fa fa-flickr"></i></a></li>
  <li><a href="#"><i class="fa fa-codepen"></i></a></li>
  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
</ul>
 
<ul class="orange">
  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
  <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
  <li><a href="#"><i class="fa fa-behance"></i></a></li>
  <li><a href="#"><i class="fa fa-flickr"></i></a></li>
  <li><a href="#"><i class="fa fa-codepen"></i></a></li>
  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
</ul>
 
<ul class="pink">
  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
  <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
  <li><a href="#"><i class="fa fa-behance"></i></a></li>
  <li><a href="#"><i class="fa fa-flickr"></i></a></li>
  <li><a href="#"><i class="fa fa-codepen"></i></a></li>
  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
</ul>
</div>

</body>
</html>

