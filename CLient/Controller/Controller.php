<?php
include('Model/Model.php');

class Controller
{
	var $model;
	
	public function __construct()
	{
		$this->model = new Model();
	}

	public function login() {
		$this->model->login();
		include('includes/login.php');
	}

	// trang chủ
	public function getlist() {
		$this->getnews();
		$this->getlap();
		$this->getmouse();
	}

	// sản phẩm ở trang chủ
	public function getnews() {
		$data = $this->model->getnews();
		include('includes/content-1.php');
	}

	public function regis() {
		$data = $this->model->regis();
	}

	public function account($user) {
		$acc = $this->model->account($user);
		include('includes/account.php');
	}
	public function ctdh($dh_ma) {
		$acc = $this->model->ctdh($dh_ma);
		include('includes/donhang-chitiet.php');
	}

	public function update($user) {
		$acc = $this->model->update($user);
		$this->account($user);
	}

	public function search() {
		$loai = $_POST['loai'];
		$_SESSION['search'] = 'search';

		if ($loai == 'laptop') {
			$listlap = $this->model->search();
			include('includes/listlaptop.php');
		} else if ($loai == 'mouse') {
			$listmouse = $this->model->search();
			include('includes/listmouse.php');
		}

	}

	public function getlap() {
		$rslap = $this->model->getlap();
		include('includes/content-2-lap.php');
	}

	public function getmouse() {
		$rsmouse = $this->model->getmouse();
		include('includes/content-2-mouse.php');
	}

	// trang list
	public function listlap() {
		$listlap = $this->model->getlap();
		include('includes/listlaptop.php');
	}

	public function listmouse() {
		$listmouse = $this->model->getmouse();
		include('includes/listmouse.php');
	}
	
	public function listnews() {
		$listnews = $this->model->listnews();
		include('includes/tintuc.php');
	}

		
	// end

	// chi tiết
	public function lapdetail($sp_ma) {
		$data = $this->model->lapdetail($sp_ma);
		include('includes/laptop.php');
	}

	public function mousedetail($sp_ma) {
		$data = $this->model->mousedetail($sp_ma);
		include('includes/mouse.php');
	}

	public function newsdetail($tt_ma) {
		$rs = $this->model->newsdetail($tt_ma);
		include('includes/tintuc-chitiet.php');
	}
	// end

	// đặt hàng
	public function order($sp_ma) {
		$recordset = $this->model->orderlap();
		include('includes/orderlap.php');
	}

	public function phanhoi() {
		$this->model->phanhoi();
		$this->getlist();
	}
}

?> 