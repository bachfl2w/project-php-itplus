<?php

class Model
{
	var $con ;

	public function __construct()
	{
		$this->con = new PDO(
			'mysql:host=localhost;dbname=shoplaptop',
			'root' , // user
			'', 	 // pass
			array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
		);	
	}

	public function login(){
		$user = '';
		$pass = '';

		if (isset($_POST['txtuser']) && isset($_POST['txtpass'])) {
			$user = $_POST['txtuser'];
			$pass = $_POST['txtpass'];

			$sql = "
				select kh_taikhoan, kh_matkhau
				from khach_hang
				where kh_taikhoan = '".$user."' and kh_matkhau = '".$pass."'
			";

			$sta = $this->con->prepare($sql);
			$sta->execute();
			
			$num = $sta->rowCount();
			// echo $num;
			if ($num >= 1) {
				$sta->fetchAll(PDO::FETCH_OBJ);

				$_SESSION['user'] = $user;

				echo '<script type="text/javascript"> window.open("index.php","_self");</script>';
			} else {
				echo '<script type="text/javascript"> window.open("login.php?error","_self");</script>';
			}
		}
	}

	public function search() {
		$search	= $_POST['txtsearch'];
		$loai 	= $_POST['loai'];
		$hang 	= $_POST['hang'];

		if ($loai == 'laptop'){
			$sql = "
				select * from san_pham a

				inner join sp_laptop b
				on a.sp_ma = b.sp_ma

				inner join anh c
				on c.sp_ma = a.sp_ma

				inner join nha_san_xuat d
				on d.nsx_ma = a.nsx_ma

				inner join san_pham e
				on e.sp_ma = a.sp_ma

				inner join loai_san_pham f
				on f.lsp_ma = e.lsp_ma

				where a.sp_ten like '%{$search}%' and d.nsx_ten = '{$hang}' and f.lsp_ten = '{$loai}'
			";
			$sta = $this->con->prepare($sql);
			// $sta->bindParam(":search" , $search, PDO::PARAM_STR);
			// $sta->bindParam(":lsp_ten", $loai  , PDO::PARAM_STR);
			// $sta->bindParam(":seach"  , $search, PDO::PARAM_STR);
			$sta->execute();

			$data['laptop'] = $sta->fetchAll(PDO::FETCH_OBJ);
		}
		elseif ($loai == 'mouse'){
			$sql1 = "
				select * from san_pham a

				inner join phu_kien b
				on a.sp_ma = b.sp_ma

				inner join anh c
				on c.sp_ma = a.sp_ma

				inner join nha_san_xuat d
				on d.nsx_ma = a.nsx_ma

				inner join san_pham e
				on e.sp_ma = a.sp_ma

				inner join loai_san_pham f
				on f.lsp_ma = e.lsp_ma

				where a.sp_ten like '%".$search."%' and d.nsx_ten = '".$hang."' and f.lsp_ten = '".$loai."'
			";
			$sta = $this->con->prepare($sql1);
			// $sta->bindParam(":search",  $search, PDO::PARAM_STR);
			// $sta->bindParam(":lsp_ten", $loai,   PDO::PARAM_STR);
			$sta->execute();

			$data['mouse'] = $sta->fetchAll(PDO::FETCH_OBJ);
		}
		
		return $data;
	}

		public function ctdh($dh_ma) {
			$acc = array();

			$sql1 = "
				select *
				from ct_don_dat_hang a
				
				inner join don_dat_hang b
				on a.ddh_ma = b.ddh_ma

				inner join khach_hang c
				on c.KH_ma = b.kh_ma

				inner join san_pham d
				on d.sp_ma = a.sp_ma

				where b.DDH_Ma = :dh_ma
			";
			$sta = $this->con->prepare($sql1);
			$sta->bindParam(":dh_ma", $dh_ma, PDO::PARAM_STR);
			$sta->execute();
			$acc['ctdh'] = $sta->fetchAll(PDO::FETCH_OBJ);

			return $acc;
		}

		public function account($user) {
			$acc = array();

			$sql1 = "
				select *, SUM(d.SP_GiaNiemYet * c.So_Luong) as TongTien
				from don_dat_hang a

				inner join khach_hang b
				on b.KH_Ma = a.KH_Ma

				inner join ct_don_dat_hang c
				on a.DDH_Ma = c.DDH_Ma

				inner join san_pham d
				on d.SP_Ma = c.SP_Ma

				where kh_taikhoan = :user
				group by a.DDH_Ma desc
			";
			$sta = $this->con->prepare($sql1);
			$sta->bindParam(":user", $user, PDO::PARAM_STR);
			$sta->execute();
			$acc['donhang'] = $sta->fetchAll(PDO::FETCH_OBJ);

			$sql2 = "
				select *
				from khach_hang
				where kh_taikhoan = :user
				limit 1
			";
			$sta = $this->con->prepare($sql2);
			$sta->bindParam(":user", $user, PDO::PARAM_STR);
			$sta->execute();
			$acc['taikhoan'] = $sta->fetchAll(PDO::FETCH_OBJ);

			return $acc;
		}

		public function update($user) {
			$acc = array();

			$hoten 	= $_POST['hoten'];
			$sdt 	= $_POST['sdt'];
			$diachi = $_POST['diachi'];
			$email 	= $_POST['email'];

			if (isset($_POST['matkhaumoi'])) {
				$matkhau = $_POST['matkhaumoi'];
				$sql1 = "
					update khach_hang
					set kh_hoten   = :hoten,
						kh_sdt     = :sdt,
						kh_diachi  = :diachi,
						kh_email   = :email,
						kh_matkhau = :matkhau
					where kh_taikhoan = :user
				";

				$sta = $this->con->prepare($sql1);
				$sta->bindParam(":matkhau", $matkhau, PDO::PARAM_STR);
			} else {
				$sql1 = "
					update khach_hang set 
					kh_hoten = :hoten,
					kh_sdt = :sdt,
					kh_diachi = :diachi,
					kh_email = :email
					where kh_taikhoan = :user
				";
				$sta = $this->con->prepare($sql1);
			}
			$sta->bindParam(":user",	$user, 	  PDO::PARAM_STR);
			$sta->bindParam(":hoten",	$hoten,   PDO::PARAM_STR);
			$sta->bindParam(":sdt", 	$sdt, 	  PDO::PARAM_INT);
			$sta->bindParam(":diachi", 	$diachi,  PDO::PARAM_STR);
			$sta->bindParam(":email", 	$email,   PDO::PARAM_STR);

			$sta->execute();
			$acc['donhang'] = $sta->fetchAll(PDO::FETCH_OBJ);

			return $acc;
		}

		public function getnews() {
			$data = array();
			
			// news
			$sql1 	= "
				SELECT *
				FROM tin_tuc
				limit 2
			";
			$sta 	= $this->con->prepare($sql1);
			$sta->execute();
			$data['tin_tuc'] = $sta->fetchAll(PDO::FETCH_OBJ);

			// advertisment type 1
			$sql2 	= '
				SELECT *
				FROM quang_cao
				where QC_Loai = 1
				limit 4
			';
			$sta 	= $this->con->prepare($sql2);
			$sta->execute();
			$data['quang_cao1'] = $sta->fetchAll(PDO::FETCH_OBJ);

			// advertisment type 1
			$sql3 	= '
				SELECT *
				FROM quang_cao
				where QC_Loai = 2 
				imit 2
			';
			$sta 	= $this->con->prepare($sql3);
			$sta->execute();
			$data['quang_cao2'] = $sta->fetchAll(PDO::FETCH_OBJ);

			return $data;
		}

		public function getlap() {
			$array = array();

			$sql = "
				select *
				from sp_laptop a

				inner join san_pham b
				on a.sp_ma = b.sp_ma

				inner join anh c
				on c.sp_ma = b.sp_ma

				inner join ct_don_dat_hang d
				on d.sp_ma = c.sp_ma

				order by d.sp_ma desc
			";

			$sql1 = "
				select * 
				from sp_laptop a

				inner join san_pham b	
				on a.sp_ma = b.sp_ma

				inner join anh c
				on c.SP_Ma = b.SP_Ma

				ORDER BY RAND()
			";

			$sta 	= $this->con->prepare($sql);
			$sta1 	= $this->con->prepare($sql1);

			$sta->execute();
			$sta1->execute();

			$data['laptop_hot'] = $sta->fetchAll(PDO::FETCH_OBJ);
			$data['laptop'] 	= $sta1->fetchAll(PDO::FETCH_OBJ);
			
			return $data;
		}

		public function getmouse() {
			$sql = "
				select * 
				from phu_kien a 
				inner join san_pham b 
				on a.sp_ma = b.sp_ma
				inner join anh c
				on c.SP_Ma = b.SP_Ma
				ORDER BY RAND()
			";

			$sta = $this->con->prepare($sql);
			$sta->execute();

			$rsmouse['mouse'] = $sta->fetchAll(PDO::FETCH_OBJ);
			return $rsmouse;
		}

	// end

	// lấy ra những sản phẩm cùng hãng
	public function getlapsame($sp_ma) {
		$sql = "
			select * 
			from sp_laptop a
			inner join san_pham b	
			on a.sp_ma = b.sp_ma
			inner join anh c
			on c.SP_Ma = b.SP_Ma
			where b.NSX_Ma = (select NSX_Ma from san_pham where SP_Ma = :sp_ma)
		";

		$sta = $this->con->prepare($sql);
		$sta->bindParam(":sp_ma", $sp_ma, PDO::PARAM_INT);
		$sta->execute();

		$rslap = $sta->fetchAll(PDO::FETCH_OBJ);
		return $rslap;
	}

	// end get list

	public function listnews() {
		$sql = "select * from tin_tuc";

		$sta = $this->con->prepare($sql);
		$sta->execute();

		$listnews = $sta->fetchAll(PDO::FETCH_OBJ);
		return $listnews;
	}

	public function news() {
		$sql = "select * from tin_tuc";

		$sta = $this->con->prepare($sql);
		$sta->execute();

		$rsnews = $sta->fetchAll(PDO::FETCH_OBJ);
		return $rsnews;
	}

	public function phanhoi() {
		$email 	= $_POST['email'];
		$tieude = $_POST['tieude'];
		$yk 	= $_POST['yk'];
		$query	= ("
			insert into hoi_dap(hd_tieude, email, hd_noidung, HD_CauTraLoi, HD_TrangThai, HD_NgayPhanHoi) values
			(:tieude, :email, :yk, '', 1 ,'')
		");
		$sta = $this->con->prepare($query);
		$sta->bindParam(":tieude", $tieude, PDO::PARAM_STR);
		$sta->bindParam(":email", $email, PDO::PARAM_STR);
		$sta->bindParam(":yk", $yk, PDO::PARAM_STR);
		$sta->execute();
	}

	/*chi tiết*/
		public function lapdetail($sp_ma) {
			$data = array();

			$sql1 = "
				select * 
				from sp_laptop a
				inner join san_pham b
				on a.SP_Ma = b.SP_Ma
				inner join anh c
				on c.SP_Ma = b.SP_Ma
				where a.SP_Ma = :sp_ma
			";
			$sta = $this->con->prepare($sql1);
			$sta->bindParam(":sp_ma", $sp_ma, PDO::PARAM_INT);
			$sta->execute();
			$data['lap-detail'] = $sta->fetchAll(PDO::FETCH_OBJ);

			$gia = "
				select b.SP_GiaNiemYet
				from sp_laptop a
				inner join san_pham b
				on a.SP_Ma = b.SP_Ma
				inner join anh c
				on c.SP_Ma = b.SP_Ma
				where a.SP_Ma = :sp_ma
			";
			$gianho = ($gia - 1500000);
			$gialon = ($gia + 1500000);

			$sql2 = "
				select * 
				from sp_laptop a
				inner join san_pham b
				on a.SP_Ma = b.SP_Ma
				inner join anh c
				on c.SP_Ma = b.SP_Ma
				where b.SP_GiaNiemYet = (($gia) > $gianho and ($gia) < $gialon)
			";
			$sta = $this->con->prepare($sql2);
			$sta->bindParam(":sp_ma", $sp_ma, PDO::PARAM_INT);
			$sta->execute();
			$data['lap-same'] = $sta->fetchAll(PDO::FETCH_OBJ);

			return $data;
		}

		public function mousedetail($sp_ma) {
			$sql = "
				select * 
				from phu_kien a
				inner join san_pham b
				on a.SP_Ma = b.SP_Ma
				inner join anh c
				on c.SP_Ma = b.SP_Ma
				inner join nha_san_xuat d
				on d.NSX_Ma = b.NSX_Ma
				where a.SP_Ma = :sp_ma
			";

			$sta = $this->con->prepare($sql);
			$sta->bindParam(":sp_ma", $sp_ma, PDO::PARAM_INT);
			$sta->execute();
			$data['mouse-detail'] = $sta->fetchAll(PDO::FETCH_OBJ);

			return $data;
		}

		public function newsdetail($tt_ma) {
			$sql = "select * from tin_tuc where TT_Ma = :tt_ma";

			$sta = $this->con->prepare($sql);

			$sta->bindParam(":tt_ma", $tt_ma, PDO::PARAM_INT);
			$sta->execute();

			$rs = $sta->fetchAll(PDO::FETCH_OBJ);
			return $rs;
		}
	// end

	// xử lý đơn hàng
		// public function cart($sp_ma) {
			
		// }

		public function order($sp_ma) {
			$sql = "
				select * 
				from san_pham a
				inner join ct_don_dat_hang b
				on a.sp_ma = b.sp_ma
				where a.sp_ma = :sp_ma
			";

			$sta = $this->con->prepare($sql);

			$sta->bindParam(":sp_ma", $sp_ma, PDO::PARAM_INT);
			$sta->execute();

			$rs = $sta->fetchAll(PDO::FETCH_OBJ);
			return $rs;
		}

		// get producer
		public function getNSX(){
			$sql = "select NSX_Ten from nha_san_xuat";

			$sta = $this->con->prepare($sql);

			$sta->execute();
			
		}

}
?>