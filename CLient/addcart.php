<?php
session_start();

$id       = $_GET['item']; // get id of item from url
$function = '';

if (isset($_GET['f'])) {
	$function = $_GET['f'];

	if ($function == 'add') {
		// if session['cart'] isset id of $_GET['item'] => quantity + 1
		// else quantity = 1
		if(isset($_SESSION['cart'][$id]))
		{
			$qty = $_SESSION['cart'][$id] + 1;
		}
		else
		{
			$qty = 1;
		}
	}
	elseif ($function == 'minus') {
		if(isset($_SESSION['cart'][$id]))
		{
			$qty = $_SESSION['cart'][$id] - 1;
			if($qty < 1)
		 		$qty += 1;
		}
		else
		{
			$qty = 1;
		}
	}
	elseif ($function == 'xoa') {
		$cart=$_SESSION['cart'];
		$id=$_GET['sp_ma'];
		if($id == 0)
		{
			unset($_SESSION['cart']);
		}
		else
		{
			unset($_SESSION['cart'][$id]);
		}
	}
}

$_SESSION['cart'][$id] = $qty;

header("location: index.php?function=giohang");

exit();

?>