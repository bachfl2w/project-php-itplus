<div class="filter-lr content-2">
  <div class="panel panel-default">
    <!-- panel heading -->
    <div class="panel-heading">
      <b>SẢN PHẨM BÁN CHẠY</b>
    </div>
    <!-- end panel heading -->

    <!-- panel body -->
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <div class="carousel carousel-showmanymoveone slide" id="carousel-tilenav" data-interval="false">
          <div class="carousel-inner">

            <?php $r=0 ;foreach ($rslap['laptop_hot'] as $l) { ?>
              <?php
                if($r == 0) echo'<div class="item active" >';
                else        echo'<div class="item">'; 
              ?>
                <div <?php if($l->SP_KhuyenMai > 0) echo 'class ="col-xs-12 col-sm-6 col-md-2 offer offer-warning"'; else echo 'class ="col-xs-12 col-sm-6 col-md-2 offer offer-none"' ;?> >
                  <a href="index.php?function=lapdetail&sp_ma=<?php echo $l->SP_Ma;?>" >
                    <div class="shape">
                      <div class="shape-text">
                        -<?=$l->SP_KhuyenMai?>%               
                      </div>
                    </div>
                    <img src="image/<?php echo $l->Anh_Ten ;?>" class="img-responsive">
                  </a>
                  <div style="height: 40px">
                    <p style="font-weight: 600"><?php echo $l->SP_Ten;?></p><br>
                  </div>
                  <div style="height: 75px">
                    <p>
                      <?php
                        $GiaGoc = $l->SP_GiaNiemYet;
                        $KhuyenMai = $l->SP_KhuyenMai;

                        if ($KhuyenMai > 0){
                          $Gia = $GiaGoc - ($GiaGoc * $KhuyenMai / 100);
                          echo '<b>Giá: '.number_format($Gia, 0, '', '.').'đ</b><br>';
                          echo '<small style="text-decoration: line-through;">'.number_format($GiaGoc, 0, '', '.').'đ</small>';
                        }
                        else 
                          echo '<b>Giá: '.number_format($GiaGoc, 0, '', '.').'đ</b><br>';
                      ?>
                    </p>
                  </div>

                </div>
              </div>
            <?php $r++; } ?>

          </div>

          <a class="left carousel-control" href="#carousel-tilenav" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
          <a class="right carousel-control" href="#carousel-tilenav" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>

        </div>
      </div>
    </div>
  </div>
  <!-- end panel body -->
  </div>
  <!-- end panel default -->
</div>

<div class="filter-lr thuonghieu">
  <div class="panel panel-default">
    <div class="panel-body">
      <p align="center" style="font-size: 25px">THƯƠNG HIỆU LAPTOP</p>
      <div class="row">

        <div class="col-sm-3">
          <img src="image/logo/dell-logo.png">
        </div>

        <div class="col-sm-3">
          <img src="image/logo/hp.png">
        </div>

        <div class="col-sm-3 th-test">
          <img src="image/logo/asus.png">
        </div>

        <div class="col-sm-3">
          <img src="image/logo/apple.png">
        </div>

      </div>
    </div>
  </div>
</div>

<div class="filter-lr content-2">
  <div class="panel panel-default">
    <!-- panel heading -->
    <div class="panel-heading">
      <b >LAPTOP ĐƯỢC QUAN TÂM</b>
    </div>
    <!-- end panel heading -->

    <!-- panel body -->
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <div class="carousel carousel-showmanymoveone slide" id="carousel-tilenav-1" data-interval="false">
          <div class="carousel-inner">
            <?php $r=0 ;foreach ($rslap['laptop'] as $l) { ?>
              <?php
                if($r == 0) echo'<div class="item active" >';
                else        echo'<div class="item">'; 
              ?>
                <div <?php if($l->SP_KhuyenMai > 0) echo 'class ="col-xs-12 col-sm-6 col-md-2 offer offer-warning"'; else echo 'class ="col-xs-12 col-sm-6 col-md-2 offer offer-none"' ;?> >
                  <a href="index.php?function=lapdetail&sp_ma=<?= $l->SP_Ma;?>" >
                    <div class="shape">
                      <div class="shape-text">
                        -<?=$l->SP_KhuyenMai?>%               
                      </div>
                    </div>
                    <img src="image/<?php echo $l->Anh_Ten ;?>" class="img-responsive">
                  </a>
                  <div style="height: 40px">
                    <p style="font-weight: 600"><?php echo $l->SP_Ten;?></p><br>
                  </div>
                  <div style="height: 75px">
                    <p>
                      <?php
                        $GiaGoc = $l->SP_GiaNiemYet;
                        $KhuyenMai = $l->SP_KhuyenMai;

                        if ($KhuyenMai > 0){
                          $Gia = $GiaGoc - ($GiaGoc * $KhuyenMai / 100);
                          echo '<b>Giá: '.number_format($Gia, 0, '', '.').'đ</b><br>';
                          echo '<small style="text-decoration: line-through;">'.number_format($GiaGoc, 0, '', '.').'đ</small>';
                        }
                        else 
                          echo '<b>Giá: '.number_format($GiaGoc, 0, '', '.').'đ</b><br>';
                      ?>
                    </p>
                  </div>
                </div>
              </div>
            <?php $r++; } ?>
          </div>

          <a class="left carousel-control" href="#carousel-tilenav-1" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
          <a class="right carousel-control" href="#carousel-tilenav-1" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>

        </div>
      </div>
    </div>
  </div>
  <!-- end panel body -->
  </div>
  <!-- end panel default -->
</div>

<!-- <script  src="js/carousel2.js"></script> -->