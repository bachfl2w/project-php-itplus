<?php
	$user = $_SESSION['user'];
?>
<style type="text/css" media="screen">
  .col-sm-12 li {line-height: 25px}
  .col-sm-9 {border-left: 2px solid #f0f3f4;font-size: 13px}
  .col-sm-9 .col-sm-6 ul {margin-left: -55px;list-style-type: none}

  .col-sm-9 .row {border-bottom: 2px solid #f0f3f4;}

  .listlaptop a:hover {color:black;}

  .row .panel-heading{border-bottom: 2px solid #f0f3f4}

	.listlaptop li {list-style-type: none;margin-left: -20px;padding-top: 20px}

  .form-group input , .form-group select {border-radius: 0}

  .control-label .col-sm-4 {text-align: left}
</style>

<div class="filter-lr listlaptop">
  <div class="panel panel-default">

    <div class="panel-heading row" style="padding-bottom: 30px;background-color: #f0f3f4">
      <a href="index.php?function=getlist" style="color: gray">Trang chủ</a>&nbsp; /&nbsp; <a href="#" style="color: #d2534d">Chi tiết đơn hàng</a>
    </div>

    <div class="panel-body">

      <div class="col-sm-12">
				<div class="tab-content">
					<!-- 1 -->
					<div id="donhang" class="tab-pane fade in active">
						<h2>Đơn hàng của tôi</h2>
						<table class="table table-hover">
					    <thead>
					      <tr>
					        <th>Mã đơn</th>
					        <th>Ngày mua</th>
					        <th>Ngày nhận</th>
					        <th>Địa chỉ</th>
					        <th>Sản phẩm</th>
					        <th>Đơn giá</th>
					        <th>Số lượng</th>
					      </tr>
					    </thead>
					    <tbody>
					    	<?php $tongtien = 0; foreach ($acc['ctdh'] as $a){ ?>
						      <tr>
						        <td class="btn-link"><?=$a->DDH_Ma?></td>
						        <td><?=$a->DDH_NgayDat?></td>
						        <td><?php if(isset($a->DDH_NgayNhan)) echo $a->DDH_NgayNhan; else echo 'Chưa nhận';?></td>
						        <td><?=$a->DDH_DiaChi?></td>
						        <td><?=$a->SP_Ten?></td>
						        <td><?=number_format($a->SP_GiaNiemYet, 0, '', '.')?>đ</td>
						        <td><?=$a->So_Luong?></td>
						      </tr>
					      <?php
					      	
					       	$soluong = $a->So_Luong;
					       	$dongia = $a->SP_GiaNiemYet;
					       	$tongtien = $tongtien + $soluong * $dongia;
					       }
					      ?>
					    </tbody>
					  </table>
					</div>
					<!-- 1 -->
				</div>
      </div>
      <!-- end col 9 -->
    </div>

    <div class="panel-body" style="border-top: 2px solid #f0f3f4">
    	<h3>Tổng cộng : <?php echo number_format($tongtien,0, '', '.'); ?>đ </h3>
    	<a class="btn-link" href="index.php?function=account&user=<?=$user?>">Trở lại</a>
    </div>

  </div>
</div>
