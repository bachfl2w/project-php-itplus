<div class="filter-lr sp-chitiet">
  <div class="panel panel-default">

    <!-- panel body -->
    <div class="panel-body">
      <div class="content-2-heading">
        <h4>Gửi phản hồi</h4>
      </div>
      <div class="row">
        <!-- left -->
        <div class="col-sm-4">
          <b>Bạn đã sử dụng sản phẩm này ?</b>
          <br>Hãy cho chúng tôi biết cảm nhận của bạn về sản phẩm này, nếu có bất kì điều gì khiến bạn không hài lòng về sản phẩm của chúng tôi thì hãy gửi lại phản hồi để chúng tôi có thể nâng cao chất lượng dịch vụ. Cảm ơn !<br>

          <b>Sản phẩm bị lỗi ?</b>
          <br>Nếu có bất kì lỗi gì từ nhà cung cấp xảy ra, hãy gửi phản hồi cho chúng tôi để có thể khắc phục lỗi, chúng tôi sẽ tiến hành bảo hành sản phẩm nếu sản phẩm có đủ điều kiện bảo hành.
          
          <br><br>Tìm hiểu chính sách bảo hành <a href="index.php?function=chinhsach" class="btn-link">Tại đây</a>.
        </div>
        <!-- end left -->
        <!-- right -->
        <div class="col-sm-8 panel-body">
          <form action="index.php?function=phanhoi" method="post" accept-charset="utf-8">
            <div class="form-group">
              <label for="email" required>Email:</label>
              <input type="email" class="form-control" id="email" placeholder="Nhập email" name="email">
            </div>
            <div class="form-group">
              <label for="tieude">Tiêu đề:</label>
              <input type="text" class="form-control" id="tieude" placeholder="Nhập tiêu đề" name="tieude">
            </div>
            <div class="form-group">
              <label for="yk">Ý kiến:</label>
              <textarea style="max-width: 100%" type="text" class="form-control" id="yk" placeholder="Nhập phản hồi" name="yk" rows="5"></textarea>
            </div>
            <input type="submit" name="submit" class="btn btn-default" value="Gửi đánh giá">

            <div></div>

          </form>
        </div>
        <!-- end right -->
      </div>
      <!-- end row -->
    </div>
    <!-- end panel body -->
  </div>
<!-- end panel default -->

</div>