<?php foreach ($data['mouse-detail'] as $m) { ?>
  <div class="filter-lr product">
    <div class="panel panel-default">
      <!-- panel heading -->
      <div class="content-2-heading">
        <b><?=$m->SP_Ten?></b>
      </div>
      <!-- end panel heading -->

      <!-- panel body -->
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-4 mag" align="center">
            <img data-toggle="magnify" src="image/<?=$m->Anh_Ten?>" width="100%">
            <i style="color: #9f9f9f;font-size: 12px">Rê chuột để phóng to hình ảnh <span class="glyphicon glyphicon-search" style="font-size: 10px"></span></i>
          </div>
          <div class="col-sm-5">

            <div class="row">
              <div class="col-sm-12">
                <ul style="list-style-type: none;margin-left: -20px">
                  <?php
                    $GiaGoc = $m->SP_GiaNiemYet;
                    $KhuyenMai = $m->SP_KhuyenMai;

                    if ($KhuyenMai > 0){
                      $Gia = $GiaGoc - ($GiaGoc * $KhuyenMai / 100);
                      echo '<li><h3><b>'.number_format($Gia, 0, '', '.').'đ</b></h3>';
                      echo '<small style="text-decoration:line-through">' . number_format($GiaGoc,0,'','.'). 'đ</small> (-'.$KhuyenMai.'%)</li>';
                    }
                    else 
                      echo '<li><h3><b>'.number_format($GiaGoc, 0, '', '.').'đ</b></h3></li>';
                  ?>
                </ul>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <ul style="line-height:25px">
                  <li><b>Màu sắc : </b><?=$m->PK_MauSac?></li>
                  <li><b>PDI : </b><?=$m->PK_DPI?></li>
                  <li><b>Loại chuột : </b><?=$m->PK_LoaiChuot?></li>
                  <li><b>Độ bền : </b><?=number_format($m->PK_DoBen, 0, '', '.')?> lần nhấn</li>
                  <li><b>Mô tả : </b><?=$m->PK_MoTaChiTiet?></li>
                </ul>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-12" style="text-align: center">
                <a href="addcart.php?&item=<?=$m->SP_Ma?>&f=add" class="btn btn-danger btn-lg" role="button">
                  Mua ngay<br>
                  <small style="font-size: 12px">Giao tận nơi hoặc nhận ở shop</small>
                </a>
                <a href="addcart.php?&item=<?=$m->SP_Ma?>&f=add" class="btn btn-default btn-lg" role="button">
                  Thêm vào giỏ hàng<br>
                  <small style="font-size: 12px">Bạn có thể kiểm tra ở giỏ hàng</small>
                </a>
              </div>
            </div>

          </div>

          <div class="col-sm-3">
            <img src="image/carousel/QC_right.jpg" width="100%">  
          </div>

        </div>
        <!-- end row -->

      </div>
      <!-- end panel body -->

    </div>
    <!-- end panel default -->
  </div>
<?php } ?>

<!-- include 'includes/sp_cungloai.php'; -->

<div class="filter-lr sp-chitiet">
  <div class="panel panel-default">

    <!-- panel body -->
    <div class="panel-body">
      <div class="row">

        <div class="col-sm-12">
          <div class="content-2-heading">
            <h4>Thông tin số cơ bản</h4>
          </div>
          <div class="panel-body">
            <!-- 1 -->
            <b>Thiết kế</b>
            <table class="table">
              <tbody>
                <tr>
                  <td width="200px">Loại chuột :</td>
                  <td><?=$m->PK_LoaiChuot?></td>
                </tr>
                <tr>
                  <td>Màu sắc :</td>
                  <td><?=$m->PK_MauSac?></td>
                </tr>
                <tr>
                  <td>Thương hiệu :</td>
                  <td><?=$m->NSX_Ten?></td>
                </tr>
              </tbody>
            </table>
            <!-- end 1 -->
            <!-- 3 -->
            <b>Thông số</b>
            <table class="table">
              <tbody>
                <tr>
                  <td width="200px">Các mức DPI :</td>
                  <td><?=$m->PK_DPI?></td>
                </tr>
                <tr>
                  <td>Độ bền :</td>
                  <td><?=number_format($m->PK_DoBen,0,'','.')?> lần nhấn</td>
                </tr>
              </tbody>
            </table>
            <!-- end 3 -->
            <!-- 3 -->
            <b>Thông tin khác</b>
            <table class="table">
              <tbody>
                <tr>
                  <td width="200px">Mô tả : </td>
                  <td><?=$m->PK_MoTaChiTiet?></td>
                </tr>
              </tbody>
            </table>
            <!-- end 3 -->
          </div>
        </div>



      </div>
    </div>

    <!-- end panel body -->
  </div>

</div>
<!-- end panel default -->

<?php include 'includes/phanhoi-content.php'; ?>

<script  src="js/carousel2.js"></script>