<?php 
  if (isset($_SESSION['search'])) {
    unset($_SESSION['search']);
  }
?>

<style type="text/css" media="screen">
  .col-sm-12 li {line-height: 25px}
  .col-sm-9 {border-left: 2px solid #f0f3f4;font-size: 13px}
  .col-sm-9 .col-sm-6 ul {margin-left: -55px;list-style-type: none}

  .col-sm-9 .row {border-bottom: 2px solid #f0f3f4;}

  .listlaptop a:hover {color:black;}

  .row .panel-heading{border-bottom: 2px solid #f0f3f4}

  .form-group input , .form-group select {border-radius: 0}
</style>

<div class="filter-lr listlaptop">
  <div class="panel panel-default">

    <div class="panel-heading row" style="padding-bottom: 30px;background-color: #f0f3f4">
      <a href="index.php?function=getlist" style="color: gray">Trang chủ</a>&nbsp; /&nbsp; <a href="#" style="color: #d2534d">Mouse</a>
    </div>

    <div class="row">

      <div class="col-sm-3">
        <div class="panel-heading">
          <h3>Bộ lọc</h3>
        </div>
        <!-- body -->
        <div class="panel-body">
          <b style="font-size: 16px">Nhập tên chuột</b><br><br><br>
          <form action="index.php?function=search" method="post">
             <div class="form-group">
              <label for="txtsearch">Tìm theo tên :</label>
              <input type="text" class="form-control" id="txtsearch" placeholder="Nhập tên sản phẩm" name="txtsearch">
            </div>
           <div class="form-group">
              <input type="hidden" class="form-control" name="loai" id="loai" value="mouse">
            </div>
            <div class="form-group">
              <label for="hang">Hãng :</label>
              <div id="loai" name="hang" value=""></div>
              <select class="form-control" id="hang" name="hang">
                <option value="logitech">Logitech</option>
                <option value="razer">Razer</option>
                <option value="steelseris">Steelseris</option>
                <option value="apple">Apple</option>
              </select>
            </div>
            <button type="submit" class="btn btn-danger">Lọc</button>
          </form>          
        </div>
        <!-- end body -->
      </div>

      <div class="col-sm-9">
        <div class="panel-heading">
          <h3>Chuột máy tính</h3>
        </div>
        <div class="panel-body">
          <?php foreach($listmouse['mouse'] as $m) { ?>
            <a href="index.php?function=mousedetail&sp_ma=<?= $m->SP_Ma;?>"><div class="row" style="padding: 10px 0px">
              <div class="col-sm-4" align="center">
                <img src="image/<?php echo $m->Anh_Ten; ?>" width="90%">
              </div>

              <div class="col-sm-8">
                  <div class="col-sm-12">
                    <ul>
                      <b style="font-size: 20px;margin-left: -40px"><?php echo $m->SP_Ten; ?></b> <b style="color: #d2534d;float: right;font-size: 20px"><?php echo number_format($m->SP_GiaNiemYet, 0, '', '.');?>đ</b><br><br>
                      <li><b>Màu sắc : </b> <?php echo $m->PK_MauSac; ?></li>
                      <li><b>Mô tả : </b><?php echo substr($m->PK_MoTaChiTiet, 0, 350).' ...'; ?></li>
                    </ul>
                  </div>
                  <div class="col-sm-12" style="border: 2px solid #f0f3f4;border-style: dotted;background-color: #F1FBF7;font-size: 15px">
                    <div class="panel-body" style="background-color: #F1FBF7">
                      <span style="color: #9f9f9f;color: green">Đặt hàng online nhận ngay khuyến mãi :</span>
                      <ul>
                        <li>Giữ hàng tại Shop (không mua không sao)</li>
                        <li>Giao hàng miễn phí toàn quốc trong 60 phút</li>
                        <li>Tư vấn miễn phí 24/7 <span style="color: #d2534d">18006601</span> (cả dịp Lễ, Tết)</li>
                        <li>Cam kết hàng chính hãng, uy tín hàng đầu Việt Nam</li>
                      </ul>
                    </div>
                  </div>
              </div>

            </div></a>
          <?php } ?>
        </div>
      </div>
      <!-- end col 9 -->
    </div>

  </div>
</div>