<?php 
$user = $_SESSION['user']; 

if (isset($_SESSION['messenge'])) {
	echo $_SESSION['messenge'];

	unset($_SESSION['messenge']);
}
?>
<style type="text/css" media="screen">
  .col-sm-12 li {line-height: 25px}
  .col-sm-9 {border-left: 2px solid #f0f3f4;font-size: 13px}
  .col-sm-9 .col-sm-6 ul {margin-left: -55px;list-style-type: none}

  .col-sm-9 .row {border-bottom: 2px solid #f0f3f4;}

  .listlaptop a:hover {color:black;}

  .row .panel-heading{border-bottom: 2px solid #f0f3f4}

	.listlaptop li {list-style-type: none;margin-left: -20px;padding-top: 20px}

  .form-group input , .form-group select {border-radius: 0}

  .control-label .col-sm-4 {text-align: left}
</style>


<div class="filter-lr listlaptop">
  <div class="panel panel-default">

    <div class="panel-heading row" style="padding-bottom: 30px;background-color: #f0f3f4">
      <a href="index.php?function=getlist" style="color: gray">Trang chủ</a>&nbsp; /&nbsp; <a href="#" style="color: #d2534d">Tài khoản</a>
    </div>

    <div class="row">

      <div class="col-sm-3">
        <div class="panel-heading" style="border-bottom: 2px solid white;">
          <h3>Tài khoản của : </h3>
          <?php	foreach ($acc['taikhoan'] as $ac){ ?>
	          <h4>&nbsp;&nbsp;<?=$ac->KH_HoTen?></h4>
	          <!-- ul -->
	          <ul>
					    <li class="active btn-link"><a data-toggle="tab" href="#donhang">Quản lí đơn hàng</a></li>
					    <li class="btn-link"><a data-toggle="tab" href="#taikhoan">Quản lí tài khoản</a></li>
					  </ul>
					  <!-- ul -->
					<?php } ?>
        </div>
      </div>

      <div class="col-sm-9">
				<div class="tab-content">
					<!-- 1 -->
					<div id="donhang" class="tab-pane fade in active">
						<h2>Đơn hàng của tôi</h2>
						<table class="table table-hover">
					    <thead>
					      <tr>
					        <th>Mã đơn hàng</th>
					        <th>Ngày mua</th>
					        <th>Ngày nhận</th>
					        <th>Tổng tiền</th>
					        <th>Trạng thái</th>
					        <th>Chi tiết</th>
					      </tr>
					    </thead>
					    <tbody>
					    	<?php	foreach ($acc['donhang'] as $a) { ?>
					      	<?php if(isset($a->DDH_Ma)) {	?>
							      <tr>
							        <td class="btn-link"><?=$a->DDH_Ma?></td>
							        <td><?=$a->DDH_NgayDat?></td>
							        <td><?php if(isset($a->DDH_NgayNhan)) echo $a->DDH_NgayNhan; else echo 'Chưa nhận';?></td>
							        <td><?=number_format($a->TongTien, 0)?>đ</td>
							        <td>
							        	<?php
							        	if($a->DDH_TinhTrang == '2')echo'Đã nhận';
							        	elseif ($a->DDH_TinhTrang == '1')echo'Đang xử lý';
							        	?>
							        	</td>
							        <td><a class="btn-link" href="index.php?function=ctdh&dh_ma=<?=$a->DDH_Ma?>">Xem</a></td>
							      </tr>
				      	<?php }   }?>
					    </tbody>
					  </table>
					</div>
					<!-- 1 -->
					<!-- 2 -->
					<div id="taikhoan" class="tab-pane fade">
						<h2>Tài khoản của tôi</h2>
						<?php	foreach ($acc['taikhoan'] as $ac){ ?>
						  <form class="form-horizontal panel-body col-sm-8" method="post" action="index.php?function=update&user=<?=$_SESSION['user']?>">
						    <div class="form-group">
						      <label class="control-label col-sm-4" for="hoten">Họ và tên:</label>
						      <div class="col-sm-8">
						        <input type="text" class="form-control" id="hoten" placeholder="Nhập họ và tên" name="hoten" value="<?=$ac->KH_HoTen?>">
						        <input type="hidden" class="form-control" id="hoten" name="kh_ma" value="<?=$ac->KH_Ma?>">
						      </div>
						    </div>
						    <div class="form-group">
						      <label class="control-label col-sm-4" for="sdt">Số điện thoại:</label>
						      <div class="col-sm-8">
						        <input type="text" class="form-control" id="sdt" placeholder="Nhập số điện thoại" name="sdt" value="<?=$ac->KH_SDT?>">
						      </div>
						    </div>
						    <div class="form-group">
						      <label class="control-label col-sm-4" for="diachi">Địa chỉ:</label>
						      <div class="col-sm-8">
						        <input type="text" class="form-control" id="diachi" placeholder="Nhập địa chỉ" name="diachi" value="<?=$ac->KH_DiaChi?>">
						      </div>
						    </div>
				        <div class="form-group">
						      <label class="control-label col-sm-4" for="email">Email:</label>
						      <div class="col-sm-8">
						        <input type="email" class="form-control" id="email" placeholder="Nhập email" name="email" value="<?=$ac->KH_Email;?>">
						      </div>
						    </div>
						    <div class="form-group">
						      <label class="control-label col-sm-4" for="txttk">Tài khoản:</label>
						      <div class="col-sm-8">
						        <input type="email" class="form-control" id="txttk" name="txttk" readonly="readonly" value="<?=$ac->KH_TaiKhoan?>">
						        <p>(Bạn không thể thay đổi tên tài khoản.)</p>
						      </div>
						    </div>
						    <div class="form-group">
						      <label class="control-label col-sm-4" for="pwd" required>Mật khẩu:</label>
						      <div class="col-sm-8">          
						        <input type="text" class="form-control" id="pwd" name="pwd" value="<?=$ac->KH_MatKhau?>" readonly>
						      </div>
						    </div>
								<div class="form-group"> 
								  <div class="col-sm-offset-2 col-sm-12">
								    <div class="checkbox">
								      <label><input type="checkbox" data-toggle="collapse" data-target="#doimatkhau">Đổi mật khẩu</label>
								    </div>
								  </div>
								  <div id="doimatkhau" class="collapse">
								    <label class="control-label col-sm-4" for="matkhaumoi">Mật khẩu mới:</label>
							      <div class="col-sm-8">          
							        <input type="text" class="form-control" id="matkhaumoi" placeholder="Nhập mật khẩu mới" name="matkhaumoi" value="<?=$ac->KH_MatKhau?>">
							      </div>
								  </div>
								</div>
						    <div class="form-group">        
						      <div class="col-sm-offset-2 col-sm-8">
						        <input type="submit" class="btn btn-danger" value="Thay đổi">
						      </div>
						    </div>
					  </form>
					  <?php } ?>
					</div>
					<!-- 2 -->
				</div>
      </div>
      <!-- end col 9 -->
    </div>

  </div>
</div>
