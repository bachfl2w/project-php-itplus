<!-- Login / register -->
<?php if (isset($_SESSION['user'])) {
  $user = $_SESSION['user'];
?>
<div class="container-fluid banner" style="background-color:#2C343B;height:32px;">
  <div class="navbar-right filter-lr">
    <a href="index.php?function=account&user=<?php echo $user; ?>" class="btn link-w">
      <i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp; <?php echo $user; ?>
    </a>
    <span style="color: white">|</span>
    <a href="logout.php" class="btn link-w">
      Đăng xuất &nbsp; <i class="fa fa-sign-out" aria-hidden="true"></i></i>
    </a>
  </div>
</div>
<?php } else { ?>
<div class="container-fluid banner" style="background-color:#2C343B;height:32px;">
  <div class="navbar-right filter-lr">
    <a href="index.php?function=login" class="btn link-w">
      <i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp; Đăng nhập / Đăng ký
    </a>
  </div>
</div>


<?php } ?>
<!-- end login / register -->

<!-- menu -->
<nav class="navbar banner" data-offset-top="29" data-spy="affix" style="border-bottom:1px solid #D1D1D1;padding:15px 0px;">
  <div class="container-fluid filter-lr">

    <!-- header -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <span class="navbar-brand" style="font-size: 25px;margin-top:-3px;color:#d2534d" href="#">iLap</span>
    </div>
    <!-- end header -->

    <!-- nav -->
    <div id="myNavbar">
      <ul class="nav navbar-nav" >
        <li>
          <a type="button" class="active active-home" style="background-color: white" href="index.php?funtion=getlist">
            <i class="fa fa-home" aria-hidden="true"></i>&nbsp; TRANG CHỦ
          </a>
        </li>

        <li>
          <a class="active active-home" style="background-color: white" href="index.php?function=listlap">
            <i class="fa fa-laptop" aria-hidden="true"></i>&nbsp; LAPTOP
          </a>
        </li>

        <li>
          <a class="active active-home" style="background-color: white" href="index.php?function=listmouse">
            <i class="fa fa-headphones" aria-hidden="true"></i>&nbsp; PHỤ KIỆN
          </a>
        </li>

        <li>
          <a class="active active-home" style="background-color: white" href="index.php?function=cauhoi">
            <i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp; PHẢN HỒI
          </a>
        </li>

        <li>
          <a class="active active-home" style="background-color: white" href="index.php?function=tintuc">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp; TIN TỨC
          </a>
        </li>

      </ul>
      <!-- end nav -->
<?php
  if (isset($_SESSION['cart']))  $count = count($_SESSION['cart']);
  else $count = 0;
?>
      <!-- nav right -->
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a class="active active-home" href="#" data-toggle="modal" data-target="#search-modal" style="margin-right: 10px;background-color: white">
            <span class="glyphicon glyphicon-search" ></span>
          </a>
        </li>
        <li>
          <a class="active active-home" style="background-color: white" href="index.php?function=giohang">
            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 16px"></i>&nbsp; Cart: <?php echo $count; ?> item(s)
          </a>
        </li>
      </ul>
      <!-- end nav right -->

    </div>
    <!-- end nav -->
  </div>
</nav>
<!-- end menu -->

<!-- search -->
<div class="modal fade" id="search-modal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tìm kiếm</h4>
      </div>
      <div class="modal-body">
        <form class="form-group" method="post" action="index.php?function=search">
          <div class="form-group">
            <label for="txtsearch">Từ khóa:</label>
            <input type="text" class="form-control" id="txtsearch" placeholder="Nhập từ khóa" name="txtsearch" size="40">
          </div>
          <div class="form-group">
            <label for="loai">Loại:</label>
            <select class="form-control" name="loai" id="loai">
              <option value="laptop">Laptop</option>
              <option value="mouse">Chuột</option>
            </select>
          </div>
          <div class="form-group">
            <label for="hang">Hãng:</label>
            <select class="form-control" name="hang" id="hang">
              <option value="dell">Dell</option>
              <option value="asus">Asus</option>
              <option value="hp">Hp</option>
              <option value="steelseris">Steelseris</option>
              <option value="razer">Razer</option>
              <option value="apple">Apple</option>
            </select>
          </div>
          <input type="submit" name="submit" class="btn btn-default" value="Tìm kiếm">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
      </div>
    </div>
    <!-- end modal content -->
  </div>
</div>