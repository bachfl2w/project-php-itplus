<?php 
  if (isset($_SESSION['search'])) {
    unset($_SESSION['search']);
  }
?>

<style type="text/css" media="screen">
  .col-sm-12 li {line-height: 25px}
  .col-sm-9 {border-left: 2px solid #f0f3f4;font-size: 13px}
  .col-sm-9 .col-sm-6 ul {margin-left: -55px;list-style-type: none}

  .col-sm-9 .row {border-bottom: 2px solid #f0f3f4;}

  .listlaptop a:hover {color:black;}

  .row .panel-heading{border-bottom: 2px solid #f0f3f4}

  .form-group input , .form-group select {border-radius: 0}
</style>

<div class="filter-lr listlaptop">
  <div class="panel panel-default">

    <div class="panel-heading row" style="padding-bottom: 30px;background-color: #f0f3f4">
      <a href="index.php?function=getlist" style="color: gray">Trang chủ</a>&nbsp; /&nbsp; <a href="#" style="color: #d2534d">Laptop</a>
    </div>

    <div class="row">

      <div class="col-sm-3">
        <div class="panel-heading">
          <h3>Bộ lọc</h3>
        </div>
        <!-- body -->
        <div class="panel-body">
          <b style="font-size: 16px">Nhập tên laptop</b><br><br><br>
          <form action="index.php?function=search" method="post">
            <div class="form-group">
              <label for="txtsearch">Tìm theo tên :</label>
              <input type="text" class="form-control" id="txtsearch" placeholder="Nhập tên sản phẩm" name="txtsearch">
            </div>
            <div class="form-group">
              <input type="hidden" class="form-control" name="loai" id="loai" value="laptop">
            </div>
            <div class="form-group">
              <label for="hang">Hãng:</label>
              <select class="form-control" name="hang" id="hang">
                <option value="dell">Dell</option>
                <option value="asus">Asus</option>
                <option value="hp">Hp</option>
                <option value="apple">Apple</option>
              </select>
            </div>
            <input type="submit" name="submit" class="btn btn-danger" value="Lọc">
          </form>          
        </div>
        <!-- end body -->
      </div>

      <div class="col-sm-9">
        <div class="panel-heading">
          <h3>Laptop</h3>
        </div>
        <div class="panel-body">
          <?php foreach($listlap['laptop'] as $l) { ?>
            <a href="index.php?function=lapdetail&sp_ma=<?php echo $l->SP_Ma;?>"><div class="row" style="padding: 10px 0px">
              <div class="col-sm-4" align="center">
                <img src="image/<?php echo $l->Anh_Ten ;?>" width="90%">
              </div>

              <div class="col-sm-8">
                <div class="col-sm-12" style="margin-left: -15px">
                  <b style="font-size: 18px"><?php echo $l->SP_Ten ;?></b>
                  <b style="font-size: 18px;color: #d2534d;float: right;"><?php echo number_format($l->SP_GiaNiemYet, 0, '', '.') ;?>đ</b><br><br>
                </div>
                <div class="col-sm-6">
                  <ul>
                    <li><b>Màn hình : </b> <?php echo $l->Lap_ManHinh ;?></li>
                    <li><b>VGA : </b> 12 inchHD Anti-Glare LED- Backlit Display</li>
                    <li><b>RAM : </b><?php echo $l->Lap_RAM ;?></li>
                  </ul>
                </div>
                <div class="col-sm-6">
                  <ul>
                    
                    <li><b>CPU : </b><?php echo $l->Lap_CPU ;?></li>
                    <li><b>Hệ điều hành : </b><?php echo $l->Lap_HDH ;?></li>
                    <li><b>Trọng lượng : </b>2 Kg</li>
                  </ul>
                </div>
                <div class="col-sm-12" style="border: 2px solid #f0f3f4;border-style: dotted;background-color: #F1FBF7;font-size: 15px">
                  <div class="panel-body" style="background-color: #F1FBF7">
                    <span style="color: #9f9f9f;color: green">Đặt hàng online nhận ngay khuyến mãi :</span>
                    <ul>
                      <li>Tặng Balo Laptop</li>
                      <li>Tặng Voucher <span style="color: #d2534d">800,000đ</span> mua Office 365</li>
                      <li>Tặng Voucher <span style="color: #d2534d">800,000đ</span> mua Win10</li>
                    </ul>
                  </div>
                </div>
              </div>

            </div></a>
          <?php } ?>
        </div>
      </div>
      <!-- end col 9 -->
    </div>

  </div>
</div>