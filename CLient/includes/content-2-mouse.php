<div class="filter-lr thuonghieu">
  <div class="panel panel-default">
    <div class="panel-body">
      <p align="center" style="font-size: 25px">THƯƠNG HIỆU CHUỘT</p>
      <div class="row">

        <div class="col-sm-3">
          <img src="image/Logo/steelseries.png">
        </div>

        <div class="col-sm-3">
          <img src="image/logo/razer.png">
        </div>

        <div class="col-sm-3 th-test">
          <img src="image/logo/logitech.png" class="">
        </div>

        <div class="col-sm-3">
          <img src="image/logo/apple.png">
        </div>

      </div>
    </div>
  </div>
</div>


<div class="filter-lr content-2">
  <div class="panel panel-default">
    <!-- panel heading -->
    <div class="panel-heading">
      <b>PHỤ KIỆN HOT</b>
    </div>
    <!-- end panel heading -->

    <!-- panel body -->
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <div class="carousel carousel-showmanymoveone slide" id="carousel-tilenav-2" data-interval="false">
          <div class="carousel-inner">

              <?php 
                $r=0 ;
                foreach (array_slice($rsmouse['mouse'], 0, 9) as $m) {
                  if($r == 0) echo'<div class="item active" >';
                  else        echo'<div class="item">'; 
              ?>
                <div <?php if($m->SP_KhuyenMai > 0) echo 'class ="col-xs-12 col-sm-6 col-md-2 offer offer-warning"'; else echo 'class ="col-xs-12 col-sm-6 col-md-2 offer offer-none"' ;?> >

                  <a href="index.php?function=mousedetail&sp_ma=<?php echo $m->SP_Ma;?>" >
                    <div class="shape">
                      <div class="shape-text">
                        -<?=$m->SP_KhuyenMai?>%               
                      </div>
                    </div>
                    <div style="height: 200px;text-align: center;padding-bottom: 100px">
                      <img src="image/<?php echo $m->Anh_Ten ;?>" class="img-responsive"></a>
                    </div>
                    <div style="height: 60px;margin-top: 40px">
                      <p style="font-weight: 600"><?php echo $m->SP_Ten;?></p><br>
                    </div>
                    <div style="height: 80px">
                      <p>
                        <?php
                          $GiaGoc = $m->SP_GiaNiemYet;
                          $KhuyenMai = $m->SP_KhuyenMai;

                          if ($KhuyenMai > 0){
                            $Gia = $GiaGoc - ($GiaGoc * $KhuyenMai / 100);
                            echo '<b>Giá: '.number_format($Gia, 0, '', '.').'đ</b><br>';
                            echo '<small style="text-decoration: line-through;">'.number_format($GiaGoc, 0, '', '.').'đ</small>';
                          }
                          else 
                            echo '<b>Giá: '.number_format($GiaGoc, 0, '', '.').'đ</b><br>';
                        ?>
                      </p>
                    </div>
                </div>
              </div>
            <?php $r++; } ?>

          </div>

          <a class="left carousel-control" href="#carousel-tilenav-2" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
          <a class="right carousel-control" href="#carousel-tilenav-2" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>

        </div>
      </div>
    </div>
  </div>
  <!-- end panel body -->
  </div>
  <!-- end panel default -->
</div>

<script  src="js/carousel2.js"></script>