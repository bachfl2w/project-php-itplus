<?php
  function custom_echo($x, $length) {
    if (strlen($x)<=$length)
      echo $x;
    else {
      $y = mb_substr($x,0,$length, 'utf-8') . '...';
      echo $y;
    }

  }
?>

<?php if(!isset($_SESSION['mail'])) { ?>
<div class="modal fade modal-email" id="myModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content" >

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <br><img src="image/news/email.png" width="148px" height="72px"><br><br>
        <h3 class="modal-title">Đăng ký nhận thông tin khuyến mãi</h3><br>
        <!-- form email -->
        <form class="form-inline" method="post" action="index.php?function=nhanmail">
          <div class="form-group">
            <label for="email"><h4>Email address: </h4></label>
            <input type="email" class="form-control" name="email" id="email" size="40" style="border-radius: 0px">
            <input type="submit" class="btn btn-danger" value="Đăng ký">
          </div>
        </form>
        <!-- end form -->
        <br><p>(Hãy nhập email của bạn để có thể nhận thông tin khuyến mãi và các tin công nghệ hot.)</p>
      </div>

      <div class="modal-footer"></div>

    </div>
    <!-- end modal content -->
  </div>
</div>
<?php  } ?>

<div class="container-fluid filter-lr content-1"> 
  <div class="col-sm-9" style="padding-left: 0">
    <div id="myCarousel" class="carousel slide slide-adv" data-ride="carousel">
      <!-- Wrapper for slides -->
      <div class="carousel-inner"  style="border-top: 1px solid #e5e6e9;border-left: 1px solid #e5e6e9;border-right: 1px solid #e5e6e9;">
        <?php $k=0; foreach($data['quang_cao1'] as $q) { ?>
          <div <?php if($k==0)echo'class="item active"';else echo'class="item"' ?>>
            <img src="image/<?=$q->QC_HinhAnh?>" width="100%">
          </div>
        <?php $k++; } ?>
      </div>
        <!-- End Carousel Inner -->

      <ul class="nav nav-pills nav-justified" style="border-bottom: 1px solid #e5e6e9;border-left: 1px solid #e5e6e9;border-right: 1px solid #e5e6e9">
        <?php $i=0; foreach($data['quang_cao1'] as $q){ ?>
          <li data-target="#myCarousel" data-slide-to="<?=$i?>" <?php if($i==0)echo'class="active"';?>>
            <a href="#">
              <?=$q->QC_TieuDe?>
              <small><?=$q->QC_NoiDung?></small>
            </a>
          </li>
        <?php $i++; } ?>
      </ul>
    </div>
    <!-- End Carousel -->
  </div>

  <!-- adv and news -->
  <div class="col-sm-3" style="padding-right: 0">
    <div class="panel">
      <div class="panel-body" style="border: 1px solid #e5e6e9;border-radius:3px">
        <?php $i=0; foreach($data['quang_cao2'] as $q){ ?>
          <a href="#"><img src="image/<?=$q->QC_HinhAnh?>" width="100%"></a>
        <?php $i++; } ?> 
      </div>

      <div class="panel-heading" style="background-color: #f0f3f4"></div>
    <!-- panel -->
      <div class="panel-body" style="padding-bottom: 0%;border: 1px solid #e5e6e9;border-radius:3px">
        <b style="font-size: 16px; color: #d2534d;">TIN MỚI</b>
        <!-- row -->
        <div class="row">
          <?php foreach ($data['tin_tuc'] as $n) { ?>
            <div class="col-sm-12">
              <a href="index.php?function=tintuc-chitiet&tt_ma=<?=$n->TT_Ma?>" title='<?=$n->TT_TieuDe?>'>
                <img width="24%" src="image/<?=$n->TT_HinhAnh?>">
                <span class="news-content"><?php custom_echo($n->TT_TieuDe, 40) ;?></span>
              </a>
            </div>
          <?php } ?>
        </div>
        <!-- end row -->
        <div class="panel-body text-center" style="border-top: 1px solid #f0f3f4">
          <a href="index.php?function=tintuc" class="btn-link">Xem tất cả</a>
        </div>
      </div>


    </div>
    <!-- end panel -->
  </div>
  <!-- end adv and news -->
  
</div>