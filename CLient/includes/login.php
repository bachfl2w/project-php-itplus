<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>USER Login Form</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Montserrat:400,700'>
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>

  <link rel="stylesheet" href="css/styleLogin.css">

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>


<script language="javascript">
    function checkform2()
  {
    var td = document.forms['form2'].txtuser.value;
      if (td == '')
      {
        alert('Bạn Cần Nhập Tên Đăng Nhập!');
        document.forms[0].txtuser.focus();
        return;
        }
    var nv = document.forms['form2'].txtpass.value;
      if (nv == '')
      {
        alert('Bạn Cần Nhập Mật Khẩu');
        document.forms['form2'].txtpass.focus();
        return;
        }         
      
      document.forms['form2'].action = 'index.php?function=login';
      document.forms['form2'].submit();           
    }
</script>
<script language="javascript">
    function checkform1()
  {
    var td = document.forms['form1'].txtemail.value;
      if (td == '')
      {
        alert('Bạn Cần Nhập Email Để Nhận Lại Tài Khoản!');
        document.forms['form1'].txtemail.focus();
        return;
        }           
      
    document.forms['form2'].action = 'index.php?function=login';
    document.forms['form2'].submit();           
    }
</script>
  
</head>

<body>


  <?php
    if(isset($_GET['error']))
      $error = 'Tài Khoản Hoặc Mật Khẩu Không Đúng!';
?> 
<div class="container">
  <div class="info">
    <h1>ĐĂNG NHẬP</h1>
  </div>
</div>
<div class="form">
  <div class="thumbnail"><img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/169963/hat.svg"/></div>
  
  <form class="login-form" name="form2" action="index.php?function=login" method="post">
    <input type="text" name="txtuser" placeholder="Username"/>
    <input type="password" name="txtpass" placeholder="Password"/>
    <button type="button" name="btnsubmit" onClick="checkform2()" >Đăng nhập</button>
    <p class="message">Quên mật khẩu? <a href="#">Gửi email</a></p>
    <p class="message">Đăng ký? <a href="regis.php"> Sign Up</a></p>
    <br>
    <p class="message1" style="color:#F00;font-size:12px;" ><?php if(isset($error)) echo $error;?></p>
  </form>
  <form class="register-form" name="form1">   
    <input type="text" name="txtemail" placeholder="Email Address"/>
    <button type="button" name="btnsubmit1" onClick="checkform2()">Gửi</button>
    <p class="message">Đã đăng ký? <a style="cursor: pointer;">Sign In</a></p>
    
  </form>
</div>
  <script>
    $('.message a').click(function(){
      $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    } );
  </script>

</body>
</html>
