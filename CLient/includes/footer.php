<div class="content-footer" style="margin-top: 2%;">
  <a href="http://www1.ap.dell.com/content/default.aspx?c=vn&l=en&s=&s=gen&~ck=cr" target="_blank">Dell</a> /
  <a href="https://www.asus.com/vn/" target="_blank">Asus</a> /
  <a href="http://www.hp.com/country/us/en/welcome.html" target="_blank">Hp</a> /
  <a href="https://www.apple.com/" target="_blank">Apple</a> /
  <a href="https://steelseries.com/" target="_blank">Steelseris</a> /
  <a href="https://www.razerzone.com/" target="_blank">Razer</a> /
  <a href="https://www.logitech.com/vi-vn" target="_blank">Logitech</a>
</div>

<div class="container-fluid footer">
  <div class="filter-lr">

    <div class="col-sm-3" >
      <b>GIỚI THIỆU VỀ ILAP</b>
      <p style="font-size: 13px;font-weight: 400">iLap được tạo bởi nhóm gồm có 3 thành viên với nhóm trưởng là Trần Văn Độ và các thành viên trong nhóm bao gồm Vũ Thế Đạt, Đinh Đức Bão.</p>
    </div>

    <div class="col-sm-3">
      <b>HỖ TRỢ KHÁCH HÀNG</b><br>
      <a href="index.php?function=chinhsach" class="btn-link">Các chính sách Của iLap</a><br>
      <a href="index.php?function=cauhoi" class="btn-link">Câu hỏi thường gặp khi mua hàng</a><br>
    </div>

    <div class="col-sm-3" >
      <b>KẾT NỐI VỚI CHÚNG TÔI</b><br>
      <ul>
        <li><a href="https://twitter.com/?lang=vi"><i class="fa fa-twitter"></i></a></li>
        <li><a href="https://www.facebook.com/profile.php?id=100014919254201"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://www.instagram.com/?hl=vi"><i class="fa fa-instagram"></i></a></li>
      </ul><br>
      <ul>
        <li><a href="https://plus.google.com/u/0/100440181638570330550"><i class="fa fa-google"></i></a></li>
        <li><a href="https://www.youtube.com/user/VEVO"><i class="fa fa-youtube"></i></a></li>
      </ul>
    </div>

    <div class="col-sm-3" style="font-size: 14px;">
      <span style="font-weight: 600;">Hỗ trợ thanh toán</span><br>
      <img src="image/logo/visa.png">&nbsp;
      <img src="image/logo/atm.png">&nbsp;
      <img src="image/logo/mastercard.png"><br>
      <div class="row" style="height: auto;width: 100%;padding: 15px 0px">
        <div class="col-sm-6" style="border-right: 1px solid #DFDFDF">
          Tư vấn miễn phí :<br>
          <b>1800 6601</b>
        </div>
        <div class="col-sm-6">
          Góp ý, phản ánh :<br>
          <b>1800 6601</b>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="container" style="height: 35px;line-height: 35px;text-align: center;color: gray">
  Website thương mại điện tử bán laptop của nhóm : Bách, Dương.
</div>

