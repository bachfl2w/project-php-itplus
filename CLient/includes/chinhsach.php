<style type="text/css" media="screen">
	html {background-color: #f0f3f4}

	body,.panel .panel-heading {background-color: white}

	a:hover {text-decoration: underline}

	.cauhoi b {color:#d2534d;}

	.cauhoi .col-sm-3 li {list-style-type: none;border-top: 2px solid #f0f3f4;padding: 5px 20px}

	.tab-content h2 {padding: 40px 0px}

	.nav>li>a:focus, .nav>li>a:hover {background-color: white;}

</style>

<script> 
$(document).ready(function(){
    $("#header1").click(function(){
        $("#content1").slideToggle("slow");
        $("#content2").slideUp("slow");
    });

    $("#header2").click(function(){
        $("#content2").slideToggle("slow");
        $("#content1").slideUp("slow");
    });
});

$(window).on('load',function(){
	$('#content1').hide();
});
</script>

<div class="filter-lr cauhoi">
	<div class="panel panel-default">
		<!-- panel heading -->
		<div class="panel-heading row">
			<a href="index.php?function=getlist" style="color: gray">Trang chủ</a>&nbsp; |&nbsp; <a href="#" style="color: #d2534d">Các chính sách của iLap</a>
		</div>
		<!-- end panel heading -->
		<!-- panel body -->
		<div class="panel-body row">
			<div class="col-sm-9">
				<ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#tab-1">Chính sách bảo hành</a></li>
			    <li><a data-toggle="tab" href="#tab-2">Chính sách bảo mật</a></li>
			    <li><a data-toggle="tab" href="#tab-3">Chính sách giao hàng</a></li>
			  </ul>

			  <div class="tab-content">
			    <div id="tab-1" class="tab-pane fade in active">
			    	<h2 >Chính sách bảo hành</h2>
			    	<p align="center"><img src="image/News/bao_hanh.jpg" width="100%"></p>
			    	<!-- 1 -->
			      <b>1. Đối với laptop, màn hình và linh kiện:</b>
							<div class="panel-body">
								Trong  vòng 15 ngày đầu sau khi mua hàng sản phẩm bị lỗi thì được đổi sản phẩm mới 100% (Sản phẩm phải có đầy đủ vỏ hộp, phụ kiện kèm theo và không bị trầy xước, không vi phạm điều kiện bảo hành khác và không phải là vật tư tiêu hao (<i>Vật tiêu hao là vật khi đã qua sử dụng một lần thì mất đi hoặc không giữ được tính chất, hình dáng và tính năng sử dụng ban đầu</i>))<br><br>
								Cam kết khắc phục sự cố và trả bảo hành trong thời gian 07 ngày (không tính thứ 7, chủ nhật, các ngày lễ, tết và các trường hợp khác được thỏa thuận trước) cho các sản phẩm đã mua từ ngày thứ 16 cho đến hết thời hạn bảo hành
							</div>
						<!-- end 1 -->
						<!-- 1 -->
			      <b>2. Những trường hợp bảo hành muộn hơn so với quy định:</b>
							<div class="panel-body">
								Trong trường hợp thời gian trả bảo hành muộn hơn thời gian quy định, chúng tôi sẽ cộng thêm vào thời hạn bảo hành sản phẩm của Quý khách, thời gian cộng thêm tương ứng với thời gian chậm trả cho khách hàng.<br><br>
								Đối với các sản phẩm được bảo hành theo quy định của hãng, của nhà cung cấp mà bảo h ành lâu thì Trung tâm bảo hành có trách nhiệm đôn đốc hãng trả bảo hành trong thời gian sớm nhất cho Quý khách hàng và liên lạc với khách hàng ngay khi hãng trả bảo hành. Quý khách có thể được mượn thiết bị tương đương trong thời gian chờ bảo hành muộn (nếu có có sẵn hàng cho mượn).<br><br>
								Trường hợp  không bảo hành được hoặc thời gian bảo hành quá lâu (trên 60 ngày) quý khách hàng có thể lựa chọn một trong các phương án sau:<br><br>
								+/ Đổi sang thiết bị khác gần giống sản phẩm bảo hành hoặc sản phẩm có thông số kỹ thuật cao hơn với chi phí thỏa thuận.<br><br>
								+/ Nhập lại sản phẩm theo giá thỏa thuận.<br><br>
								<b>Lưu ý :</b><br><br>
								Chính sách đổi và nhập lại chỉ áp dụng với sản phẩm bị lỗi do hãng sản xuất và đủ điều kiện bảo hành.<br><br>
								Công Ty cổ Phần Máy tính Hà nội không bảo hành và không chịu trách nhiệm về dữ liệu, tính hợp pháp của các phần mềm có trong sản phẩm của khách hàng. Do đó Quý khách hàng vui lòng lưu lại dữ liệu của khách hàng trước khi gửi bảo hành<br><br>
							</div>
						<h2>Điều kiện bảo hành</h2>
						<!-- end 1 -->
						<!-- 1 -->
			      <b>1/ Những trường hợp sau đây sẽ không được bảo hành:</b>
							<div class="panel-body">
								Thiết bị không có phiếu bảo hành, phiếu bảo hành đã bị sửa đổi thông tin, bị chắp nối, rách, nát…<br><br>
								Các thiết bị không còn nguyên tem bảo hành và mã vạch của hãng sản xuất, nhà phân phối và của Công ty Cổ phần Máy tính Hà nội.<br><br>
								Sử dụng điện áp sai quy định, quá công suất của thiết bị hoặc cháy nổ, rơi móp, nứt, thủng, nước vào, trầy xước…<br><br>
								Hư hỏng do thiên tai, động vật, côn trùng và con người gây ra.<br><br>
								Các phụ kiện tiêu hao được định nghĩa ở mục (*)<br><br>
								Tất cả các lỗi liên quan đến phần mềm.<br><br>
								Hết thời gian bảo hành ghi trên phiếu.<br><br>
								Khách hàng tự ý can thiệp vào Bios và phần cứng khác gây hư, hỏng.<br><br>
								Khách hàng tự ý can thiệp vào Bios và phần cứng khác gây hư, hỏng. 
							</div>
						<!-- end 1 -->
						<b>2/Các trường hợp khác sẽ được bảo hành theo chính sách và tiêu chuẩn của hãng sản xuất, nhà phân phối</b>
			    </div>

			    <div id="tab-2" class="tab-pane fade">
			      <h2 >Chính sách bảo mật</h2>
			      <p align="center"><img src="image/News/bao-mat-thong-tin-khach-hang.jpg" width="100%"></p>
			      <i>iLap cam kết sẽ bảo mật những thông tin mang tính riêng tư của bạn. Bạn vui lòng đọc bản “Chính sách bảo mật” dưới đây để hiểu hơn những cam kết mà chúng tôi thực hiện, nhằm tôn trọng và bảo vệ quyền lợi của người truy cập.</i><br><br>
						<!-- 1 -->
						<b>1. Mục đích và phạm vi thu thập?</b>
							<div class="panel-body">
								Để truy cập và sử dụng một số dịch vụ tại iLap, bạn có thể sẽ được yêu cầu đăng ký với chúng tôi thông tin cá nhân (Email, Họ tên, Số ĐT liên lạc…). Mọi thông tin khai báo phải đảm bảo tính chính xác và hợp pháp. iLap không chịu mọi trách nhiệm liên quan đến pháp luật của thông tin khai báo.<br><br>

								Chúng tôi cũng có thể thu thập thông tin về số lần viếng thăm, bao gồm số trang bạn xem, số links (liên kết) bạn click và những thông tin khác liên quan đến việc kết nối đến site iLap. Chúng tôi cũng thu thập các thông tin mà trình duyệt Web (Browser) bạn sử dụng mỗi khi truy cập vào iLap, bao gồm: địa chỉ IP, loại Browser, ngôn ngữ sử dụng, thời gian và những địa chỉ mà Browser truy xuất đến.
							</div>
						<!-- end 1 -->
						<!-- 1 -->
						<b>2. Phạm vi sử dụng thông tin</b>
							<div class="panel-body">
								iLap thu thập và sử dụng thông tin cá nhân bạn với mục đích phù hợp và hoàn toàn tuân thủ nội dung của “Chính sách bảo mật” này. Khi cần thiết, chúng tôi có thể sử dụng những thông tin này để liên hệ trực tiếp với bạn dưới các hình thức như: gởi thư ngỏ, đơn đặt hàng, thư cảm ơn, sms, thông tin về kỹ thuật và bảo mật…
							</div>
						<!-- end 1 -->
						<!-- 1 -->
						<b>3. Thời gian lưu trữ thông tin</b>
							<div class="panel-body">
								Dữ liệu cá nhân của Thành viên sẽ được lưu trữ cho đến khi có yêu cầu hủy bỏ hoặc tự thành viên đăng nhập và thực hiện hủy bỏ. Còn lại trong mọi trường hợp thông tin cá nhân thành viên sẽ được bảo mật trên máy chủ của iLap.
							</div>
						<!-- end 1 -->
						<b>4.Phương tiện và công cụ để người dùng tiếp cận và chỉnh sửa dữ liệu cá nhân</b>
							<div class="panel-body">
								Hiện website chưa triển khai trang quản lý thông tin cá nhân của thành viên, vì thế việc tiếp cận và chỉnh sửa dữ liệu cá nhân dựa vào yêu cầu của khách hàng bằng cách hình thức sau:<br><br>
								Gọi điện thoại đến tổng đài chăm sóc khách hàng 1800 6601, bằng nghiệp vụ chuyên môn xác định thông tin cá nhân và nhân viên tổng đài sẽ hỗ trợ chỉnh sửa thay người dùng<br>
								Để lại bình luận hoặc gửi góp ý trực tiếp từ website iLap, quản trị viên kiểm tra thông tin và xem xét nội dung bình luận có phù hợp với pháp luật và chính sách của iLap
							</div>
						<!-- end 1 -->
						<!-- end 1 -->
						<b>5. Cam kết bảo mật thông tin cá nhân khách hàng</b>
							<div class="panel-body">
								Thông tin cá nhân của thành viên trên iLap được iLap cam kết bảo mật tuyệt đối theo chính sách bảo vệ thông tin cá nhân của iLap. Việc thu thập và sử dụng thông tin của mỗi thành viên chỉ được thực hiện khi có sự đồng ý của khách hàng đó trừ những trường hợp pháp luật có quy định khác.<br><br>

								Không sử dụng, không chuyển giao, cung cấp hay tiết lộ cho bên thứ 3 nào về thông tin cá nhân của thành viên khi không có sự cho phép đồng ý từ thành viên.<br><br>


								Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn công dẫn đến mất mát dữ liệu cá nhân thành viên, iLap sẽ có trách nhiệm thông báo vụ việc cho cơ quan chức năng điều tra xử lý kịp thời và thông báo cho thành viên được biết.

								Bảo mật tuyệt đối mọi thông tin giao dịch trực tuyến của Thành viên bao gồm thông tin hóa đơn kế toán chứng từ số hóa tại khu vực dữ liệu trung tâm an toàn cấp 1 của iLap.<br><br>


								Ban quản lý iLap yêu cầu các cá nhân khi đăng ký/mua hàng là thành viên, phải cung cấp đầy đủ thông tin cá nhân có liên quan như: Họ và tên, địa chỉ liên lạc, email, số chứng minh nhân dân, điện thoại, số tài khoản, số thẻ thanh toán …., và chịu trách nhiệm về tính pháp lý của những thông tin trên. Ban quản lý iLap không chịu trách nhiệm cũng như không giải quyết mọi khiếu nại có liên quan đến quyền lợi của Thành viên đó nếu xét thấy tất cả thông tin cá nhân của thành viên đó cung cấp khi đăng ký ban đầu là không chính xác.
							</div>
						<!-- end 1 -->
			    </div>

			    <div id="tab-3" class="tab-pane fade">
			    	<h2 >Chính sách giao hàng</h2>
			    	<p align="center"><img src="image/News/shipper.png" width="100%"></p>
			      <!-- 1 -->
			      <b>60 Phút giao hàng</b>
							<div class="panel-body">
								Cam kết giao hàng tối đa trong vòng 60 phút (bán kính 10km) toàn bộ 63 tỉnh thành
							</div>
						<!-- end 1 -->
						<!-- 1 -->
			      <b>63 tỉnh thành</b>
							<div class="panel-body">
								iLap nhận giao hàng siêu tốc khắp 63 tỉnh thành trên toàn quốc.
							</div>
						<!-- end 1 -->
						<!-- 1 -->
			      <b>20km Giao hàng miễn phí</b>
							<div class="panel-body">
								Miễn phí giao hàng trong bán kính 20km có đặt shop.<br><br>Với khoảng cách lớn hơn 20km, nhân viên iLap Shop sẽ tư vấn chi tiết về cách thức giao nhận thuận tiện nhất
							</div>
						<!-- end 1 -->
						<!-- 1 -->
			      <b>Mua hàng online</b>
							<div class="panel-body">
								Cam kết 100% là hàng chính hãng, bảo hành, đổi trả trên toàn quốc.<br><br>
								Hỗ trợ kỹ thuật online miễn phí qua tổng đài.<br><br>
								Tiết kiệm thời gian, thỏa sức mua sắm 24/7.<br><br>
								Khuyến mãi mới cập nhật liên tục.
							</div>
						<!-- end 1 -->
			    </div>

			  </div>

			</div>
			<!-- end col 9 -->


		</div>
		<!-- end panel body -->
	</div>
</div>

<?php include('includes/phanhoi-content.php') ?>