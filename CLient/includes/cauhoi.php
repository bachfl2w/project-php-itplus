<style type="text/css" media="screen">
	html {background-color: #f0f3f4}

	body,.panel .panel-heading {background-color: white}

	a:hover {text-decoration: underline}

	.cauhoi b {color:#d2534d;}

</style>

<!-- chạy phần chính sách -->

<div class="filter-lr cauhoi">
	<div class="panel panel-default">
		<!-- panel heading -->
		<div class="panel-heading row">
			<a href="index.php?function=getlist" style="color: gray">Trang chủ</a>&nbsp; |&nbsp; <a href="#" style="color: #d2534d">Câu hỏi thường gặp</a>
		</div>
		<!-- end panel heading -->
		<!-- panel body -->
		<div class="panel-body row">
			<div class="col-sm-9">
				<h2 style="font-weight: 600">Câu hỏi thường gặp</h2><br>
				<p align="center"><img src="image/News/FAQ.jpg" width="100%"></p>
					<!-- 1 -->
					<b>1. Hàng được giao đến như thế nào?</b>
						<div class="panel-body">
							iLap giao hàng trong vòng 48 giờ tính từ thời điểm đặt hàng và tính theo thời gian làm việc hành chính trong phạm vi các tỉnh thành: Thành Phố Hồ Chí Minh, Hà Nội, Đà Nẵng, Hải Phòng, Bình Dương, Cần Thơ, Huế, Đắk Lắk, Vinh, Hải Dương, Gia Lai, Thái Bình, Bắc Ninh, Thái Nguyên, Bình Thuận. Đối với địa phương khác, chúng tôi sẽ giao hàng nhanh nhất có thể tùy theo dịch vụ vận chuyển mà quý khách chọn sử dụng và các điều kiện khách quan khác.
						</div>
					<!-- end 1 -->
					<!-- 1 -->
					<b>2. Nếu phát hiện lỗi sản phẩm trong thời hạn bảo hành thì xử lý thế nào?</b>
						<div class="panel-body">
							Quý khách có thể liên hệ trực tiếp với trung tâm hỗ trợ khách hàng của nhà sản xuất. Những thông tin này thường có trong hướng dẫn sử dụng đính kèm theo sản phẩm. Trung tâm sẽ nhanh chóng cho Quý khách biết nguyên nhân gây lỗi sản phẩm của bạn và đề xuất hướng xử lý. Trường hợp không thể liên hệ được với nhà sản xuất, quý khách có thể đến các cửa hàng của chúng tôi để được hỗ trợ trực tiếp.<br><br>
							<i>***Lưu ý: nếu các hư hỏng không thuộc phạm vi bảo hành của nhà sản xuất, khách hàng sẽ phải chịu chi phí cho việc vận chuyển nêu trên.</i>
						</div>
					<!-- end 1 -->
					<!-- 1 -->
					<b>3. Làm sao khi hàng bị hư hỏng trong trường hợp quá thời hạn bảo hành?</b>
						<div class="panel-body">
							Nếu thời hạn bảo hành đã hết, Quý khách sẽ phải chịu mọi chi phí dịch vụ phát sinh trong quá trình xử lý.<br><br>
							Chúng tôi khuyên Quý khách nên sửa chữa tại những trung tâm dịch vụ được chứng nhận của nhà sản xuất. Quý khách có thể liên hệ trực tiếp các Shop của chúng tôi để được hỗ trợ mang đến trung tâm bảo hành hoặc liên hệ trực tiếp đến các trung tâm bản hành do nhà sản xuất chỉ định. Quý khách có thể tham khảo thêm chính sách bảo hành của chúng tôi <a href="#" class="btn-link">tại đây</a>.
						</div>
					<!-- end 1 -->
					<b>4. Làm sao để biết thời hạn bảo hành của sản phẩm?</b>
						<div class="panel-body">
							Quý khách sẽ tìm thấy thời hạn bảo hành trong thẻ bảo hành đính kèm sản phẩm của nhà sản xuất hoặc liên hệ với bộ phận chăm sóc khách hàng của chúng tôi qua số đường dây nóng <b>1800 6616</b> (từ 08:00 đến 22:00 tất cả các ngày kể cả ngày lễ) để được hướng dẫn kiểm tra hoặc <a href="index.php?function=chinhsach" class="btn-link">vào đây</a>. để tra cứu danh sách cửa hàng của chúng tôi để liên hệ với cửa hàng gần nhất.
						</div>
					<!-- end 1 -->
			</div>
		</div>
		<!-- end panel body -->
	</div>
</div>

<?php include('includes/phanhoi-content.php'); ?>