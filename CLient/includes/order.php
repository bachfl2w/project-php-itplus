<?php
	$con = mysql_connect('localhost', 'root','');
	mysql_select_db('shoplaptop');
	
	foreach($_SESSION['cart'] as $key=>$value)
	{
		$item[]=$key;
	}
	$str=implode(",",$item);

?>


<style type="text/css" media="screen">
	body {background-color: white}
</style>
<div class="filter-lr">
	<div class="row">
		<div class="col-sm-9">
			<form class="form-horizontal" method="post" action="thanhtoan.php" accept-charset="utf-8">
				<li class="list-group-item">
					<h3>1. Thông tin khách hàng</h3>
				</li>
				<li class="list-group-item">
			    <div class="form-group">
			      <label class="control-label col-sm-3" for="hoten">Họ tên người nhận*:</label>
			      <div class="col-sm-9">
			        <input type="text" class="form-control" id="hoten" placeholder="Nhập tên người nhận hàng" name="hoten">
			      </div>
			    </div>
			    <div class="form-group">
			      <label class="control-label col-sm-3" for="sdt">Số Điện thoại:</label>
			      <div class="col-sm-9">
			        <input type="text" class="form-control" id="sdt" placeholder="Nhập số điện thoại" name="sdt">
			      </div>
			    </div>
			    <div class="form-group">
			      <label class="control-label col-sm-3" for="sdt">Lưu ý:</label>
			      <div class="col-sm-9">
			        <textarea class="form-control" name="luuy" placeholder="Nhập lưu ý (nếu có)." style="max-width: 100%" rows="5"></textarea> 
			      </div>
			    </div>
				</li>
				<li class="list-group-item">
					<h3>2. Phương thức nhận hàng</h3>
				</li>
				<li class="list-group-item">
					<h4>Nhận hàng tại Shop</h4>
					<ul>
						<li>147 Xuân Thủy - Cầu Giấy - Hà Nội</li>
						<li>1 Hoàng Đạo Thúy - Cầu Giấy - Hà Nội</li>
					</ul>
					<div class="checkbox">
					  <label for="giaohang"><input name="giaohang" id="giaohang" type="checkbox" data-toggle="collapse" data-target="#giaohang-noidung">Giao hàng tận nơi, miễn phí (Ship cod)</label>
					</div>
					<div id="giaohang-noidung" class="collapse">
						<div class="form-group">
				      <label class="control-label col-sm-3" for="diachi">Địa chỉ người nhận*:</label>
				      <div class=" col-sm-9">
				        <input type="text" class="form-control" id="diachi" placeholder="Nhập địa chỉ người nhận hàng" name="diachi">
				      </div>
				    </div>
					</div>
				</li>
				<li class="list-group-item">
					<h3>3. Hình thức thanh toán</h3>
				</li>
				<li class="list-group-item col-sm-12">
					<h4>Thanh toán trực tiếp khi nhận hàng</h4>
					<?php if(isset($_SESSION['user'])) { ?>
						<input type="submit" class="btn btn-lg btn-danger" style="float: right" value="ĐẶT HÀNG">
					<?php } else { ?>
						<a href="index.php?function=login" class="btn btn-lg btn-danger" style="float: right" >Đăng nhập để đặt hàng</a>
					<?php } ?>
				</li>
		  </form>
		</div>
		<div class="col-sm-3">
		  <ul class="list-group">
		  	<form action=".php" method="post" accept-charset="utf-8">
		    <li class="list-group-item"><h3>Đơn hàng</h3></li>
			    <li class="list-group-item">
		    <?php 
				$i = 0;
				$total = 0;
				$tongSL = 0;

				$sql="
					select * 
					from san_pham a
					inner join anh b
					on a.SP_Ma = b.SP_Ma
					where a.SP_Ma in ($str)
				";
				$query=mysql_query($sql);

				while($row=mysql_fetch_array($query)) {

					$giagoc  = $row['SP_GiaNiemYet'];
					$km      = $row['SP_KhuyenMai'];
					$gia     = $giagoc - ($giagoc*$km/100);
					$soluong = $_SESSION['cart'][$row['SP_Ma']];
					$giaban  = $soluong * $gia;
					$total  += $giaban;
					$tongSL += $soluong;
				?>
		        <h5><?=$row['SP_Ten']?> : <?=$soluong?></h5>
		 		<?php } ?>
		        <h5>Phí vận chuyển : <span style="color:green">Free</span></h5>
			    </li>
			    <li class="list-group-item">
		        <h5>
		        	<b style="font-size: 20px">Thành tiền :</b>
		        	<b style="color: #d2534d; font-size: 20px"><?=number_format($total,0)?>đ</b>
		        </h5>
		         <small>(Đã bao gồm VAT)</small><br><br>
			 		</li>
			 	</form>
		  </ul>
		</div>
	</div>
</div>