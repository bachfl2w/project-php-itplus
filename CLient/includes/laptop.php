<?php foreach($data['lap-detail'] as $l){ ?>
  <div class="filter-lr product">
    <div class="panel panel-default">
      <!-- panel heading -->
      <div class="content-2-heading">
        <b><?php echo $l->SP_Ten; ?></b>
      </div>
      <!-- end panel heading -->

      <!-- panel body -->
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-4 mag" align="center" style="z-index: 49">
            <img data-toggle="magnify" src="image/<?php echo $l->Anh_Ten; ?>" width="100%">
            <i style="color: #9f9f9f;font-size: 12px">Rê chuột để phóng to hình ảnh <span class="glyphicon glyphicon-search" style="font-size: 10px"></span></i>
          </div>
          <div class="col-sm-5">

            <div class="row">
              <div class="col-sm-12">
                <ul style="list-style-type: none;margin-left: -20px">
                  <?php
                    $GiaGoc    = $l->SP_GiaNiemYet;
                    $KhuyenMai = $l->SP_KhuyenMai;

                    if ($KhuyenMai > 0){
                      $Gia = $GiaGoc - ($GiaGoc * $KhuyenMai / 100);
                      echo '<li><h3><b>'.number_format($Gia, 0, '', '.').'đ</b></h3>';
                      echo '<small style="text-decoration:line-through">' . number_format($GiaGoc,0,'','.'). 'đ</small> (-'.$KhuyenMai.'%)</li>';
                    }
                    else 
                      echo '<li><h3><b>'.number_format($GiaGoc, 0, '', '.').'đ</b></h3></li>';
                  ?>
                </ul>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <ul>
                  <li><b>Màn hình : </b><?=$l->Lap_ManHinh?></li>
                  <li><b>RAM : </b><?=$l->Lap_RAM?></li>
                  <li><b>CPU : </b><?=$l->Lap_CPU?></li>
                </ul>
              </div>
              <div class="col-sm-6">
                <ul>
                  <li><b>Trọng lượng : </b>2 Kg</li>
                  <li><b>Pin : </b><?=$l->Lap_Pin?></li>
                  <li><b>HĐH : </b><?=$l->Lap_HDH?></li>
                  <li><b>Thời gian bảo hành : </b>1 năm</li>
                </ul>
              </div>
              <div class="col-sm-12">
                <ul>
                  <li>
                    <div class="panel-body" style=";border: 2px solid #C9C9C9;border-style: dotted;background-color: #F1FBF7;font-size: 15px">
                      <span style="font-weight: 600;color: #9f9f9f;color: green">Đặt hàng tại shop nhận ngay khuyến mãi :</span>
                      <ul>
                        <li>Tặng Balo Laptop</li>
                        <li>Tặng Voucher <span style="color: red">800,000đ</span> mua Office 365</li>
                        <li>Tặng Voucher <span style="color: red">800,000đ</span> mua Win10</li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-12" style="text-align: center">
                <a href="addcart.php?&item=<?=$l->SP_Ma?>&f=add" class="btn btn-danger btn-lg" role="button">
                  Mua ngay<br>
                  <small style="font-size: 12px">Giao tận nơi hoặc nhận ở shop</small>
                </a>
                <a href="addcart.php?&item=<?=$l->SP_Ma?>&f=add" class="btn btn-default btn-lg" role="button">
                  Thêm vào giỏ hàng<br>
                  <small style="font-size: 12px">Bạn có thể kiểm tra ở giỏ hàng</small>
                </a>
              </div>
            </div>

          </div>

          <div class="col-sm-3">
            <img src="image/carousel/QC_right.jpg" width="100%">
          </div>

        </div>
        <!-- end row -->

      </div>
      <!-- end panel body -->

    </div>
    <!-- end panel default -->
  </div>
<?php } ?>

<!-- include('/includes/sp_cungloai.php'); -->

<?php foreach($data['lap-detail'] as $l){ ?>
  <div class="filter-lr sp-chitiet">
    <div class="panel panel-default">

      <!-- panel body -->
      <div class="panel-body">
        <div class="row">

          <div class="col-sm-12">
            <div class="content-2-heading">
              <h4>Thông tin số kĩ thuật</h4>
            </div>
            <div class="panel-body">
              <!-- 1 -->
              <b>Bộ xử lý</b>
              <table class="table">
                <tbody>
                  <tr>
                    <td width="200px">CPU :</td>
                    <td><?=$l->Lap_CPU?></td>
                  </tr>
                  <tr>
                    <td>Bộ nhớ đệm :</td>
                    <td><?=$l->Lap_BoNhoDem?></td>
                  </tr>
                  <tr>
                    <td>RAM :</td>
                    <td><?=$l->Lap_RAM?></td>
                  </tr>
                </tbody>
              </table>
              <!-- end 1 -->
              <!-- 2 -->
              <b>Phần cứng</b>
              <table class="table">
                <tbody>
                  <tr>
                    <td width="200px">Ổ cứng :</td>
                    <td><?=$l->Lap_OCung?></td>
                  </tr>
                  <tr>
                    <td>Ổ đĩa :</td>
                    <td><?=$l->Lap_ODia?></td>
                  </tr>
                  <tr>
                    <td>Âm thanh :</td>
                    <td><?=$l->Lap_AmThanh?></td>
                  </tr>
                  <!-- <tr>
                    <td>Webcam :</td>
                    <td><?=$l->Lap_?></td>
                  </tr> -->
                  <tr>
                    <td>Pin :</td>
                    <td><?=$l->Lap_Pin?></td>
                  </tr>
                </tbody>
              </table>
              <!-- end 2 -->
              <!-- 3 -->
              <b>Màn hình</b>
              <table class="table">
                <tbody>
                  <tr>
                    <td width="200px">Kích thước và phân giải :</td>
                    <td><?=$l->Lap_ManHinh?></td>
                  </tr>
                  <!-- <tr>
                    <td>Card màn hình :</td>
                    <td>Intel® Core™ i5-7200U Processor (3M Cache, up to 3.10 GHz)</td>
                  </tr> -->
                </tbody>
              </table>
              <!-- end 3 -->
              <!-- 3 -->
              <b>Thông tin khác</b>
              <table class="table">
                <tbody>
                  <tr>
                    <td width="200px">Mô tả :</td>
                    <td><?=$l->Lap_MoTa?></td>
                  </tr>
                </tbody>
              </table>
              <!-- end 3 -->
            </div>
          </div>


        </div>
      </div>

      <!-- end panel body -->
    </div>
    <!-- end panel default -->
  </div>
<?php } ?>

<?php include('/includes/phanhoi-content.php') ?>


<script  src="js/carousel2.js"></script>